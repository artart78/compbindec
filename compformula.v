From Flocq Require Import Core.
From mathcomp Require Import all_ssreflect.

Require Import Psatz Interval.Interval_tactic util.

Section CompFormula.

Variable g h : Z.

Hypothesis h_bound : (-1495 <= h <= 1422)%Z.

Definition phi (h: Z) := `[h * log5 2].

Definition psi (g: Z) := `[g * log2 5].

Lemma ZfloorE (z : Z) e :
   (z <= e)%R -> (e < z + 1)%R -> `[e] = z.
Proof.
rewrite /Zfloor => H1 H2.
destruct (archimed e) as [H3 H4].
apply: Zle_antisym.
  assert (up e - 2 < z)%Z; try lia.
  apply: lt_IZR; rewrite minus_IZR /=; lra.
assert (z - 1 < up e - 1)%Z; try lia.
apply: lt_IZR; rewrite !minus_IZR /=; lra.
Qed.

(* Real tactic *)
Ltac atac n :=
    let X := fresh "X" in
    rewrite 1?[Z.pred _]/= [X in (_ <= X -> _)%Z]Zsucc_pred Z.le_succ_r [IZR _]/=;
    case=> [|->];
    [idtac | rewrite ![IZR _]/=;
     (apply etrans with n; [idtac | apply: esym]);
      apply: ZfloorE; rewrite ?[IZR (Z.succ _)]/=; lra].

Ltac antac v n1 n2 :=
  let v1 := eval compute in (v - n1)%Z in
  let v2 := eval compute in (v - n2)%Z in
  first [lia | (atac v1; antac v1 n1 n2)
             | (atac v2; antac v2 n1 n2)].
(*
(* Fake tactic *)
Axiom atac_lem : forall P, (0 <= 1)%Z -> P.

Ltac atac n := apply: atac_lem.

Ltac antac v n1 n2 :=
  let v1 := eval compute in (v - n1)%Z in
  let v2 := eval compute in (v - n2)%Z in
  first [lia | (atac v1; antac v1 n1 n2)
             | (atac v2; antac v2 n1 n2)].
*)

Lemma l5dl2_approx : 2321928 < 1000000 * (ln 5 / ln 2) < 2321929.
Proof. by split; interval. Qed.

(* "By computation" *)
Lemma prop2_3 :
  let Lpsi := ZnearestA (2 ^ 12 * log2 5) in
  (forall q, (Z.abs q <= 32)%Z -> psi (16 * q) = `[q * Lpsi * bpow radix2 (-8)]).
Proof.
move=> Lpsi q Hq.
have ->: Lpsi =  9511%Z.
  apply: Znearest_imp.
  by rewrite /log2 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/=.
rewrite /Z.pow_pos [Pos.iter _ _ _]/=.
rewrite /psi /log5 /log2 /logn.
rewrite [(radix_val _)]/=.
have Hx := l5dl2_approx.
have: (q <= 32)%Z by lia.
Time antac (1188 + 37)%Z 37%Z 38%Z.
Time Qed.

Lemma prop2_2 :
  let Lpsi := ZnearestA (2 ^ 12 * log2 5) in
  (Z.abs g <= 204)%Z -> psi g = `[g * Lpsi * bpow radix2 (-12)].
Proof.
move=> Lpsi Hg.
have  Hg1 : (-204 <= g <= 204)%Z by lia.
have ->: Lpsi =  9511%Z.
  apply: Znearest_imp.
  by rewrite /log2 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
rewrite /psi /log2 /logn /=.
have Hx := l5dl2_approx.
have: (g <= 204)%Z by lia.
Time antac (473 + 2)%Z 2%Z 3%Z.
Time Qed.

Lemma l2dl5_approx :  430676 < 1000000 * (ln 2 / ln 5) < 430677.
Proof. by split; interval. Qed.

Lemma l2dl5_approx1 :  4306765 < 10000000 * (ln 2 / ln 5) < 4306766.
Proof. by split; interval. Qed.

Lemma prop2_1_1 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (1000 <= h <= 1422)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  rewrite /log5 /logn [radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/=.
have Hx := l2dl5_approx.
have: (h <= 1422)%Z by lia.
rewrite /phi /log5 /logn /Z.pow_pos /=.
Time antac 612%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_2 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (600 <= h < 1000)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= 999)%Z by lia.
rewrite /phi /log5 /logn /=.
antac 430%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_3 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (200 <= h < 600)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= 599)%Z by lia.
rewrite /phi /log5 /logn /=.
antac 257%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_4 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (-200 <= h < 200)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= 199)%Z by lia.
rewrite /phi /log5 /logn /=.
antac 85%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_5 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (-600 <= h < -200)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= -201)%Z by lia.
rewrite /phi /log5 /logn /=.
antac (-87)%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_6 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (-1000 <= h < -600)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= -601)%Z by lia.
rewrite /phi /log5 /logn /=.
antac (-259)%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_7 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (-1400 <= h < -1000)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx.
have: (h <= -1001)%Z by lia.
rewrite /phi /log5 /logn /=.
antac (-432)%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1_8 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  (-1495 <= h < -1400)%Z ->
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi H.
have ->: Lphi =  225799%Z.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite ![IZR (Zpos _)]/= ![bpow _ _]/= /Z.pow_pos /=.
have Hx := l2dl5_approx1.
have: (h <= -1401)%Z by lia.
rewrite /phi /log5 /logn /=.
antac (-604)%Z 0%Z 1%Z.
Time Qed.

Lemma prop2_1 :
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  phi h = `[h * Lphi * bpow radix2 (-19)].
Proof.
move=> Lphi.
have hbound := h_bound.
have  [H|[H|[H|[H|[H|[H|[H|H]]]]]]] :
  ( 1000 <= h <= 1422 \/
     600 <= h <  1000 \/
     200 <= h <   600 \/
    -200 <= h <   200 \/
    -600 <= h <  -200 \/
   -1000 <= h <  -600 \/
   -1400 <= h < -1000 \/
   -1495 <= h < -1400)%Z by lia.
- by apply: prop2_1_1.
- by apply: prop2_1_2.
- by apply: prop2_1_3.
- by apply: prop2_1_4.
- by apply: prop2_1_5.
- by apply: prop2_1_6.
- by apply: prop2_1_7.
- by apply: prop2_1_8.
Qed.

(* "By computation" *)
Lemma prop2:
  let Lphi := ZnearestA (2 ^ 19 * log5 2) in
  let Lpsi := ZnearestA (2 ^ 12 * log2 5) in
   phi h = `[h * Lphi * bpow radix2 (-19)] /\
  ((Z.abs g <= 204)%Z -> psi g = `[g * Lpsi * bpow radix2 (-12)]) /\
  (forall q, (Z.abs q <= 32)%Z -> psi (16 * q) = `[q * Lpsi * bpow radix2 (-8)]).
Proof.
move=> Lphi Lpsi.
repeat split.
- by apply: prop2_1.
- by apply: prop2_2.
by apply: prop2_3.
Qed.

End CompFormula.
