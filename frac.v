From Flocq Require Import Core.
From mathcomp Require Import all_ssreflect.
From Interval Require Import Interval_tactic.
Require Import rfrac util Psatz compformula.

Section FracComp.

Open Scope Z_scope.

(* mk_conv_list_rec : 
    n the number of iteration 
    l the accumulated list 
    qmax the maximum denominator 
    (p/q) the current reminder 
    (p1/q1) the previous previous convergent
    (p2/q2) the previous convergent
*)
Fixpoint mk_conv_list_rec n k l qmax p q p1 q1 p2 q2 :=
  if n is n1.+1 then
   let (a, r) := Z.div_eucl p q in
   let p3 := a * p2 + p1 in
   let q3 := a * q2 + q1 in
   if (Zle_bool q3 qmax) then 
      if (Zeq_bool r 0) then None else 
      mk_conv_list_rec n1 k.+1 ((p3, q3) :: l) qmax q r p2 q2 p3 q3
   else Some l
  else None.

Local Notation " 'e[ r ]_ n" := (eps r n) (at level 10, format " ''e[' r ]_ n ").
Local Notation " 'a[ r ]_ n" := (elt r n) (at level 10, format " ''a[' r ]_ n ").
Local Notation " 'p[ r ]_ n" := (num r n) (at level 10, format " ''p[' r ]_ n").
Local Notation " 'q[ r ]_ n" := (denom r n) (at level 10, format " ''q[' r ]_ n").

Lemma denom_les m n r : (n <= m)%N -> ('q[r]_n <= 'q[r]_m)%Z.
Proof.
move=> /(subnK)<-.
elim: (_ - _)%N => [|k IH]; first by rewrite add0n; lia.
by apply: Z.le_trans (denom_le (k + n) _).
Qed.

Lemma num_ids m n r : 
  'a[r]_ n.+2 = 0%Z -> (n.+1 <= m)%N -> 'p[r]_m = 'p[r]_n.+1.
Proof.
move=> aZ /(subnK)<-.
elim: (_ - _)%N => //= k IH.
rewrite !addnS !addSn /= num_id -addnS //.
apply: elt_eq_0 aZ => /=.
by rewrite ltnS leq_addl.
Qed.

Lemma denom_ids m n r : 
  'a[r]_ n.+2 = 0%Z -> (n.+1 <= m)%N -> 'q[r]_m = 'q[r]_n.+1.
Proof.
move=> aZ /(subnK)<-.
elim: (_ - _)%N => //= k IH.
rewrite !addnS !addSn /= denom_id -addnS //.
apply: elt_eq_0 aZ => /=.
by rewrite ltnS leq_addl.
Qed.

Lemma eps_recZ r n : `{'e[r]_n} = 0 -> 'e[r]_n.+1 = 0.
Proof.
elim: n r => [r|n IH r].
  by rewrite eps_0 => H; rewrite eps_rec0.
case: (Req_EM_T `{r} 0) => H.
  rewrite !eps_rec0 // frac_inv; lra.
rewrite eps_rec // => H1.
rewrite eps_rec //.
by apply: IH.
Qed.

Lemma Zfloor_div_Z p q : 
  (0 < q)%Z -> `[ IZR p / IZR q] = (p / q)%Z.
Proof.
move=> qP.
apply: Zfloor_eq; split.
  apply: Rdiv_le_r; first by apply:(@IZR_lt 0).
  rewrite -mult_IZR; apply: IZR_le.
  by rewrite Zmult_comm; apply: Z_mult_div_ge; lia.
rewrite -(plus_IZR _ 1).
apply: Rdiv_lt_l; first by apply:(@IZR_lt 0).
rewrite -mult_IZR; apply: IZR_lt.
rewrite {1}(Z_div_mod_eq p q); try lia.
have : (0 <= p mod q < q)%Z; try lia.
by apply: Z_mod_lt; lia.
Qed.

Lemma frac_div_Z p q : 
  (0 < q)%Z -> (0 < p mod q)%Z -> 
  (1 / `{ IZR p / IZR q})%R = (IZR q / IZR (p mod q))%R.
Proof.
move=> qP mP.
case: (Z_mod_lt p q)=> [|Z1 Z2]; try lia.
have H : (p / q = IZR(p / q) + (p mod q) / q)%R.
  rewrite {1}(Z_div_mod_eq p q); try lia.
  rewrite plus_IZR mult_IZR /=.
  by field; apply: not_0_IZR; lia.
have H1 : (0 <= IZR (p mod q) / IZR q < 1)%R.
  split.
    apply: Rcomplements.Rdiv_le_0_compat.
      by apply: (@IZR_le 0); lia.
    by apply: (@IZR_lt 0); lia.
  apply: Rdiv_lt_l; try lra.
    by apply: (@IZR_lt 0); lia.
  by rewrite Rmult_1_l; apply: IZR_lt; lia.
rewrite H Rplus_comm frac_addz.
rewrite frac_inv //.
by field; split; apply: not_0_IZR; lia.
Qed.

Lemma mk_conv_list_rec_cor m (l :seq (Z * Z))  r n qmax (p q : Z) p1 q1 p2 q2 l1 :
   (0 < q < p)%Z ->
  'e[r]_m.+1 = (IZR p / IZR q)%R -> 'p[r]_m = p1 -> 'p[r]_m.+1 = p2 ->
  'q[r]_m = q1 -> 'q[r]_m.+1 = q2 ->
  (forall n1, (0 < n1 <= m.+1)%N -> List.In ('p[r]_n1, 'q[r]_n1) l) ->
   mk_conv_list_rec n m l qmax p q p1 q1 p2 q2 = Some l1 ->
  (forall n1, 0 < 'q[r]_n1 <= qmax -> List.In ('p[r]_n1, 'q[r]_n1) l1) /\
  (exists k, qmax < 'q[r]_k).
Proof.
elim: n m l r qmax p q p1 p2 q1 q2 l1 => //= n IH m l r qmax p q p1 p2 q1 q2 l1.
move=> qP He Hp1 Hp2 Hq1 Hq2 Hl.
have aE : 'a[r]_m.+2 = (p / q).
  rewrite elt_eps He.
  by apply: Zfloor_div_Z; lia.
rewrite Zdiv_eucl_unique.
have aNZ: 'a[r]_m.+2 <> 0.
  suff : (0 < p / q)%Z by lia.
  by apply: Z.div_str_pos; lia.
have-> : p / q * p2 + p1 = 'p[r]_m.+2.
  by rewrite -aE num_rec ?Hp1 ?Hp2 ?aE; try lia.
have-> : p / q * q2 + q1 = 'q[r]_m.+2.
  by rewrite -aE denom_rec ?Hq1 ?Hq2 ?aE; lia.
case: Z.leb_spec => [H2 | H2 [<-]]; last first.
  split; last by exists m.+2.
  move=> n1 Hn1.
  apply/Hl/andP; split.
    by case: (n1) Hn1 => //; rewrite denom_0; lia.
  case: leqP => // H3.
  suff: 'q[r]_m.+2 <= 'q[r]_n1 by lia.
  by apply: denom_les.
case: Zeq_bool_spec => [//|H3].
apply: IH (refl_equal _) _ => //.
- by case: (Z_mod_lt p q); lia.
- have H4 : (p / q = IZR(p / q) + (p mod q) / q)%R.
    rewrite {1}(Z_div_mod_eq p q); try lia.
    rewrite plus_IZR mult_IZR /=.
    by field; apply: not_0_IZR; lia.
  case: (Z_mod_lt p q)=> [|Z1 Z2]; try lia.
  rewrite eps_recT He.
    by apply: frac_div_Z; lia.
  rewrite H4 Rplus_comm frac_addz.
  have H5 : (0 <= IZR (p mod q) / IZR q < 1)%R.
    split.
      apply: Rcomplements.Rdiv_le_0_compat.
        by apply: (@IZR_le 0); lia.
      by apply: (@IZR_lt 0); lia.
    apply: Rdiv_lt_l; try lra.
      by apply: (@IZR_lt 0); lia.
    by rewrite Rmult_1_l; apply: IZR_lt; lia.
  rewrite frac_inv //.
  suff: (0 < (p mod q) / q)%R by lra.
  by apply: Rdiv_lt_0_compat; apply: (@IZR_lt 0); lia.
move=> n1 /andP[n1P]; rewrite leq_eqVlt => /orP[/eqP<-|H4].
  by apply: List.in_eq.
by apply: List.in_cons; apply: Hl; rewrite n1P.
Qed.

(* The denominator should fit in 54 bits *)
Definition maxn := Eval compute in 2 ^54 - 1.

(* mk_conv_list : returns the list of all the convergent of p/q 
                          the denominators fit in 54 and there is no more 
                          the size of the list is limited to n

*)
Definition mk_conv_list n p q :=
 let (a1, e1) := Z.div_eucl p q in
  let p0 := 1 in
  let q0 := 0 in
  let p1 := a1 in
  let q1 := 1 in
  mk_conv_list_rec n 0 [::(p1, q1)] maxn q e1 p0 q0 p1 q1.


Lemma mk_conv_list_cor n p q l :
  0 < p mod q -> 0 < q ->
  mk_conv_list n p q = Some l ->
  (forall n1, 0 < 'q[p/q]_n1 <= maxn -> List.In ('p[p/q]_n1, 'q[p/q]_n1) l)
 /\
  (exists k, maxn < 'q[p/q]_k).
Proof.
rewrite /mk_conv_list.
rewrite Zdiv_eucl_unique => mP qP H.
have H1 :  'q[IZR p / IZR q]_1 = 1.
  by rewrite denom_1.
case: (Z_mod_lt p q) => [|Z1 Z2]; try lia.
apply: mk_conv_list_rec_cor H1 _ H => //.
- rewrite eps_recT eps_0.
    by apply frac_div_Z; lia.  have {1}-> : (p / q = IZR(p / q) + (p mod q) / q)%R.
    rewrite {1}(Z_div_mod_eq p q); try lia.
    rewrite plus_IZR mult_IZR /=.
    by field; apply: not_0_IZR; lia.
  rewrite Rplus_comm frac_addz.
  have H5 : (0 <= IZR (p mod q) / IZR q < 1)%R.
    split.
      apply: Rcomplements.Rdiv_le_0_compat.
        by apply: (@IZR_le 0); lia.
      by apply: (@IZR_lt 0); lia.
    apply: Rdiv_lt_l; try lra.
      by apply: (@IZR_lt 0); lia.
    by rewrite Rmult_1_l; apply: IZR_lt; lia.
  rewrite frac_inv //.
  suff: (0 < (p mod q) / q)%R by lra.
  by apply: Rdiv_lt_0_compat; apply: (@IZR_lt 0); lia.
- rewrite num_1.
  by apply: Zfloor_div_Z.
move=> [|[|]] // _.
rewrite num_1 denom_1.
rewrite Zfloor_div_Z //.
apply: List.in_eq.
Qed.

Definition m52 := Eval compute in 2 ^ 52.
Definition m53m1 := Eval compute in 2 ^ 53 - 1.
Definition m53 := Eval compute in 2 ^ 53.
Definition m54m1 := Eval compute in 2 ^ 54 - 1.
Definition m16 := 10 ^ 16.

(* The conditions of Problem 1 *)
Definition check1 d h m1 n1 := 
  let m := m1 * d in
  let n := n1 * d in
  ((Zle_bool m52 m) && (Zle_bool m m53m1)) && 
  ((Zle_bool m53 n) && (Zle_bool n m54m1)) &&
  ((Zle_bool (-787) h) && (Zle_bool h 716)) &&
  (implb (Zle_bool m16 n) (Z.even n)).

Lemma check1_correct d h m1 n1 (m := m1 * d) (n := n1 * d) :
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   check1 d h m1 n1.
Proof.
rewrite /m /n => [] [H1 H2] [H3 H4] H5 H6.
rewrite /check1.
(do 7 (case: Zle_bool_spec => //=; try lia)) => V1 V2 V3 V4 V5 V6 V7.
by rewrite (H6 V1).
Qed.

Fixpoint check2 k d h m1 n1 := 
  if check1 d h m1 n1 then true
  else if k is k1.+1 then check2 k1 (d + 1) h m1 n1
       else false.

Lemma check2_correct k d d1 h m1 n1 (m := m1 * d1) (n := n1 * d1) :
   (d <= d1 <= d + (Z.of_nat k)) ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   check2 k d h m1 n1.
Proof.
rewrite /m /n => H1 H2 H3 H4 H5.
elim: k d H1 => /= [d H1|k IH d H1].
  replace d with d1 by lia.
  by have ->: check1 d1 h m1 n1 by apply: check1_correct.
have [V1|V2] : d = d1 \/ d + 1 <= d1 by lia.
  subst d.
  by have ->: check1 d1 h m1 n1 by apply: check1_correct => //; try lia.
suff -> : check2 k (d + 1) h m1 n1 by rewrite if_same.
by apply: IH => //; lia.
Qed.

Definition check h m1 n1 :=
  let d1 := Z.div m53 n1 in
  let d := Z.div m54m1 n1 in
  let k := Z.to_nat (d - d1) in check2 k d1 h m1 n1.

Lemma check_correct (h m1 n1 m n : Z) :
    Z.gcd m1 n1 = 1 -> 0 < n -> 0 < n1 -> 
   (m / n = m1 / n1)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   check h m1 n1.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7 H8.
have nP: (0 < n)%R by apply: (@IZR_lt 0).
have n1P: (0 < n1)%R by apply: (@IZR_lt 0).
have F0 : m * n1 = m1 * n.
  apply: eq_IZR.
  rewrite !mult_IZR.
  replace (m : R) with (m/n * n)%R by (field; lra).
  by rewrite H4; field; lra.
have F1 : n = n1 * (n / n1).
  have F : (n1 | n).
    rewrite Z.gcd_comm in H1.
    apply: Z.gauss H1; rewrite -F0.
    by exists m; lia. 
  by apply: Znumtheory.Zdivide_Zdiv_eq.
have F2 : m = m1 * (n / n1).
  apply: Z.mul_reg_l (_ : n1 <> 0) _; try lia.
  apply: etrans (_ : m1 * (n1 * (n / n1)) = _).
    by rewrite -F1; lia.
  by ring.
have F3 : m53 / n1 <= n / n1 <= m53 / n1 + Z.of_nat (Z.to_nat (m54m1 / n1 - m53 / n1)).
  rewrite  Z2Nat.id; last first.
  by apply/Zle_minus_le_0/Z_div_le; lia.
  replace  (m53 / n1 + (m54m1 / n1 - m53 / n1)) with (m54m1 / n1) by lia.
  split; first by apply: Zdiv_le_upper_bound; lia.
  by apply: Zdiv_le_lower_bound; lia.
by apply: check2_correct F3 _ _ _ _; rewrite -?F1; try lia.
Qed.

Definition verify_best h1 p q bp bq m1 n1 := 
  let bp1 := Z.abs (p * n1 - m1 * q) in
  let bq1 := n1 * q in
  if Zle_bool (bp * bq1) (bp1 * bq) then true
  else 
    if Zeq_bool bp1 0 then true else
    if check h1 m1 n1 then false else true.

Lemma verify_best_correct (m n h bp bq p q m1 n1 : Z) :
    Z.gcd m1 n1 = 1 -> 0 < bq -> 0 < q -> 0 < n1 -> 0 < n ->
   (m / n = m1 / n1)%R -> (p / q <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m1 / n1) <  bp / bq)%R ->
   verify_best h p q bp bq m1 n1 = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7 H8 H9 H10 H11 H12.
have bqP: (0 < bq)%R by apply: (@IZR_lt 0).
have n1P: (0 < n1)%R by apply: (@IZR_lt 0).
have nP: (0 < n)%R by apply: (@IZR_lt 0).
have qP: (0 < q)%R by apply: (@IZR_lt 0).
rewrite /verify_best.
case: Zle_bool_spec => V1.
  have := IZR_le _ _ V1.
  rewrite !mult_IZR abs_IZR minus_IZR !mult_IZR.
  rewrite -[IZR bq]Rabs_right; try lra.
  rewrite -Rabs_mult.
  replace (bp * (n1 * q))%R with 
          (bp / bq * (bq * n1 * q))%R by (field; lra).
  replace ((p * n1 - m1 * q) * bq)%R with
          ((p / q - m1 / n1) * (bq * n1 * q))%R; last first.
    by field; split; lra.
  have F : (0 < bq * n1 * q)%R by apply: Rmult_lt_0_compat; nra.
  rewrite Rabs_mult (Rabs_right _ (Rle_ge _ _ (Rlt_le _ _ F))).
  by move/Rmult_le_reg_r=> /(_ F); lra.
case: Zeq_bool_spec => V2.
  have := IZR_eq _ 0%Z V2.
  rewrite abs_IZR minus_IZR !mult_IZR.
  have F : (0 < q * n1)%R by nra.
  replace (p * n1 - m1 * q)%R with ((p / q - m1 / n1) * (q * n1))%R; last first.
    by field; lra.
  rewrite Rabs_mult => /Rmult_integral[/Rcomplements.Rabs_eq_0|]; try lra.
  by rewrite Rabs_right; lra.
suff->: check h m1 n1 by [].
by apply: check_correct H8 H9 H10 H11.
Qed.

Fixpoint verify_bestl h p q bp bq l := 
 if l is (m1, n1) :: l1 then
   if verify_best h p q bp bq m1 n1 then
      verify_bestl h p q bp bq l1
   else false
 else true.

Lemma verify_bestl_correct l (m n h bp bq p q m1 n1 : Z) :
    List.In (m1, n1) l ->
    Z.gcd m1 n1 = 1 -> 0 < bq -> 0 < q -> 0 < n1 -> 0 < n ->
   (m / n = m1 / n1)%R -> (p / q <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m1 / n1) <  bp / bq)%R ->
   verify_bestl h p q bp bq l = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7 H8 H9 H10 H11 H12 H13.
elim: l H1 => //= p1 l IH [->/=|V] //.
  suff ->: verify_best h p q bp bq m1 n1 = false by [].
  by apply: verify_best_correct H9 H10 H11 H12 H13.
suff->: verify_bestl h p q bp bq l = false.
  by case: p1 => p1 q1; rewrite if_same.
by apply: IH.
Qed.

(* Verify the best approximation of 5 ^ phi h / 2 ^ h *)
Definition verify_besth h bp bq :=
  let p := 
     if Zle_bool 0 h then 5 ^  ((h * 225799) / (2 ^19)) else 2 ^ (-h)  in
  let q := if Zle_bool 0 h then 2 ^ h else (5 ^  (((-h) * 225799) / (2 ^19) + 1)) in
  if mk_conv_list 1000 p q is Some l then 
    verify_bestl h p q bp bq l else false.


Lemma ext_elim r q : 
  0 <= q <= maxn -> (exists m, maxn <'q[r]_m) -> (exists k, 'q[r]_k <= q < 'q[r]_k.+1).
Proof.
move=> H1 [m Hm].
have : 0 <= q < 'q[r]_m by lia.
elim: (m) => [|m1 IH].
  rewrite denom_0; lia.
case: (Z_le_dec ('q[r]_m1) q).
  by move=> *; exists m1; lia.
move=> H2 H3.
have [k Hk] : exists k : nat,  'q[r]_k <= q <  'q[r]_k.+1.
  by apply: IH; lia.
by exists k; lia.
Qed.

Definition bp1 := 
  9583723647151497715748855985336925387350397478403319344175936715887917002529499793646704660169505172304500318539059654998877401306169390840117819918935869930600601291.
Definition bq1 :=
  150766046227125863784130855355431583102341132913540640713808524072186206546962278594854444134050092053000527172816154049523115277896304282171615809979653733004672632613248399492700530151704033193099264.

Lemma bp1_bound : (bp1 / bq1 <= / 2 ^ 109)%R.
Proof.
rewrite -[X in (_ <= X)%R]Rmult_1_l.
apply: Rdiv_le_Rdiv.
- rewrite (pow_IZR 2 109).
  apply: (IZR_lt 0).
  by compute.
- apply: (IZR_lt 0).
  by compute.
rewrite (pow_IZR 2 109) -mult_IZR -(mult_IZR 1).
by apply: IZR_le; compute.
Qed.

Lemma Zgcd_num n r : 'a[r]_ n.+1 <> 0%Z ->
   Z.gcd ('p[r]_n) ('q[r]_n) = 1.
Proof.
move=> /bezout_num.
rewrite Zpower_nat_sign_odd.
case: odd => H; apply: Z.bezout_1_gcd.
  by exists (-'q[r]_n.+1); exists ('p[r]_n.+1); lia.
by exists ('q[r]_n.+1); exists (-'p[r]_n.+1); lia.
Qed.

Lemma verify_besth_correct (h m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
    0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (54 <= h <= 716)  ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m / n) < bp1 / bq1)%R -> 
   verify_besth h bp1 bq1 = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7.
rewrite /verify_besth.
case: Zle_bool_spec=> hP; last by lia.
set p1 := 5 ^ _.
set q1 := 2 ^ _.
have Ep1 : IZR p1 = p.
  rewrite /p1 /p prop2_1; try lia.
  have->: ZnearestA (2 ^ 19 * log5 2) =  225799%Z.
    apply: Znearest_imp.
    by rewrite /log5 /logn ![radix_val _]/=; interval.
  have F : 0 <= `[ IZR (h * 225799) / IZR (2 ^ 19)].
    rewrite -(ZfloorZ 0).
    apply: Zfloor_le.
    apply: Rcomplements.Rdiv_le_0_compat.
      by apply: (IZR_le 0); lia.
    by apply: (IZR_lt 0); lia.
  rewrite -Zfloor_div_Z //.
  rewrite -[`[_]]Z2Nat.id //.
  rewrite -pow_IZR.
  rewrite Interval_missing.pow_powerRZ.
  replace (IZR 5) with 5%R by lra.
  rewrite Z2Nat.id //.
  rewrite mult_IZR.
  by rewrite (bpow_opp _ 19).
have Eq1 : IZR q1 = q.
  rewrite /q1 /q.
  rewrite -[h]Z2Nat.id //.
  rewrite -pow_IZR //.
  rewrite Interval_missing.pow_powerRZ.
  by replace (IZR 2) with 2%R by lra.
case E : mk_conv_list => [l|] //.
have F0 : 0 < q1 by apply: Z.pow_pos_nonneg.
have F1 : 0 < p1 mod q1.
  have F1 : Z.gcd p1 q1 = 1.
    rewrite Z.gcd_comm; apply: gcd_pow_2_5; try lia.
    by apply:Z_div_pos; lia.
  suff F2 : p1 mod q1 <> 0.
     suff : 0 <= p1 mod q1 < q1 by lia.
     by apply: Z_mod_lt; lia.
  move=> H.
  have F3 : (q1 | p1).
    by apply: Znumtheory.Zmod_divide; lia.
  have : Z.gcd q1 p1 = q1.
    by apply Z.divide_gcd_iff; try lia.
  rewrite Z.gcd_comm F1.
  suff: 2 ^ 1 < q1 by lia.
  by apply: Z.pow_lt_mono_r; lia.
have [V1 V2] := mk_conv_list_cor _ _ _ _ F1 F0 E.
have F3 : 0 <= n <= maxn.
  by move: H4; rewrite /m53 /m54m1 /maxn; lia.
have [k Hk] := ext_elim _ _ F3 V2.
have F4 : 0 <  'q[IZR p1 / IZR q1]_k.
  case: (k) Hk => [|k1 _].
    rewrite denom_1.
    by move: H4; rewrite /m53 /m54m1 /maxn; lia.
  by apply: denom_spos.
have F5 : (m / n)%R = ( 'p[p1 / q1]_k /  'q[p1 / q1]_k)%R.
  apply: conv_2q2 => //.
  rewrite Ep1 Eq1.
  apply: Rlt_le_trans H7 _.
  apply: Rle_trans bp1_bound _.
  rewrite [X in (_ <= X)%R]Rmult_1_l.
  apply: Rinv_le.
    suff: (0 < IZR n ^ 2)%R by nra.
    by apply/pow_lt/(IZR_lt 0).
  rewrite pow_IZR -(mult_IZR 2) (pow_IZR 2 109).
  apply: IZR_le.
  replace (2 ^ Z.of_nat 109) with (2 * (2 ^54) ^ 2); last first.
   by compute.
  suff: n  <= 2 ^ 54 by nia.
  by move: H4; rewrite /m53 /m54m1 /maxn; lia.
have F6 : List.In ('p[p1 / q1]_k,'q[p1 / q1]_k) l.
  by apply: V1; lia.
have F7 : (Rabs (p1 / q1 - ( 'p[p1 / q1]_k /  'q[p1 / q1]_k)%R) < bp1 / bq1)%R.
  by rewrite -F5 Ep1 Eq1.
have F8 : -787 <= h <= 716 by lia.
apply: verify_bestl_correct H3 H4 F8 H6 F7 => //.
  have kP : (0 < k)%N.
     case: (k) Hk => [|//].
     by rewrite denom_1; lia.
  apply: Zgcd_num.
  rewrite -(prednK kP)=> /halton_eq_0.
  rewrite /halton=> /Rmult_integral[].
    case: (Rpower_signE k.-1.+2); lra.
  rewrite prednK //.
  move=> /Rminus_diag_uniq_sym HH; case: H2.
  rewrite F5 -Ep1 -Eq1 HH.
  by field; split; apply: (IZR_neq _ 0); lia.
by rewrite Ep1 Eq1.
Qed.

Fixpoint verify_bestf n h bp bq :=
  if verify_besth h bp bq then
    if n is n1.+1 then verify_bestf n1 (h + 1) bp bq
    else true
  else false.

Lemma verify_bestf_correct k (h h1 m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
   (54 <= h1 <= h) ->
   (h <= h1 + Z.of_nat k <= 716) ->
   0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m / n) < bp1 / bq1)%R -> 
   verify_bestf k h1 bp1 bq1 = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7 H8.
elim: k h1 H1 H2 => //= [h1 H1 H2| k IH h1 H1 H2].
  suff ->: verify_besth h1 bp1 bq1 = false by [].
  replace h1 with h by lia.
  by apply: verify_besth_correct H8 => //; lia.
have [V1|V2] : h  = h1 \/ h1 + 1 <= h by lia.
  subst h.
  suff ->: verify_besth h1 bp1 bq1 = false by [].
  by apply: verify_besth_correct H8 => //; lia.
suff -> : verify_bestf k (h1 + 1) bp1 bq1 = false by rewrite if_same.
by apply: IH; lia.
Qed.

Lemma pos_correct (h m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
   (54 <= h <= 716) ->
   0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (m16 <= n -> Z.even n) ->
   (bp1 / bq1 <= Rabs (p / q - m / n))%R.
Proof.
have F : verify_bestf (716 - 54)%N 54 bp1 bq1 = true.
  by native_cast_no_check (refl_equal true).
move=> H1 H2 H3 H4 H5 H6.
case: (Rle_lt_dec (bp1 / bq1) (Rabs (p / q - IZR m / IZR n))) => HH.
  exact: HH.
have: verify_bestf (716 - 54)%N 54 bp1 bq1 = false.
  apply: verify_bestf_correct H2 H3 H4 H5 H6 HH.
    by lia.
  by rewrite [Z.of_nat _]/=; lia.
by rewrite F.
Time Qed.

Definition bp2 := 112529423171232400134835569054963071809061903761868569039360251397.
Definition bq2 := 1860451484274037501915433537364990769170345094056549724607571572043696050968719646334648132324218750.

Lemma bp2_bound : (bp2 / bq2 <= / 2 ^ 109)%R.
Proof.
rewrite -[X in (_ <= X)%R]Rmult_1_l.
apply: Rdiv_le_Rdiv.
- rewrite (pow_IZR 2 109).
  apply: (IZR_lt 0).
  by compute.
- apply: (IZR_lt 0).
  by compute.
rewrite (pow_IZR 2 109) -mult_IZR -(mult_IZR 1).
by apply: IZR_le; compute.
Qed.

Lemma verify_besth_correct_bis (h m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
    0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (-787 <= h <= -54)  ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m / n) < bp2 / bq2)%R -> 
   verify_besth h bp2 bq2 = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7.
rewrite /verify_besth.
case: Zle_bool_spec=> hP; first by lia.
set p1 := 2 ^ _.
set q1 := 5 ^ _.
have Eq1 : IZR q1 = (1 / p)%R.
  rewrite /q1 /p prop2_1; try lia.
  have->: ZnearestA (2 ^ 19 * log5 2) =  225799%Z.
    apply: Znearest_imp.
    by rewrite /log5 /logn ![radix_val _]/=; interval.
  rewrite -mult_IZR.
  rewrite (bpow_opp _ 19) //.
  rewrite -IZR_Zpower //.
  rewrite [Zfloor _]Zfloor_div_Z //.
  have F : 0 <= - h * 225799 / 2 ^ 19.
     by apply: Z_div_pos; lia.
  rewrite -[_ + 1]Z2Nat.id //; try lia.
  rewrite -pow_IZR.
  rewrite Interval_missing.pow_powerRZ.
  rewrite Z2Nat.id //; try lia.
  rewrite -Zopp_mult_distr_l.
  rewrite Z_div_nz_opp_full=> [|HH].
    set u := _ + _; set v := _ / _.
    have ->: u = - v by  rewrite /u /v /=; lia.
    replace (IZR 5) with 5%R by lra.
    by case: (v) => [|u1|u1] /=; field; apply: pow_nonzero; lra.
  suff : 2 ^ 19 <= - h by lia.
  apply: Z.divide_pos_le; try lia.
  apply: Znumtheory.Zdivide_opp_r.
  apply: (Z.gauss _ _ _ _ (_ : Z.gcd _ 225799 = 1)).
    rewrite Zmult_comm.
    apply: Znumtheory.Zmod_divide; lia.
  by compute.
have Ep1 : IZR p1 = (1 / q)%R.
  rewrite /p1 /q.
  rewrite -[- h]Z2Nat.id //; try lia.
  rewrite -pow_IZR //.
  rewrite Interval_missing.pow_powerRZ.
  rewrite Z2Nat.id //; try lia.
  replace (IZR 2) with 2%R by lra.
  by case: (h) => //= [|u|u]; field; apply: pow_nonzero; lra.
case E : mk_conv_list => [l|] //.
have K : 0 < - h * 225799 / 2 ^ 19.
  by apply: Z.div_str_pos; lia.
have F0 : 0 < q1.
  by apply: Z.pow_pos_nonneg; lia.
have F1 : 0 < p1 mod q1.
  have F1 : Z.gcd p1 q1 = 1.
    by apply: gcd_pow_2_5; lia.
  suff F2 : p1 mod q1 <> 0.
     suff : 0 <= p1 mod q1 < q1 by lia.
     by apply: Z_mod_lt; lia.
  move=> H.
  have F3 : (q1 | p1).
    by apply: Znumtheory.Zmod_divide; lia.
  have : Z.gcd q1 p1 = q1.
    by apply Z.divide_gcd_iff; try lia.
  rewrite Z.gcd_comm F1.
  suff: 5 ^ 1 < q1 by lia.
  by apply: Z.pow_lt_mono_r; lia.
have [V1 V2] := mk_conv_list_cor _ _ _ _ F1 F0 E.
have F3 : 0 <= n <= maxn.
  by move: H4; rewrite /m53 /m54m1 /maxn; lia.
have [k Hk] := ext_elim _ _ F3 V2.
have F4 : 0 <  'q[IZR p1 / IZR q1]_k.
  case: (k) Hk => [|k1 _].
    rewrite denom_1.
    by move: H4; rewrite /m53 /m54m1 /maxn; lia.
  by apply: denom_spos.
have Ep1q1 : (IZR p1 / IZR q1 = p / q)%R.
  rewrite Ep1 Eq1.
  by field; split; apply: powerRZ_NOR; lra.
have F5 : (m / n)%R = ( 'p[p1 / q1]_k /  'q[p1 / q1]_k)%R.
  apply: conv_2q2 => //.
  rewrite Ep1q1.
  apply: Rlt_le_trans H7 _.
  apply: Rle_trans bp2_bound _.
  rewrite [X in (_ <= X)%R]Rmult_1_l.
  apply: Rinv_le.
    suff: (0 < IZR n ^ 2)%R by nra.
    by apply/pow_lt/(IZR_lt 0).
  rewrite pow_IZR -(mult_IZR 2) (pow_IZR 2 109).
  apply: IZR_le.
  replace (2 ^ Z.of_nat 109) with (2 * (2 ^54) ^ 2); last first.
   by compute.
  suff: n  <= 2 ^ 54 by nia.
  by move: H4; rewrite /m53 /m54m1 /maxn; lia.
have F6 : List.In ('p[p1 / q1]_k,'q[p1 / q1]_k) l.
  by apply: V1; lia.
have F7 : (Rabs (p1 / q1 - ( 'p[p1 / q1]_k /  'q[p1 / q1]_k)%R) < bp2 / bq2)%R.
  by rewrite -F5 Ep1q1.
have F8 : -787 <= h <= 716 by lia.
apply: verify_bestl_correct H3 H4 F8 H6 F7 => //.
  have kP : (0 < k)%N.
     case: (k) Hk => [|//].
     by rewrite denom_1; lia.
  apply: Zgcd_num.
  rewrite -(prednK kP)=> /halton_eq_0.
  rewrite /halton=> /Rmult_integral[].
    case: (Rpower_signE k.-1.+2); lra.
  rewrite prednK //.
  move=> /Rminus_diag_uniq_sym HH; case: H2.
  rewrite F5 -Ep1q1 HH.
  by field; split; apply: (IZR_neq _ 0); lia.
by rewrite Ep1q1.
Qed.

Lemma verify_bestf_correct_bis k (h h1 m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
   (-787 <= h1 <= h) ->
   (h <= h1 + Z.of_nat k <= -54) ->
   0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (m16 <= n -> Z.even n) ->
   (Rabs (p / q - m / n) < bp2 / bq2)%R -> 
   verify_bestf k h1 bp2 bq2 = false.
Proof.
move=> H1 H2 H3 H4 H5 H6 H7 H8.
elim: k h1 H1 H2 => //= [h1 H1 H2| k IH h1 H1 H2].
  suff ->: verify_besth h1 bp2 bq2 = false by [].
  replace h1 with h by lia.
  by apply: verify_besth_correct_bis H8 => //; lia.
have [V1|V2] : h  = h1 \/ h1 + 1 <= h by lia.
  subst h.
  suff ->: verify_besth h1 bp2 bq2 = false by [].
  by apply: verify_besth_correct_bis H8 => //; lia.
suff -> : verify_bestf k (h1 + 1) bp2 bq2 = false by rewrite if_same.
by apply: IH; lia.
Qed.

Lemma neg_correct (h m n : Z) (p := powerRZ 5 (phi h)) (q := powerRZ 2 h) :
   (-787 <= h <= -54) ->
   0 < n ->
   (p / q  <> m / n)%R ->
   (m52 <= m <= m53m1) -> (m53 <= n <= m54m1) ->
   (m16 <= n -> Z.even n) ->
   (bp2 / bq2 <= Rabs (p / q - m / n))%R.
Proof.
have F : verify_bestf (787 - 54)%N (-787) bp2 bq2 = true.
  by native_cast_no_check (refl_equal true).
move=> H1 H2 H3 H4 H5 H6.
case: (Rle_lt_dec (bp2 / bq2) (Rabs (p / q - IZR m / IZR n))) => HH.
  exact: HH.
have: verify_bestf (787 - 54)%N (-787) bp2 bq2 = false.
  apply: verify_bestf_correct_bis H2 H3 H4 H5 H6 HH.
    by lia.
  by rewrite [Z.of_nat _]/=; lia.
by rewrite F.
Time Qed.

(* Algorithm to get bp1 bq1 bp2 bq2 

(* Check if the (p/q - m1/n1) is better than what is stored in v *)
Definition check_best h1 p q v m1 n1 := 
  let bp1 := Z.abs (p * n1 - m1 * q) in
  let bq1 := n1 * q in
  if v is Some (h, m, n, bp, bq) then 
     if (Zlt_bool (bp1 * bq) (bp * bq1)) then
       if (check h1 m1 n1) &&  (Zlt_bool 0 bp1) then
         Some (h1, m1, n1, bp1, bq1) else v
     else v else
   if (check h1 m1 n1) &&  (Zlt_bool 0 bp1) then
         Some (h1, m1, n1, bp1, bq1) else v.

(* Find the best approximation of p/q from the list l *)
Fixpoint bestl h p q v l := 
 if l is (m1, n1) :: l1 then
   let: v1 := check_best h p q v m1 n1 in
   bestl h p q v1 l1
 else v.

(* Find the best approximation of 5 ^ phi h / 2 ^ h *)
Definition besth h v :=
  let p := 
     if Zle_bool 0 h then 5 ^  ((h * 225799) / (2 ^19)) else 2 ^ (-h)  in
  let q := if Zle_bool 0 h then 2 ^ h else (5 ^  (((-h) * 225799) / (2 ^19) + 1)) in
  if mk_conv_list 1000 p q is Some l then 
    Some (bestl h p q v l) else None.
 
(* Find the best approximation of 5 ^ phi x / 2 ^ x for  h <= x <= h + n *)  
Fixpoint find_best n h v :=
  let v1 := besth h v in
  if v1 is Some r then 
    if n is n1.+1 then find_best n1 (h + 1) r
    else Some v1
  else None.

Time Compute find_best 1%nat 612 None.
Definition best :=
Some
               (612, 3521275406171921, 8870461176410409,
               9583723647151497715748855985336925387350397478403319344175936715887917002529499793646704660169505172304500318539059654998877401306169390840117819918935869930600601291,
               150766046227125863784130855355431583102341132913540640713808524072186206546962278594854444134050092053000527172816154049523115277896304282171615809979653733004672632613248399492700530151704033193099264).
q

(* 140 s *)
Time Compute find_best (787 - 54)%nat (-787) None.

*)

End FracComp.
