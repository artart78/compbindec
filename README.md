# Formalization of the correctness of comparison algorithms between binary64 and decimal64

This repository contains a formalization of the article available here:
https://hal.archives-ouvertes.fr/hal-01512294.

Its aim is to formalize algorithms which do an exact comparison between binary64
and decimal64 floating-point numbers.

## Organization of the code

The code is splitted into two files, `util.v` which contains general results,
mostly about real calculus, `rfrac` which contains general results about
continued fraction, `frac.v` which contains the computation using continued
fraction, `compformula.v` which contains the computations using the interval
tactic, and `compbindec.v`, which contains the actual
formalization of the paper.

## Compilation

To compile, you need flocq, coq-interval and ssreflect. You can install everything
using, first, if you didn't yet install Coq (through opam):
```bash
opam repo add coq-released https://coq.inria.fr/opam/released
```
and then:
```bash
opam install coq-mathcomp-ssreflect coq-flocq coq-interval
```

You can then compile the formalization by using:
```bash
coqc util.v rfrac.v frac.v compformula.v compbindec.v
```
(note you need to compile all the other files before reading the `compbindec.v`
interactively).
