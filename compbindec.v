From Flocq Require Import Core.
From mathcomp Require Import all_ssreflect.
From Interval Require Import Interval_tactic.

Require Import Psatz compformula util frac.

Open Scope Z_scope.

Section CompBinDec.

Variable x2 : float radix2.
Variable x10 : float radix10.

(* II. First step: eliminating the "simple cases" *)

Definition M2 := Fnum x2.
Definition e2 := Fexp x2 + 52. 

Definition M10 := Fnum x10.
Definition e10 := Fexp x10 + 15.

(* Initial normalization *)
Hypothesis M2_bound : 2 ^ 52 <= M2 <= 2 ^ 53 - 1.
Hypothesis M10_bound : 1 <= M10 <= 10 ^ 16 - 1.
Hypothesis e2_bound : -1074 <= e2 <= 1023.
Hypothesis e10_bound : -383 <= e10 <= 384.

Definition v : Z := 53 - (log2z M10).

Lemma v_eq : v = 53 - `[log2 M10].
Proof.
  by rewrite /v log2z_correct; last by lia.
Qed.

Lemma v_bound : 0 <= v <= 53.
Proof.
rewrite v_eq; split; last first.
  apply/Z.le_sub_nonneg/Zfloor_lub.
  rewrite /= -(logn_1 radix2).
  apply: logn_le; split; [lra| ].
  by apply: IZR_le; lia.
apply: Zle_minus_le_0.
apply: (Z.le_trans _ `[log2 (IZR (10^16 - 1))]).
apply: Zfloor_le.
  apply: logn_le; split.
    apply: IZR_lt.
    by apply: (Z.lt_le_trans _ 1); lia.
  by apply: IZR_le; lia.
apply: Zfloor_leq.
rewrite -[IZR (53 + 1)](logn_inv radix2).
apply: logn_lt.
apply: IZR_lt=> //.
rewrite -IZR_Zpower //; exact: IZR_lt.
Qed.

Lemma norm2_bound : 2^53 <= 2^v * M10 <= 2^54 - 1.
Proof.
have ln_M10: ln M10 = (log2 M10 * ln 2)%R.
  rewrite /log2 /logn /=.
  by field; apply/Rgt_not_eq/ln_gt_0; lra.
split.
  (* 2 ^ 53 <= 2 ^ v * M10 *)
  apply: le_IZR.
  rewrite mult_IZR -[2]/(radix2: Z) !IZR_Zpower=> //; last first.
    by have := v_bound; lia.
  apply: (Rle_trans _ (bpow radix2 v * bpow radix2 `[log2 M10])).
    rewrite -bpow_plus.
    by apply: Req_le; congr (bpow radix2 _); rewrite v_eq; ring.
  apply: Rmult_le_compat_l; first by apply: bpow_ge_0.
  rewrite bpow_exp -[X in (_ <= X)%R]exp_ln.
    apply: exp_le.
    rewrite ln_M10 /=.
    apply: Rmult_le_compat_r; first by apply: ln_ge_0; lra.
    by apply: Zfloor_lb.
  by apply: IZR_lt; lia.
(* 2 ^ v * M10 <= 2 ^ 54 - 1 *)
apply/Z.lt_succ_r/lt_IZR.
rewrite mult_IZR plus_IZR -[2]/(radix2: Z) !IZR_Zpower=> //; last first.
  by have := v_bound; lia.
apply: (Rlt_le_trans _ (bpow radix2 v * bpow radix2 (`[log2 M10] + 1))).
  apply: Rmult_lt_compat_l; first by apply: bpow_gt_0.
  rewrite bpow_exp -[X in (X < _)%R]exp_ln.
    apply: exp_increasing.
    rewrite ln_M10 /= plus_IZR /=.
    apply: Rmult_lt_compat_r; first by apply: ln_gt_0; lra.
    by apply: Zfloor_ub.
apply: (IZR_lt 0); lia.
rewrite -bpow_plus.
have ->: v + (`[log2 M10] + 1) = 54 by rewrite v_eq; ring.
rewrite -plus_IZR -IZR_Zpower //.
exact: IZR_le.
Qed.

Definition m : Z := M2.
Definition h : Z := v + e2 - e10 - 37.
(* 2 ^ v corresponds to what we think only when v >= 0, which is proven above *)
Definition n : Z := M10 * 2 ^ v.
Definition g : Z := e10 - 15.

Lemma x2_eq : F2R x2 = (m * (bpow radix2 h) * (bpow radix2 (g - v)))%R.
Proof.
rewrite Rmult_assoc -bpow_plus /F2R /m /M2.
by congr (_ * (_ _))%R; rewrite /h /g /e2; ring.
Qed.

Lemma x10_eq : F2R x10 = (n * (bpow radix5 g) * (bpow radix2 (g - v)))%R.
Proof.
rewrite /n mult_IZR /F2R /M10 2!Rmult_assoc.
congr ((_ * _)%R).
rewrite -bpow_10 Rmult_comm [(_ * (_ * _))%R]Rmult_comm Rmult_assoc.
have ->: g = Fexp x10 by rewrite /g /e10 Z.add_simpl_r.
congr ((_ * _)%R).
rewrite (IZR_Zpower radix2) -?bpow_plus ?Z.sub_add //.
have := v_bound; lia.
Qed.

Lemma m_bound : 2 ^ 52 <= m <= 2 ^ 53 - 1.
Proof. exact: M2_bound. Qed.

Lemma n_bound : 2 ^ 53 <= n <= 2 ^ 54 - 1.
Proof. by rewrite /n Zmult_comm; exact: norm2_bound. Qed.

Lemma g_bound : -398 <= g <= 369.
Proof. by rewrite /g; lia. Qed.

Lemma h_bound : -1495 <= h <= 1422.
Proof. rewrite /h; have H := v_bound; lia. Qed.

(* Property 1 *)
Lemma easycomp_lt : g < phi h -> (F2R x10 < F2R x2)%R.
Proof.
move=> g_lt_phih.
rewrite x10_eq x2_eq.
apply: Rmult_lt_compat_r; first by apply: bpow_gt_0.
apply: (Rlt_trans _ (IZR (2 ^ 54) * (bpow radix5 g))%R).
  apply: Rmult_lt_compat_r; first by apply bpow_gt_0.
  apply: IZR_lt.
  by have H := n_bound; lia.
apply: (Rle_lt_trans _ ((4 / 5) * IZR (2 ^ 52) * bpow radix2 h)); last first.
  apply: Rmult_lt_compat_r; first by apply: bpow_gt_0.
  apply: (Rlt_le_trans _ (1 * IZR (2 ^ 52))).
    apply: Rmult_lt_compat_r; try lra.
    apply: IZR_lt; lia.
  rewrite Rmult_1_l.
  by apply: IZR_le; have := m_bound; lia.
have ->: ((4 / 5) * (IZR (2 ^ 52)) = IZR (2 ^ 54) * / 5)%R.
  have ->: 2 ^ 54 = 4 * 2 ^ 52 by [].
  by rewrite mult_IZR /=; field.
rewrite Rmult_assoc.
apply Rmult_le_compat_l=> /=; [apply: IZR_le; rewrite /Z.pow_pos /=; lia| ].
rewrite /phi in g_lt_phih.
have ->: (/ 5 * bpow radix2 h = Rpower 5 ((IZR h) * log5 2 - 1))%R.
  rewrite Rpower_plus Rpower_Ropp /Rpower Rmult_1_l exp_ln; [| lra].
  rewrite Rmult_assoc.
  suff ->: (log5 2 * ln 5)%R = ln 2 by rewrite bpow_exp /=; lra.
  rewrite /log5 /logn /=; field.
  apply: Rgt_not_eq.
  by apply: ln_gt_0; lra.
rewrite bpow_exp /Rpower /=.
suff g_le_Pphih: (IZR g <= IZR h * log5 2 - 1)%R.
  apply/exp_le/Rmult_le_compat_r=> //.
  by apply: ln_ge_0; lra.
apply: (Rle_trans _ (IZR (`[IZR h * log5 2] - 1))).
  apply/IZR_le/(Zplus_le_reg_r _ _ 1)/Zlt_le_succ.
  rewrite Z.sub_add.
  by apply g_lt_phih.
rewrite minus_IZR /=.
apply: Rplus_le_compat_r.
exact: Zfloor_lb.
Qed.

Lemma easycomp_gt : g > phi h -> (F2R x2 < F2R x10)%R.
Proof.
move=> g_gt_phih.
rewrite x2_eq x10_eq.
apply: Rmult_lt_compat_r.
apply: bpow_gt_0.
have Hm: (IZR m <= (2^53 - 1))%R.
  have ->: (2 ^ 53 - 1)%R = IZR (2 ^ 53 - 1) by rewrite /=; lra.
  apply: IZR_le.
  have Hm := m_bound; lia.
have Hn: (2 ^ 53 <= IZR n)%R.
  have ->: (2 ^ 53)%R = IZR (2 ^ 53) by rewrite pow_IZR.
  apply: IZR_le; have Hn := n_bound; lia.
suff Hmain: ((2 ^ 53 - 1) * bpow radix2 h < 2 ^ 53 * bpow radix5 g)%R.
  apply: (Rle_lt_trans _ ((2 ^ 53 - 1) * bpow radix2 h)).
    by apply: Rmult_le_compat_r; first by apply: bpow_ge_0.
  apply: (Rlt_le_trans _ ((2 ^ 53) * bpow radix5 g))=> //.
  by apply: Rmult_le_compat_r; first by apply: bpow_ge_0.
suff H': (bpow radix2 h < bpow radix5 g)%R.
  apply: (Rlt_trans _ (2 ^ 53 * bpow radix2 h)).
  apply: Rmult_lt_compat_r; [apply: bpow_gt_0| lra].
  by apply: Rmult_lt_compat_l=> //; lra.
rewrite !bpow_exp.
apply: exp_increasing=> /=.
rewrite [(IZR g * _)%R]Rmult_comm.
apply: Rmult_lt_r; first by apply: ln_gt_0; lra.
rewrite /Rdiv Rmult_assoc.
have ->: ((ln 2 * / ln 5) = log5 2)%R by [].
rewrite /phi in g_gt_phih.
apply: (Rlt_le_trans _ (IZR (`[IZR h * log5 2] + 1))).
  by rewrite plus_IZR; exact: Zfloor_ub.
by apply: IZR_le; lia.
Qed.

(* Algorithm 1 *)
Definition easycomp : comparison :=
  let h := v + e2 - e10 - 37 in
  let g := e10 - 15 in
  let phih := (225799 * h) / (2 ^ 19) in
  if g <? phih then Gt
  else if g >? phih then Lt
  else Eq.

Theorem easycomp_correct :
  (easycomp = Lt -> (F2R x2 < F2R x10)%R) /\
  (easycomp = Gt -> (F2R x2 > F2R x10)%R) /\
  (easycomp = Eq <-> g = phi h).
Proof.
have Hphi: phi h = 225799 * h / 2 ^ 19.
  rewrite -Zfloor_div //.
  have ->: IZR (2 ^ 19) = bpow radix2 19 by [].
  rewrite /Rdiv -bpow_opp [Z.opp _]/=.
  rewrite Zmult_comm.
  have [-> _] := prop2 g _ h_bound.
  rewrite mult_IZR.
  suff ->: ZnearestA (2 ^ 19 * log5 2) = 225799%Z=> //.
    rewrite /ZnearestA.
    have ->: `[2 ^ 19 * log5 2] = 225798.
      apply: Zfloor_eq; rewrite /log5 /logn ![radix_val _] /=.
      split; interval.
    have H: ((/ 2) < 2 ^ 19 * log5 2 - IZR 225798)%R.
      rewrite /log5 /logn ![radix_val _]/=; interval.
    rewrite (Rcompare_Gt _ _ H).
    have ->: (2 ^ 19)%R = 524288%R by ring.
    apply: Zceil_imp=> /=.
    rewrite /log5 /logn /=; split; interval.
rewrite /easycomp; split; case Hlt: (_ <? _) =>//.
+ case Hgt: (_ >? _)=> // _.
  apply: easycomp_gt.
  rewrite Hphi /g /h.
  move: Hgt=> /Z.gtb_lt Hgt.
  by exact: Z.lt_gt.
+ split=> //.
  move=> _.
  apply: easycomp_lt.
  rewrite Hphi /g /h.
  by move: Hlt=> /Z.ltb_lt.
+ split=> // H; exfalso.
  rewrite Hphi /g /h in H.
  rewrite -H in Hlt.
  by rewrite Z.ltb_irrefl in Hlt.
+ case Hgt: (_ >? _)=> //.
  split=> //; split=> //.
  move=> H; exfalso.
  rewrite Hphi /g /h in H.
  rewrite -H in Hgt.
  by rewrite Z.gtb_ltb Z.ltb_irrefl in Hgt.
  split=> //.
  split=> // _.
  rewrite Hphi /g /h.
  apply: Zle_antisym.
  by have := Zgt_cases (e10 - 15) (225799 * (v + e2 - e10 - 37) / 2 ^ 19); rewrite Hgt.
  have := Zlt_cases (e10 - 15) (225799 * (v + e2 - e10 - 37) / 2 ^ 19).
  rewrite Hlt=> H.
  by apply: Z.ge_le.
Qed.

(* III. Second step: a closer look at the significands *)

Hypothesis g_eq_phih : g = phi h.

Definition f h : R := (bpow radix5 (phi h) * bpow radix2 (-h))%R.

Lemma f_prop :
   ((IZR m < (f h) * (IZR n))%R -> (F2R x2 < F2R x10)%R)
/\ (((f h) * (IZR n) < IZR m)%R -> (F2R x10 < F2R x2)%R)
/\ (((f h) * (IZR n) = IZR m)%R -> (F2R x2 = F2R x10)%R).
Proof.
have ->: ((f h) * (IZR n) = (IZR n) * (bpow radix5 g) * (bpow radix2 (- h)))%R.
  rewrite /f -g_eq_phih; ring.
rewrite x2_eq x10_eq.
have invh: (IZR n * bpow radix5 g = IZR n * bpow radix5 g * bpow radix2 (- h) * bpow radix2 h)%R.
  rewrite bpow_opp Rmult_assoc Rinv_l ?Rmult_1_r //.
  by apply Rgt_not_eq; apply bpow_gt_0.
repeat split; move=> H; rewrite invh;
  repeat apply: Rmult_lt_compat_r=> //; try apply: bpow_gt_0.
by repeat apply: Rmult_eq_compat_r.
Qed.

Definition v' : Z := h + (phi h) - 971.

(* Lemma 1 *)
Lemma h_bound' : -787 <= h <= 716.
Proof.
split.
  rewrite -Zceil_eqlog5_1.
  apply/Zceil_glb/Rdiv_le_l.
    apply: Rplus_lt_0_compat; [lra| ].
    by apply: logn_pos; lra.
  apply: (Rle_trans _ (IZR (h + phi h))).
  have ->: h + phi h = e2 + v - 52.
    by rewrite -g_eq_phih /g /h; ring.
  apply: IZR_le; first by  have H := v_bound; lia.
  rewrite Rmult_plus_distr_l Rmult_1_r plus_IZR.
  apply: Rplus_le_compat_l.
  exact: Zfloor_lb.
rewrite -Zfloor_eqlog5_2.
apply/Zfloor_lub/Rdiv_le_r.
  by apply: Rplus_lt_0_compat; [idtac| apply: logn_pos]; lra.
apply: (Rle_trans _ (IZR (h + phi h + 1))).
  have H : (IZR h * log5 2 <= IZR (phi h + 1))%R.
    apply: Rlt_le.
    rewrite plus_IZR /=.
    by apply: Zfloor_ub.
  rewrite -Zplus_assoc plus_IZR; lra.
have ->: h + phi h = e2 + v - 52.
  by rewrite -g_eq_phih /g /h; ring.
apply: IZR_le.
by have H := v_bound; lia.
Qed.

Lemma n_even : 10 ^ 16 <= n -> n mod 2 = 0.
Proof.
move=> Hn.
rewrite /n -(Z.sub_simpl_r v 1) Zpower_plus //.
  by rewrite Zmult_assoc Zmult_mod Z.pow_1_r Z_mod_same_full Z.mul_0_r Zmod_0_l.
apply: Zle_minus_le_0.
rewrite /n in Hn.
apply/(Zlt_le_succ 0)/(Z.pow_lt_mono_r_iff 2 0 v)=> //.
  have := v_bound; lia.
rewrite Z.pow_0_r.
apply: (Zmult_lt_reg_r _ _ M10); lia.
Qed.

Lemma v'_pos : 680 <= h -> 0 < v'.
Proof.
rewrite /v' => H.
suff phih_ge: 292 <= phi h by lia.
apply: Zfloor_lub.
rewrite [X in (_ <= X)%R]Rmult_comm.
apply: Rmult_le_r; first by apply: logn_pos; lra.
apply: (Rle_trans _ (IZR (Zceil (292 / log5 2)))).
  by apply: Zceil_ub.
apply: IZR_le.
rewrite Zceil_eqlog5_2.
exact: (Z.le_trans _ 680).
Qed.

Lemma pow2v'_div_n : 680 <= h -> n mod 2^v' = 0.
Proof.
move=> h_ge.
have Hv'_pos := v'_pos.
have H': v' <= v.
  rewrite /v' -g_eq_phih /g /h; lia.
rewrite /n Z.mul_mod; last first.
  by apply/Zgt_not_eq/Z.pow_pos_nonneg; lia.
suff ->: 2 ^ v mod 2 ^ v' = 0.
  by rewrite Zmult_0_r Zmod_0_l.
have ->: v = v' + (v - v') by ring.
rewrite Z.pow_add_r; [| lia| lia].
rewrite Zmult_comm Z.mod_mul //.
apply: Zgt_not_eq.
apply: Z.pow_pos_nonneg=> //; lia.
Qed.

Lemma g_bound' : -339 <= g <= 308.
Proof.
rewrite g_eq_phih /phi; split.
  apply: Zfloor_lub.
  rewrite [X in (_ <= X)%R]Rmult_comm.
  apply: Rmult_le_r; first by apply: logn_pos; lra.
  apply: (Rle_trans _ (IZR (Zceil (-339 / log5 2)))).
    by apply: Zceil_ub.
  apply: IZR_le.
  rewrite Zceil_eqlog5_3.
  by have := h_bound'; lia.
apply: Zfloor_leq.
rewrite /= Rmult_comm.
apply: Rmult_lt_l; first by apply: logn_pos; lra.
apply: (Rlt_le_trans _ (IZR `[309 / log5 2])); last by apply: Zfloor_lb.
rewrite Zfloor_eqlog5_1.
apply: IZR_lt.
by have h_le := h_bound'; lia.
Qed.

Definition d_h := Rabs (bpow radix5 (phi h) / bpow radix2 h - IZR m / IZR n).

Lemma d_h_zero : F2R x2 = F2R x10 <-> d_h = 0.
Proof.
split=> H.
  rewrite /d_h.
  suff ->: (bpow radix5 (phi h) / bpow radix2 h - IZR m / IZR n = 0)%R.
    by rewrite Rabs_R0.
  apply/Rminus_diag_eq/Rdiv_eq_Rdiv.
  - apply/(IZR_lt 0)/(Z.lt_le_trans _ (2 ^ 53))=> //.
    by have:= n_bound; lia.
  - by apply: bpow_gt_0.
  rewrite Rmult_comm.
  apply: (Rmult_eq_reg_r (bpow radix2 (g - v))).
    rewrite -g_eq_phih.
    by rewrite -x2_eq -x10_eq.
  by apply: Rgt_not_eq; apply: bpow_gt_0.
(**)
rewrite /d_h in H.
have H': (bpow radix5 (phi h) / bpow radix2 h - IZR m / IZR n = 0)%R.
  split_Rabs; last by rewrite H.
  by rewrite -[(_ - _)%R]Ropp_involutive H; lra.
rewrite x2_eq x10_eq.
congr (_ * _)%R.
rewrite [(IZR n * _)%R]Rmult_comm.
apply: Rmult_eq_Rmult; try apply: bpow_gt_0.
  apply/(IZR_lt 0)/(Z.lt_le_trans _ (2 ^ 53))=> //.
  by have := n_bound; lia.
apply: Rminus_diag_uniq_sym.
rewrite g_eq_phih.
exact: H'.
Qed.

Definition eta := Rpower 2 (-113 - 7/10).

Lemma d_h_eq1 : 
  d_h = (Rabs (bpow radix5 (phi h) * IZR n - IZR m * bpow radix2 h) / 
              (bpow radix2 h * IZR n))%R.
Proof.
have IZRn_pos: (0 < IZR n)%R.
  apply: (IZR_lt 0); have Hn := n_bound; lia.
have den_pos: (0 < bpow radix2 h * IZR n)%R.
  apply: Rmult_lt_0_compat; [| lra].
  exact: bpow_gt_0.
rewrite /d_h.
rewrite -[(bpow radix2 h * IZR n)%R]Rabs_right; [| lra].
rewrite -Rcomplements.Rabs_div; [| lra].
congr (_ _).
field; split; apply: Rgt_not_eq; [lra| ].
exact: bpow_gt_0.
Qed.

Lemma d_h_eq2 : 
  d_h = (Rabs (bpow radix2 (- h) * IZR n - IZR m * bpow radix5 (- phi h)) / 
              (bpow radix5 (- phi h) * IZR n))%R.
Proof.
have IZRn_pos: (0 < IZR n)%R.
  apply: (IZR_lt 0); have Hn := n_bound; lia.
have den_pos: (0 < bpow radix5 (- phi h) * IZR n)%R.
  apply: Rmult_lt_0_compat; [| lra].
  exact: bpow_gt_0.
rewrite /d_h.
rewrite -[(bpow radix5 (- phi h) * IZR n)%R]Rabs_right; [| lra].
rewrite -Rcomplements.Rabs_div; [| lra].
congr (_ _).
rewrite !bpow_opp.
field; repeat split; apply: Rgt_not_eq; [ | |lra].
  exact: bpow_gt_0.
exact: bpow_gt_0.
Qed.

Lemma d_h_num_int1 : 
  0 <= h -> exists t,
   (Rabs (bpow radix5 (phi h) * IZR n - IZR m * bpow radix2 h)) = (IZR t)%R.
Proof.
move=> h_pos.
have phi_h_pos: 0 <= phi h.
  rewrite /phi.
  apply/Zfloor_lub/Rmult_le_pos; first by exact: (IZR_le 0).
  by rewrite /log5 /logn /=; interval.
split_Rabs.
  exists (- (5 ^ (phi h) * n - m * 2 ^ h)).
  rewrite opp_IZR minus_IZR mult_IZR mult_IZR.
  by rewrite (IZR_Zpower radix5) // (IZR_Zpower radix2).
exists (5 ^ (phi h) * n - m * 2 ^ h).
rewrite minus_IZR mult_IZR mult_IZR.
by rewrite (IZR_Zpower radix5) // (IZR_Zpower radix2).
Qed.

Lemma d_h_num_int2 : 
  h <= 0 -> exists t,
    Rabs (bpow radix2 (- h) * IZR n - IZR m * bpow radix5 (- phi h)) = (IZR t)%R.
Proof.
move=> h_neg.
have phi_h_pos: phi h <= 0.
  apply: Zfloor_leq=> /=.
  rewrite Rmult_comm.
  apply: Rmult_lt_l; first by apply: logn_pos; lra.
  apply: (Rlt_trans _ 1). 
    by apply: (IZR_lt _ 1); lia.
  by rewrite /log5 /logn /=; interval.
split_Rabs.
  exists (- (2 ^ (- h) * n - m * 5 ^ (- phi h))).
  rewrite opp_IZR minus_IZR mult_IZR mult_IZR.
  by rewrite (IZR_Zpower radix5) ?(IZR_Zpower radix2) //; lia.
exists (2 ^ (- h) * n - m * 5 ^ - phi h).
rewrite minus_IZR mult_IZR mult_IZR.
by rewrite (IZR_Zpower radix5) ?(IZR_Zpower radix2) //; lia.
Qed.

Lemma d_h_easy1 : 0 <= h <= 53 -> (d_h > 0)%R -> (bpow radix2 (-107) <= d_h)%R.
Proof.
move=> [h_pos h_le] d_h_pos.
rewrite d_h_eq1.
have IZRn_pos: (0 < IZR n)%R.
  apply: IZR_lt; have Hn := n_bound; lia.
have den_pos: (0 < bpow radix2 h * IZR n)%R.
  apply: Rmult_lt_0_compat; [| lra].
  exact: bpow_gt_0.
apply: (Rle_trans _ (1 / (bpow radix2 h * IZR n)))%R.
  have ->: (bpow radix2 (-107) = 1 / (bpow radix2 53 * bpow radix2 54))%R.
    by rewrite -bpow_plus /=; lra.
  rewrite /Rdiv.
  apply: Rmult_le_compat_l; [lra| ].
  apply: Rinv_le; [lra| ].
  apply: Rmult_le_compat; [exact: bpow_ge_0| lra| exact: bpow_le| ].
  by apply: (IZR_le _ (2 ^54)); have Hn := n_bound; lia.
rewrite /Rdiv.
apply/Rmult_le_compat_r.
  apply: Rlt_le.
  exact: Rinv_0_lt_compat.
rewrite d_h_eq1 in d_h_pos.
have num_pos: (0 < Rabs (bpow radix5 (phi h) * IZR n - IZR m * bpow radix2 h))%R.
  apply: (Rmult_lt_reg_r (/ (bpow radix2 h * IZR n))). 
  exact: Rinv_0_lt_compat.
  by rewrite Rmult_0_l.
move: (d_h_num_int1 h_pos)=> [t Ht].
rewrite Ht.
apply/(IZR_le 1)/(Zlt_le_succ 0)/lt_IZR.
by rewrite -Ht /=.
Qed.

Ltac htac n :=
  let X := fresh "X" in
  rewrite 1?[Z.pred _]/= [X in (_ <= X) -> _]Zsucc_pred Z.le_succ_r [IZR _]/=;
  move => [| ->];
  [| rewrite (ZfloorE n) ?[IZR _]/= /log5 /logn /=; interval].

Ltac hntac v :=
  first [lia |htac v; hntac v |hntac (v -1)].

Lemma h_54_ln_2 : 
  -53 <= h <= 0 -> (- IZR (`[(IZR h * log5 2)]) * ln 5 <= 54 * ln 2)%R.
Proof.
move=> [h_ge].
hntac 0%Z.
Qed.

Lemma d_h_easy2 : -53 <= h <= 0 -> (d_h > 0 -> bpow radix2 (-108) <= d_h)%R.
Proof.
move=> [h_ge h_neg] d_h_pos.
rewrite d_h_eq2.
have IZRn_pos: (0 < IZR n)%R.
  by apply: (IZR_lt 0); have Hn := n_bound; lia.
have den_pos: (0 < bpow radix5 (-phi h) * IZR n)%R.
  apply: Rmult_lt_0_compat; [| lra].
  exact: bpow_gt_0.
apply: (Rle_trans _ (1 / (bpow radix5 (- phi h) * IZR n)))%R.
have ->: (bpow radix2 (-108) = 1 / (bpow radix2 54 * bpow radix2 54))%R.
  rewrite -bpow_plus /=; lra.
rewrite /Rdiv.
apply: Rmult_le_compat_l; [lra| ].
  apply: Rinv_le; [lra| ].
  apply: Rmult_le_compat; [exact: bpow_ge_0|lra| | ].
    rewrite !bpow_exp.
    apply: exp_le=> /=.
    rewrite /phi opp_IZR.
    exact: h_54_ln_2.
  apply: (IZR_le _ (2 ^ 54)); have Hn := n_bound; lia.
rewrite /Rdiv.
apply: Rmult_le_compat_r.
  by apply: Rlt_le; exact: Rinv_0_lt_compat.
rewrite d_h_eq2 in d_h_pos.
have num_pos:
   (0 < Rabs (bpow radix2 (- h) * IZR n - IZR m * bpow radix5 (-phi h)))%R.
  apply: (Rmult_lt_reg_r (/ (bpow radix5 (- phi h) * IZR n))).
    exact: Rinv_0_lt_compat.
  by rewrite Rmult_0_l.
move: (d_h_num_int2 h_neg)=> [t Ht].
rewrite Ht.
apply/(IZR_le 1)/(Zlt_le_succ 0)/lt_IZR.
by rewrite -Ht /=.
Qed.

Theorem d_h_min : (d_h > 0 -> d_h > eta)%R.
Proof.
move=> d_h_pos.
have [Hh1 Hh2] := h_bound'.
have H1: 0 <= h <= 53 -> (d_h > eta)%R.
  move=> Hh.
  have H := (d_h_easy1 Hh d_h_pos).
  apply: (Rlt_le_trans _ (bpow radix2 (-107)))=> //.
  rewrite /eta bpow_exp /Rpower /=.
  apply: exp_increasing.
  apply: Rmult_lt_compat_r; [| lra].
  apply: ln_gt_0; lra.
have H2: -53 <= h <= 0 -> (d_h > eta)%R.
  move=> Hh.
  have H := (d_h_easy2 Hh d_h_pos).
  apply: (Rlt_le_trans _ (bpow radix2 (-108)))=> //.
  rewrite /eta bpow_exp /Rpower /=.
  apply: exp_increasing.
  apply: Rmult_lt_compat_r; [| lra].
  apply: ln_gt_0; lra.
have Hneq: (powerRZ 5 (phi h) / powerRZ 2 h <> m / n)%R.
  move=> Habs.
  rewrite /d_h !bpow_powerRZ /= Habs in d_h_pos.
  rewrite Rcomplements.Rminus_eq_0 /= Rabs_R0 in d_h_pos.
  lra.
have H3: -787 <= h <= -54 -> (d_h > eta)%R.
  move=> Hh.
  rewrite /d_h.
  apply: (Rlt_le_trans _ (IZR bp2 / IZR bq2)).
  rewrite /eta /bp2 /bq2 /= /Rpower; interval.
  rewrite !bpow_powerRZ [IZR _]/= [IZR _]/=.
  apply: neg_correct=> // [| | Hn].
  - by have Hn := n_bound; lia.
  - rewrite /d_h in d_h_pos.
    have Hn1 := n_bound.
    by rewrite /m53 /m54m1; lia.
  apply/eqP/eqP.
  by rewrite [LHS]Zeven_mod n_even.
suff H4 : 54 <= h <= 716 -> (d_h > eta)%R.
  have [Ha| [Hb| [Hc| Hd]]] : 
    -787 <= h <= -54 \/ -53 <= h <= 0 \/ 
    0 <= h <= 53 \/ 54 <= h <= 716 by lia.
  - exact: H3.
  - exact: H2.
  - exact: H1.
  exact: H4.
move=> Hh.
rewrite /d_h.
apply: (Rlt_le_trans _ (IZR bp1 / IZR bq1)).
  rewrite /eta /bp1 /bq1 /= /Rpower.
  interval.
have ->: bpow radix5 (phi h) = powerRZ 5 (phi h) by rewrite bpow_powerRZ.
have ->: bpow radix2 h = powerRZ 2 h by rewrite bpow_powerRZ.
apply: pos_correct=> // [| | Hm].
- have Hn := n_bound; lia.
- have Hn1 := n_bound; rewrite /m53 /m54m1; lia.
apply/eqP/eqP. 
by rewrite [LHS]Zeven_mod n_even.
Qed.

Corollary d_h_eta : (F2R x2 <> F2R x10) -> (d_h > eta)%R.
Proof.
move=> H.
apply: d_h_min.
move: (Rgt_ge_dec d_h 0)=> [//| d_h_le_0].
contradict H.
apply/d_h_zero/Rle_le_eq; split.
  by apply: Rge_le; exact: d_h_le_0.
by apply: Rabs_pos.
Qed.

(* IV. Inequality testing *)

(* A. Direct method *)

Theorem direct_method_correct mu e :
  (e > 0 -> e < eta / 4 ->
  Rabs ((f h) * (IZR n) - mu) <= e * (IZR n) ->
     (mu > (IZR m) + e * 2^54 -> F2R x10 > F2R x2)
  /\ (mu < (IZR m) - e * 2^54 -> F2R x10 < F2R x2)
  /\ (Rabs ((IZR m) - mu) <= e * 2^54 -> F2R x10 = F2R x2))%R.
Proof.
move=> e_pos He Hmu.
have Hmu': (Rabs (mu - (f h) * (IZR n)) < e * 2 ^ 54)%R.
  rewrite Rabs_minus_sym.
  apply (Rle_lt_trans _ (e * IZR n))=> //.
  apply: Rmult_lt_compat_l; [lra| ].
  have ->: (2 ^ 54)%R = IZR (2 ^ 54) by rewrite pow_IZR /=; lra.
  apply: IZR_lt.
  have := n_bound; lia.
(repeat split)=> [H| H| H].
(* mu > ... *)
- apply f_prop.
  apply: Rminus_gt_0_lt.
  apply: (Rlt_trans _ (mu - IZR m - e * 2 ^ 54)); [lra| ].
  have H' : (mu - f h * IZR n < e * 2 ^ 54)%R by apply Rabs_lt_inv.
  lra.
(* mu < ... *)
- apply f_prop.
  apply: Rminus_gt_0_lt.
  apply: (Rlt_trans _ (IZR m - mu - e * 2 ^ 54)); [lra| ].
  have H' : (- (e * 2 ^ 54) < mu - f h * IZR n)%R.
    by have H' := (Rabs_lt_inv _ _ Hmu'); apply H'.
  lra.
(* equality case *)
suff H': (d_h <= eta)%R.
  move: (Req_dec (F2R x10) (F2R x2))=> [//| neq].
  contradict H'.
  apply: Rlt_not_le.
  by apply: d_h_eta=> Habs; apply: neq.
have Hinv_n: (0 <= / IZR n)%R.
  apply/Rlt_le/Rinv_0_lt_compat/(IZR_lt 0).
  have Hn := n_bound; lia.
apply: (Rle_trans _ ((e * 2 ^ 55) / (IZR n))).
have ->: (d_h = Rabs (f h * (IZR n) - (IZR m)) / IZR n)%R.
  rewrite /d_h /Rdiv -bpow_opp -/(f h).
    have Hn: (/ IZR n)%R = Rabs (/ IZR n) by rewrite Rabs_pos_eq.
    rewrite {} [X in (_ = _ * X)%R]Hn -Rabs_mult.
    congr (_ _); field.
    apply: Rgt_not_eq.
    apply: IZR_lt.
    have Hn := n_bound; lia.
  apply: Rmult_le_compat_r=> //.
  have ->: (f h * IZR n - IZR m = f h * IZR n - mu + (mu - IZR m))%R by ring.
    apply: (Rle_trans _ (Rabs (f h * IZR n - mu) + Rabs (mu - IZR m))).
    exact: Rabs_triang.
  by rewrite Rabs_minus_sym [Rabs (_ - IZR m)]Rabs_minus_sym; lra.
apply: (Rle_trans _ ((eta * 2 ^ 53) / (IZR n))).
  apply: Rmult_le_compat_r; lra.
apply: Rdiv_le_l.
  apply: (IZR_lt 0).
  have := n_bound; lia.
apply: Rmult_le_compat_l.
  apply: (Rle_trans _ (4 * e)); lra.
have ->: (2 ^ 53)%R = IZR (2 ^ 53) by rewrite pow_IZR /=; lra.
apply: IZR_le.
have := n_bound; lia.
Qed.

Definition f_tbl h : Z :=
  match h with
  | -787 => 155090275327330814032632713427604519407
  | -786 =>  77545137663665407016316356713802259704
  | -785 =>  38772568831832703508158178356901129852
  | -784 =>  96931422079581758770395445892252824629
  | -783 =>  48465711039790879385197722946126412315
  | -782 => 121164277599477198462994307365316030787
  | -781 =>  60582138799738599231497153682658015394
  | -780 => 151455346999346498078742884206645038483
  | -779 =>  75727673499673249039371442103322519242
  | -778 =>  37863836749836624519685721051661259621
  | -777 =>  94659591874591561299214302629153149052
  | -776 =>  47329795937295780649607151314576574526
  | -775 => 118324489843239451624017878286441436315
  | -774 =>  59162244921619725812008939143220718158
  | -773 => 147905612304049314530022347858051795394
  | -772 =>  73952806152024657265011173929025897697
  | -771 =>  36976403076012328632505586964512948849
  | -770 =>  92441007690030821581263967411282372121
  | -769 =>  46220503845015410790631983705641186061
  | -768 => 115551259612538526976579959264102965151
  | -767 =>  57775629806269263488289979632051482576
  | -766 => 144439074515673158720724949080128706439
  | -765 =>  72219537257836579360362474540064353220
  | -764 =>  36109768628918289680181237270032176610
  | -763 =>  90274421572295724200453093175080441525
  | -762 =>  45137210786147862100226546587540220763
  | -761 => 112843026965369655250566366468850551906
  | -760 =>  56421513482684827625283183234425275953
  | -759 => 141053783706712069063207958086063189882
  | -758 =>  70526891853356034531603979043031594941
  | -757 =>  35263445926678017265801989521515797471
  | -756 =>  88158614816695043164504973803789493676
  | -755 =>  44079307408347521582252486901894746838
  | -754 => 110198268520868803955631217254736867095
  | -753 =>  55099134260434401977815608627368433548
  | -752 => 137747835651086004944539021568421083869
  | -751 =>  68873917825543002472269510784210541935
  | -750 =>  34436958912771501236134755392105270968
  | -749 =>  86092397281928753090336888480263177418
  | -748 =>  43046198640964376545168444240131588709
  | -747 => 107615496602410941362921110600328971773
  | -746 =>  53807748301205470681460555300164485887
  | -745 => 134519370753013676703651388250411214716
  | -744 =>  67259685376506838351825694125205607358
  | -743 => 168149213441267095879564235313014018395
  | -742 =>  84074606720633547939782117656507009198
  | -741 =>  42037303360316773969891058828253504599
  | -740 => 105093258400791934924727647070633761497
  | -739 =>  52546629200395967462363823535316880749
  | -738 => 131366573000989918655909558838292201871
  | -737 =>  65683286500494959327954779419146100936
  | -736 => 164208216251237398319886948547865252339
  | -735 =>  82104108125618699159943474273932626170
  | -734 =>  41052054062809349579971737136966313085
  | -733 => 102630135157023373949929342842415782712
  | -732 =>  51315067578511686974964671421207891356
  | -731 => 128287668946279217437411678553019728390
  | -730 =>  64143834473139608718705839276509864195
  | -729 => 160359586182849021796764598191274660487
  | -728 =>  80179793091424510898382299095637330244
  | -727 =>  40089896545712255449191149547818665122
  | -726 => 100224741364280638622977873869546662805
  | -725 =>  50112370682140319311488936934773331403
  | -724 => 125280926705350798278722342336933328506
  | -723 =>  62640463352675399139361171168466664253
  | -722 => 156601158381688497848402927921166660632
  | -721 =>  78300579190844248924201463960583330316
  | -720 =>  39150289595422124462100731980291665158
  | -719 =>  97875723988555311155251829950729162895
  | -718 =>  48937861994277655577625914975364581448
  | -717 => 122344654985694138944064787438411453619
  | -716 =>  61172327492847069472032393719205726810
  | -715 => 152930818732117673680080984298014317023
  | -714 =>  76465409366058836840040492149007158512
  | -713 =>  38232704683029418420020246074503579256
  | -712 =>  95581761707573546050050615186258948140
  | -711 =>  47790880853786773025025307593129474070
  | -710 => 119477202134466932562563268982823685175
  | -709 =>  59738601067233466281281634491411842588
  | -708 => 149346502668083665703204086228529606468
  | -707 =>  74673251334041832851602043114264803234
  | -706 =>  37336625667020916425801021557132401617
  | -705 =>  93341564167552291064502553892831004043
  | -704 =>  46670782083776145532251276946415502022
  | -703 => 116676955209440363830628192366038755053
  | -702 =>  58338477604720181915314096183019377527
  | -701 => 145846194011800454788285240457548443817
  | -700 =>  72923097005900227394142620228774221909
  | -699 =>  36461548502950113697071310114387110955
  | -698 =>  91153871257375284242678275285967777386
  | -697 =>  45576935628687642121339137642983888693
  | -696 => 113942339071719105303347844107459721732
  | -695 =>  56971169535859552651673922053729860866
  | -694 => 142427923839648881629184805134324652165
  | -693 =>  71213961919824440814592402567162326083
  | -692 =>  35606980959912220407296201283581163042
  | -691 =>  89017452399780551018240503208952907603
  | -690 =>  44508726199890275509120251604476453802
  | -689 => 111271815499725688772800629011191134504
  | -688 =>  55635907749862844386400314505595567252
  | -687 => 139089769374657110966000786263988918129
  | -686 =>  69544884687328555483000393131994459065
  | -685 =>  34772442343664277741500196565997229533
  | -684 =>  86931105859160694353750491414993073831
  | -683 =>  43465552929580347176875245707496536916
  | -682 => 108663882323950867942188114268741342289
  | -681 =>  54331941161975433971094057134370671145
  | -680 => 135829852904938584927735142835926677861
  | -679 =>  67914926452469292463867571417963338931
  | -678 => 169787316131173231159668928544908347326
  | -677 =>  84893658065586615579834464272454173663
  | -676 =>  42446829032793307789917232136227086832
  | -675 => 106117072581983269474793080340567717079
  | -674 =>  53058536290991634737396540170283858540
  | -673 => 132646340727479086843491350425709646348
  | -672 =>  66323170363739543421745675212854823174
  | -671 => 165807925909348858554364188032137057935
  | -670 =>  82903962954674429277182094016068528968
  | -669 =>  41451981477337214638591047008034264484
  | -668 => 103629953693343036596477617520085661210
  | -667 =>  51814976846671518298238808760042830605
  | -666 => 129537442116678795745597021900107076512
  | -665 =>  64768721058339397872798510950053538256
  | -664 => 161921802645848494681996277375133845640
  | -663 =>  80960901322924247340998138687566922820
  | -662 =>  40480450661462123670499069343783461410
  | -661 => 101201126653655309176247673359458653525
  | -660 =>  50600563326827654588123836679729326763
  | -659 => 126501408317069136470309591699323316906
  | -658 =>  63250704158534568235154795849661658453
  | -657 => 158126760396336420587886989624154146133
  | -656 =>  79063380198168210293943494812077073067
  | -655 =>  39531690099084105146971747406038536534
  | -654 =>  98829225247710262867429368515096341333
  | -653 =>  49414612623855131433714684257548170667
  | -652 => 123536531559637828584286710643870426666
  | -651 =>  61768265779818914292143355321935213333
  | -650 => 154420664449547285730358388304838033333
  | -649 =>  77210332224773642865179194152419016667
  | -648 =>  38605166112386821432589597076209508334
  | -647 =>  96512915280967053581473992690523770833
  | -646 =>  48256457640483526790736996345261885417
  | -645 => 120641144101208816976842490863154713542
  | -644 =>  60320572050604408488421245431577356771
  | -643 => 150801430126511021221053113578943391927
  | -642 =>  75400715063255510610526556789471695964
  | -641 =>  37700357531627755305263278394735847982
  | -640 =>  94250893829069388263158195986839619954
  | -639 =>  47125446914534694131579097993419809977
  | -638 => 117813617286336735328947744983549524943
  | -637 =>  58906808643168367664473872491774762472
  | -636 => 147267021607920919161184681229436906178
  | -635 =>  73633510803960459580592340614718453089
  | -634 =>  36816755401980229790296170307359226545
  | -633 =>  92041888504950574475740425768398066362
  | -632 =>  46020944252475287237870212884199033181
  | -631 => 115052360631188218094675532210497582952
  | -630 =>  57526180315594109047337766105248791476
  | -629 => 143815450788985272618344415263121978690
  | -628 =>  71907725394492636309172207631560989345
  | -627 =>  35953862697246318154586103815780494673
  | -626 =>  89884656743115795386465259539451236681
  | -625 =>  44942328371557897693232629769725618341
  | -624 => 112355820928894744233081574424314045852
  | -623 =>  56177910464447372116540787212157022926
  | -622 => 140444776161118430291351968030392557314
  | -621 =>  70222388080559215145675984015196278657
  | -620 =>  35111194040279607572837992007598139329
  | -619 =>  87777985100699018932094980018995348322
  | -618 =>  43888992550349509466047490009497674161
  | -617 => 109722481375873773665118725023744185402
  | -616 =>  54861240687936886832559362511872092701
  | -615 => 137153101719842217081398406279680231752
  | -614 =>  68576550859921108540699203139840115876
  | -613 =>  34288275429960554270349601569920057938
  | -612 =>  85720688574901385675874003924800144845
  | -611 =>  42860344287450692837937001962400072423
  | -610 => 107150860718626732094842504906000181057
  | -609 =>  53575430359313366047421252453000090529
  | -608 => 133938575898283415118553131132500226321
  | -607 =>  66969287949141707559276565566250113161
  | -606 => 167423219872854268898191413915625282901
  | -605 =>  83711609936427134449095706957812641451
  | -604 =>  41855804968213567224547853478906320726
  | -603 => 104639512420533918061369633697265801813
  | -602 =>  52319756210266959030684816848632900907
  | -601 => 130799390525667397576712042121582252266
  | -600 =>  65399695262833698788356021060791126133
  | -599 => 163499238157084246970890052651977815333
  | -598 =>  81749619078542123485445026325988907667
  | -597 =>  40874809539271061742722513162994453834
  | -596 => 102187023848177654356806282907486134583
  | -595 =>  51093511924088827178403141453743067292
  | -594 => 127733779810222067946007853634357668229
  | -593 =>  63866889905111033973003926817178834115
  | -592 => 159667224762777584932509817042947085286
  | -591 =>  79833612381388792466254908521473542643
  | -590 =>  39916806190694396233127454260736771322
  | -589 =>  99792015476735990582818635651841928304
  | -588 =>  49896007738367995291409317825920964152
  | -587 => 124740019345919988228523294564802410380
  | -586 =>  62370009672959994114261647282401205190
  | -585 => 155925024182399985285654118206003012975
  | -584 =>  77962512091199992642827059103001506488
  | -583 =>  38981256045599996321413529551500753244
  | -582 =>  97453140113999990803533823878751883109
  | -581 =>  48726570056999995401766911939375941555
  | -580 => 121816425142499988504417279848439853886
  | -579 =>  60908212571249994252208639924219926943
  | -578 => 152270531428124985630521599810549817358
  | -577 =>  76135265714062492815260799905274908679
  | -576 =>  38067632857031246407630399952637454340
  | -575 =>  95169082142578116019075999881593635849
  | -574 =>  47584541071289058009537999940796817925
  | -573 => 118961352678222645023844999851992044811
  | -572 =>  59480676339111322511922499925996022406
  | -571 => 148701690847778306279806249814990056014
  | -570 =>  74350845423889153139903124907495028007
  | -569 =>  37175422711944576569951562453747514004
  | -568 =>  92938556779861441424878906134368785009
  | -567 =>  46469278389930720712439453067184392505
  | -566 => 116173195974826801781098632667960981261
  | -565 =>  58086597987413400890549316333980490631
  | -564 => 145216494968533502226373290834951226576
  | -563 =>  72608247484266751113186645417475613288
  | -562 =>  36304123742133375556593322708737806644
  | -561 =>  90760309355333438891483306771844516610
  | -560 =>  45380154677666719445741653385922258305
  | -559 => 113450386694166798614354133464805645762
  | -558 =>  56725193347083399307177066732402822881
  | -557 => 141812983367708498267942666831007057203
  | -556 =>  70906491683854249133971333415503528602
  | -555 =>  35453245841927124566985666707751764301
  | -554 =>  88633114604817811417464166769379410752
  | -553 =>  44316557302408905708732083384689705376
  | -552 => 110791393256022264271830208461724263440
  | -551 =>  55395696628011132135915104230862131720
  | -550 => 138489241570027830339787760577155329300
  | -549 =>  69244620785013915169893880288577664650
  | -548 =>  34622310392506957584946940144288832325
  | -547 =>  86555775981267393962367350360722080813
  | -546 =>  43277887990633696981183675180361040407
  | -545 => 108194719976584242452959187950902601016
  | -544 =>  54097359988292121226479593975451300508
  | -543 => 135243399970730303066198984938628251269
  | -542 =>  67621699985365151533099492469314125635
  | -541 => 169054249963412878832748731173285314087
  | -540 =>  84527124981706439416374365586642657044
  | -539 =>  42263562490853219708187182793321328522
  | -538 => 105658906227133049270467956983303321304
  | -537 =>  52829453113566524635233978491651660652
  | -536 => 132073632783916311588084946229129151630
  | -535 =>  66036816391958155794042473114564575815
  | -534 => 165092040979895389485106182786411439538
  | -533 =>  82546020489947694742553091393205719769
  | -532 =>  41273010244973847371276545696602859885
  | -531 => 103182525612434618428191364241507149711
  | -530 =>  51591262806217309214095682120753574856
  | -529 => 128978157015543273035239205301883937139
  | -528 =>  64489078507771636517619602650941968570
  | -527 => 161222696269429091294049006627354921423
  | -526 =>  80611348134714545647024503313677460712
  | -525 =>  40305674067357272823512251656838730356
  | -524 => 100764185168393182058780629142096825890
  | -523 =>  50382092584196591029390314571048412945
  | -522 => 125955231460491477573475786427621032362
  | -521 =>  62977615730245738786737893213810516181
  | -520 => 157444039325614346966844733034526290453
  | -519 =>  78722019662807173483422366517263145227
  | -518 =>  39361009831403586741711183258631572614
  | -517 =>  98402524578508966854277958146578931533
  | -516 =>  49201262289254483427138979073289465767
  | -515 => 123003155723136208567847447683223664416
  | -514 =>  61501577861568104283923723841611832208
  | -513 => 153753944653920260709809309604029580520
  | -512 =>  76876972326960130354904654802014790260
  | -511 =>  38438486163480065177452327401007395130
  | -510 =>  96096215408700162943630818502518487825
  | -509 =>  48048107704350081471815409251259243913
  | -508 => 120120269260875203679538523128148109781
  | -507 =>  60060134630437601839769261564074054891
  | -506 => 150150336576094004599423153910185137227
  | -505 =>  75075168288047002299711576955092568614
  | -504 =>  37537584144023501149855788477546284307
  | -503 =>  93843960360058752874639471193865710767
  | -502 =>  46921980180029376437319735596932855384
  | -501 => 117304950450073441093299338992332138458
  | -500 =>  58652475225036720546649669496166069229
  | -499 => 146631188062591801366624173740415173073
  | -498 =>  73315594031295900683312086870207586537
  | -497 =>  36657797015647950341656043435103793269
  | -496 =>  91644492539119875854140108587759483171
  | -495 =>  45822246269559937927070054293879741586
  | -494 => 114555615673899844817675135734699353963
  | -493 =>  57277807836949922408837567867349676982
  | -492 => 143194519592374806022093919668374192454
  | -491 =>  71597259796187403011046959834187096227
  | -490 =>  35798629898093701505523479917093548114
  | -489 =>  89496574745234253763808699792733870284
  | -488 =>  44748287372617126881904349896366935142
  | -487 => 111870718431542817204760874740917337855
  | -486 =>  55935359215771408602380437370458668928
  | -485 => 139838398039428521505951093426146672318
  | -484 =>  69919199019714260752975546713073336159
  | -483 =>  34959599509857130376487773356536668080
  | -482 =>  87398998774642825941219433391341670199
  | -481 =>  43699499387321412970609716695670835100
  | -480 => 109248748468303532426524291739177087749
  | -479 =>  54624374234151766213262145869588543875
  | -478 => 136560935585379415533155364673971359686
  | -477 =>  68280467792689707766577682336985679843
  | -476 =>  34140233896344853883288841168492839922
  | -475 =>  85350584740862134708222102921232099804
  | -474 =>  42675292370431067354111051460616049902
  | -473 => 106688230926077668385277628651540124755
  | -472 =>  53344115463038834192638814325770062378
  | -471 => 133360288657597085481597035814425155943
  | -470 =>  66680144328798542740798517907212577972
  | -469 => 166700360821996356851996294768031444929
  | -468 =>  83350180410998178425998147384015722465
  | -467 =>  41675090205499089212999073692007861233
  | -466 => 104187725513747723032497684230019653081
  | -465 =>  52093862756873861516248842115009826541
  | -464 => 130234656892184653790622105287524566351
  | -463 =>  65117328446092326895311052643762283176
  | -462 => 162793321115230817238277631609405707939
  | -461 =>  81396660557615408619138815804702853970
  | -460 =>  40698330278807704309569407902351426985
  | -459 => 101745825697019260773923519755878567462
  | -458 =>  50872912848509630386961759877939283731
  | -457 => 127182282121274075967404399694848209327
  | -456 =>  63591141060637037983702199847424104664
  | -455 => 158977852651592594959255499618560261659
  | -454 =>  79488926325796297479627749809280130830
  | -453 =>  39744463162898148739813874904640065415
  | -452 =>  99361157907245371849534687261600163537
  | -451 =>  49680578953622685924767343630800081769
  | -450 => 124201447384056714811918359077000204421
  | -449 =>  62100723692028357405959179538500102211
  | -448 => 155251809230070893514897948846250255526
  | -447 =>  77625904615035446757448974423125127763
  | -446 =>  38812952307517723378724487211562563882
  | -445 =>  97032380768794308446811218028906409704
  | -444 =>  48516190384397154223405609014453204852
  | -443 => 121290475960992885558514022536133012130
  | -442 =>  60645237980496442779257011268066506065
  | -441 => 151613094951241106948142528170166265162
  | -440 =>  75806547475620553474071264085083132581
  | -439 =>  37903273737810276737035632042541566291
  | -438 =>  94758184344525691842589080106353915727
  | -437 =>  47379092172262845921294540053176957864
  | -436 => 118447730430657114803236350132942394658
  | -435 =>  59223865215328557401618175066471197329
  | -434 => 148059663038321393504045437666177993323
  | -433 =>  74029831519160696752022718833088996662
  | -432 =>  37014915759580348376011359416544498331
  | -431 =>  92537289398950870940028398541361245827
  | -430 =>  46268644699475435470014199270680622914
  | -429 => 115671611748688588675035498176701557283
  | -428 =>  57835805874344294337517749088350778642
  | -427 => 144589514685860735843794372720876946604
  | -426 =>  72294757342930367921897186360438473302
  | -425 =>  36147378671465183960948593180219236651
  | -424 =>  90368446678662959902371482950548091628
  | -423 =>  45184223339331479951185741475274045814
  | -422 => 112960558348328699877964353688185114535
  | -421 =>  56480279174164349938982176844092557268
  | -420 => 141200697935410874847455442110231393168
  | -419 =>  70600348967705437423727721055115696584
  | -418 =>  35300174483852718711863860527557848292
  | -417 =>  88250436209631796779659651318894620730
  | -416 =>  44125218104815898389829825659447310365
  | -415 => 110313045262039745974574564148618275913
  | -414 =>  55156522631019872987287282074309137957
  | -413 => 137891306577549682468218205185772844891
  | -412 =>  68945653288774841234109102592886422446
  | -411 =>  34472826644387420617054551296443211223
  | -410 =>  86182066610968551542636378241108028057
  | -409 =>  43091033305484275771318189120554014029
  | -408 => 107727583263710689428295472801385035071
  | -407 =>  53863791631855344714147736400692517536
  | -406 => 134659479079638361785369341001731293839
  | -405 =>  67329739539819180892684670500865646920
  | -404 => 168324348849547952231711676252164117298
  | -403 =>  84162174424773976115855838126082058649
  | -402 =>  42081087212386988057927919063041029325
  | -401 => 105202718030967470144819797657602573312
  | -400 =>  52601359015483735072409898828801286656
  | -399 => 131503397538709337681024747072003216639
  | -398 =>  65751698769354668840512373536001608320
  | -397 => 164379246923386672101280933840004020799
  | -396 =>  82189623461693336050640466920002010400
  | -395 =>  41094811730846668025320233460001005200
  | -394 => 102737029327116670063300583650002513000
  | -393 =>  51368514663558335031650291825001256500
  | -392 => 128421286658895837579125729562503141249
  | -391 =>  64210643329447918789562864781251570625
  | -390 => 160526608323619796973907161953128926561
  | -389 =>  80263304161809898486953580976564463281
  | -388 =>  40131652080904949243476790488282231641
  | -387 => 100329130202262373108691976220705579101
  | -386 =>  50164565101131186554345988110352789551
  | -385 => 125411412752827966385864970275881973876
  | -384 =>  62705706376413983192932485137940986938
  | -383 => 156764265941034957982331212844852467345
  | -382 =>  78382132970517478991165606422426233673
  | -381 =>  39191066485258739495582803211213116837
  | -380 =>  97977666213146848738957008028032792091
  | -379 =>  48988833106573424369478504014016396046
  | -378 => 122472082766433560923696260035040990114
  | -377 =>  61236041383216780461848130017520495057
  | -376 => 153090103458041951154620325043801237642
  | -375 =>  76545051729020975577310162521900618821
  | -374 =>  38272525864510487788655081260950309411
  | -373 =>  95681314661276219471637703152375773526
  | -372 =>  47840657330638109735818851576187886763
  | -371 => 119601643326595274339547128940469716908
  | -370 =>  59800821663297637169773564470234858454
  | -369 => 149502054158244092924433911175587146135
  | -368 =>  74751027079122046462216955587793573068
  | -367 =>  37375513539561023231108477793896786534
  | -366 =>  93438783848902558077771194484741966334
  | -365 =>  46719391924451279038885597242370983167
  | -364 => 116798479811128197597213993105927457918
  | -363 =>  58399239905564098798606996552963728959
  | -362 => 145998099763910246996517491382409322397
  | -361 =>  72999049881955123498258745691204661199
  | -360 =>  36499524940977561749129372845602330600
  | -359 =>  91248812352443904372823432114005826498
  | -358 =>  45624406176221952186411716057002913249
  | -357 => 114061015440554880466029290142507283123
  | -356 =>  57030507720277440233014645071253641562
  | -355 => 142576269300693600582536612678134103903
  | -354 =>  71288134650346800291268306339067051952
  | -353 =>  35644067325173400145634153169533525976
  | -352 =>  89110168312933500364085382923833814940
  | -351 =>  44555084156466750182042691461916907470
  | -350 => 111387710391166875455106728654792268675
  | -349 =>  55693855195583437727553364327396134338
  | -348 => 139234637988958594318883410818490335843
  | -347 =>  69617318994479297159441705409245167922
  | -346 =>  34808659497239648579720852704622583961
  | -345 =>  87021648743099121449302131761556459902
  | -344 =>  43510824371549560724651065880778229951
  | -343 => 108777060928873901811627664701945574878
  | -342 =>  54388530464436950905813832350972787439
  | -341 => 135971326161092377264534580877431968597
  | -340 =>  67985663080546188632267290438715984299
  | -339 => 169964157701365471580668226096789960746
  | -338 =>  84982078850682735790334113048394980373
  | -337 =>  42491039425341367895167056524197490187
  | -336 => 106227598563353419737917641310493725466
  | -335 =>  53113799281676709868958820655246862733
  | -334 => 132784498204191774672397051638117156833
  | -333 =>  66392249102095887336198525819058578417
  | -332 => 165980622755239718340496314547646446041
  | -331 =>  82990311377619859170248157273823223021
  | -330 =>  41495155688809929585124078636911611511
  | -329 => 103737889222024823962810196592279028776
  | -328 =>  51868944611012411981405098296139514388
  | -327 => 129672361527531029953512745740348785970
  | -326 =>  64836180763765514976756372870174392985
  | -325 => 162090451909413787441890932175435982462
  | -324 =>  81045225954706893720945466087717991231
  | -323 =>  40522612977353446860472733043858995616
  | -322 => 101306532443383617151181832609647489039
  | -321 =>  50653266221691808575590916304823744520
  | -320 => 126633165554229521438977290762059361298
  | -319 =>  63316582777114760719488645381029680649
  | -318 => 158291456942786901798721613452574201623
  | -317 =>  79145728471393450899360806726287100812
  | -316 =>  39572864235696725449680403363143550406
  | -315 =>  98932160589241813624201008407858876015
  | -314 =>  49466080294620906812100504203929438008
  | -313 => 123665200736552267030251260509823595018
  | -312 =>  61832600368276133515125630254911797509
  | -311 => 154581500920690333787814075637279493772
  | -310 =>  77290750460345166893907037818639746886
  | -309 =>  38645375230172583446953518909319873443
  | -308 =>  96613438075431458617383797273299683608
  | -307 =>  48306719037715729308691898636649841804
  | -306 => 120766797594289323271729746591624604510
  | -305 =>  60383398797144661635864873295812302255
  | -304 => 150958496992861654089662183239530755637
  | -303 =>  75479248496430827044831091619765377819
  | -302 =>  37739624248215413522415545809882688910
  | -301 =>  94349060620538533806038864524706722273
  | -300 =>  47174530310269266903019432262353361137
  | -299 => 117936325775673167257548580655883402842
  | -298 =>  58968162887836583628774290327941701421
  | -297 => 147420407219591459071935725819854253552
  | -296 =>  73710203609795729535967862909927126776
  | -295 =>  36855101804897864767983931454963563388
  | -294 =>  92137754512244661919959828637408908470
  | -293 =>  46068877256122330959979914318704454235
  | -292 => 115172193140305827399949785796761135588
  | -291 =>  57586096570152913699974892898380567794
  | -290 => 143965241425382284249937232245951419484
  | -289 =>  71982620712691142124968616122975709742
  | -288 =>  35991310356345571062484308061487854871
  | -287 =>  89978275890863927656210770153719637178
  | -286 =>  44989137945431963828105385076859818589
  | -285 => 112472844863579909570263462692149546472
  | -284 =>  56236422431789954785131731346074773236
  | -283 => 140591056079474886962829328365186933090
  | -282 =>  70295528039737443481414664182593466545
  | -281 =>  35147764019868721740707332091296733273
  | -280 =>  87869410049671804351768330228241833182
  | -279 =>  43934705024835902175884165114120916591
  | -278 => 109836762562089755439710412785302291477
  | -277 =>  54918381281044877719855206392651145739
  | -276 => 137295953202612194299638015981627864346
  | -275 =>  68647976601306097149819007990813932173
  | -274 =>  34323988300653048574909503995406966087
  | -273 =>  85809970751632621437273759988517415216
  | -272 =>  42904985375816310718636879994258707608
  | -271 => 107262463439540776796592199985646769020
  | -270 =>  53631231719770388398296099992823384510
  | -269 => 134078079299425970995740249982058461275
  | -268 =>  67039039649712985497870124991029230638
  | -267 => 167597599124282463744675312477573076594
  | -266 =>  83798799562141231872337656238786538297
  | -265 =>  41899399781070615936168828119393269149
  | -264 => 104748499452676539840422070298483172871
  | -263 =>  52374249726338269920211035149241586436
  | -262 => 130935624315845674800527587873103966089
  | -261 =>  65467812157922837400263793936551983045
  | -260 => 163669530394807093500659484841379957611
  | -259 =>  81834765197403546750329742420689978806
  | -258 =>  40917382598701773375164871210344989403
  | -257 => 102293456496754433437912178025862473507
  | -256 =>  51146728248377216718956089012931236754
  | -255 => 127866820620943041797390222532328091884
  | -254 =>  63933410310471520898695111266164045942
  | -253 => 159833525776178802246737778165410114855
  | -252 =>  79916762888089401123368889082705057428
  | -251 =>  39958381444044700561684444541352528714
  | -250 =>  99895953610111751404211111353381321784
  | -249 =>  49947976805055875702105555676690660892
  | -248 => 124869942012639689255263889191726652230
  | -247 =>  62434971006319844627631944595863326115
  | -246 => 156087427515799611569079861489658315288
  | -245 =>  78043713757899805784539930744829157644
  | -244 =>  39021856878949902892269965372414578822
  | -243 =>  97554642197374757230674913431036447055
  | -242 =>  48777321098687378615337456715518223528
  | -241 => 121943302746718446538343641788795558819
  | -240 =>  60971651373359223269171820894397779410
  | -239 => 152429128433398058172929552235994448523
  | -238 =>  76214564216699029086464776117997224262
  | -237 =>  38107282108349514543232388058998612131
  | -236 =>  95268205270873786358080970147496530327
  | -235 =>  47634102635436893179040485073748265164
  | -234 => 119085256588592232947601212684370662909
  | -233 =>  59542628294296116473800606342185331455
  | -232 => 148856570735740291184501515855463328636
  | -231 =>  74428285367870145592250757927731664318
  | -230 =>  37214142683935072796125378963865832159
  | -229 =>  93035356709837681990313447409664580398
  | -228 =>  46517678354918840995156723704832290199
  | -227 => 116294195887297102487891809262080725497
  | -226 =>  58147097943648551243945904631040362749
  | -225 => 145367744859121378109864761577600906871
  | -224 =>  72683872429560689054932380788800453436
  | -223 =>  36341936214780344527466190394400226718
  | -222 =>  90854840536950861318665475986000566795
  | -221 =>  45427420268475430659332737993000283398
  | -220 => 113568550671188576648331844982500708493
  | -219 =>  56784275335594288324165922491250354247
  | -218 => 141960688338985720810414806228125885616
  | -217 =>  70980344169492860405207403114062942808
  | -216 =>  35490172084746430202603701557031471404
  | -215 =>  88725430211866075506509253892578678510
  | -214 =>  44362715105933037753254626946289339255
  | -213 => 110906787764832594383136567365723348138
  | -212 =>  55453393882416297191568283682861674069
  | -211 => 138633484706040742978920709207154185172
  | -210 =>  69316742353020371489460354603577092586
  | -209 =>  34658371176510185744730177301788546293
  | -208 =>  86645927941275464361825443254471365733
  | -207 =>  43322963970637732180912721627235682867
  | -206 => 108307409926594330452281804068089207166
  | -205 =>  54153704963297165226140902034044603583
  | -204 => 135384262408242913065352255085111508957
  | -203 =>  67692131204121456532676127542555754479
  | -202 => 169230328010303641331690318856389386197
  | -201 =>  84615164005151820665845159428194693099
  | -200 =>  42307582002575910332922579714097346550
  | -199 => 105768955006439775832306449285243366373
  | -198 =>  52884477503219887916153224642621683187
  | -197 => 132211193758049719790383061606554207966
  | -196 =>  66105596879024859895191530803277103983
  | -195 => 165263992197562149737978827008192759958
  | -194 =>  82631996098781074868989413504096379979
  | -193 =>  41315998049390537434494706752048189990
  | -192 => 103289995123476343586236766880120474974
  | -191 =>  51644997561738171793118383440060237487
  | -190 => 129112493904345429482795958600150593717
  | -189 =>  64556246952172714741397979300075296859
  | -188 => 161390617380431786853494948250188242146
  | -187 =>  80695308690215893426747474125094121073
  | -186 =>  40347654345107946713373737062547060537
  | -185 => 100869135862769866783434342656367651342
  | -184 =>  50434567931384933391717171328183825671
  | -183 => 126086419828462333479292928320459564177
  | -182 =>  63043209914231166739646464160229782089
  | -181 => 157608024785577916849116160400574455221
  | -180 =>  78804012392788958424558080200287227611
  | -179 =>  39402006196394479212279040100143613806
  | -178 =>  98505015490986198030697600250359034513
  | -177 =>  49252507745493099015348800125179517257
  | -176 => 123131269363732747538372000312948793141
  | -175 =>  61565634681866373769186000156474396571
  | -174 => 153914086704665934422965000391185991427
  | -173 =>  76957043352332967211482500195592995714
  | -172 =>  38478521676166483605741250097796497857
  | -171 =>  96196304190416209014353125244491244642
  | -170 =>  48098152095208104507176562622245622321
  | -169 => 120245380238020261267941406555614055802
  | -168 =>  60122690119010130633970703277807027901
  | -167 => 150306725297525326584926758194517569753
  | -166 =>  75153362648762663292463379097258784877
  | -165 =>  37576681324381331646231689548629392439
  | -164 =>  93941703310953329115579223871573481096
  | -163 =>  46970851655476664557789611935786740548
  | -162 => 117427129138691661394474029839466851369
  | -161 =>  58713564569345830697237014919733425685
  | -160 => 146783911423364576743092537299333564211
  | -159 =>  73391955711682288371546268649666782106
  | -158 =>  36695977855841144185773134324833391053
  | -157 =>  91739944639602860464432835812083477632
  | -156 =>  45869972319801430232216417906041738816
  | -155 => 114674930799503575580541044765104347040
  | -154 =>  57337465399751787790270522382552173520
  | -153 => 143343663499379469475676305956380433800
  | -152 =>  71671831749689734737838152978190216900
  | -151 =>  35835915874844867368919076489095108450
  | -150 =>  89589789687112168422297691222737771125
  | -149 =>  44794894843556084211148845611368885563
  | -148 => 111987237108890210527872114028422213907
  | -147 =>  55993618554445105263936057014211106954
  | -146 => 139984046386112763159840142535527767383
  | -145 =>  69992023193056381579920071267763883692
  | -144 =>  34996011596528190789960035633881941846
  | -143 =>  87490028991320476974900089084704854615
  | -142 =>  43745014495660238487450044542352427308
  | -141 => 109362536239150596218625111355881068268
  | -140 =>  54681268119575298109312555677940534134
  | -139 => 136703170298938245273281389194851335335
  | -138 =>  68351585149469122636640694597425667668
  | -137 =>  34175792574734561318320347298712833834
  | -136 =>  85439481436836403295800868246782084585
  | -135 =>  42719740718418201647900434123391042293
  | -134 => 106799351796045504119751085308477605731
  | -133 =>  53399675898022752059875542654238802866
  | -132 => 133499189745056880149688856635597007163
  | -131 =>  66749594872528440074844428317798503582
  | -130 => 166873987181321100187111070794496258954
  | -129 =>  83436993590660550093555535397248129477
  | -128 =>  41718496795330275046777767698624064739
  | -127 => 104296241988325687616944419246560161846
  | -126 =>  52148120994162843808472209623280080923
  | -125 => 130370302485407109521180524058200202308
  | -124 =>  65185151242703554760590262029100101154
  | -123 => 162962878106758886901475655072750252885
  | -122 =>  81481439053379443450737827536375126443
  | -121 =>  40740719526689721725368913768187563222
  | -120 => 101851798816724304313422284420468908053
  | -119 =>  50925899408362152156711142210234454027
  | -118 => 127314748520905380391777855525586135066
  | -117 =>  63657374260452690195888927762793067533
  | -116 => 159143435651131725489722319406982668833
  | -115 =>  79571717825565862744861159703491334417
  | -114 =>  39785858912782931372430579851745667209
  | -113 =>  99464647281957328431076449629364168021
  | -112 =>  49732323640978664215538224814682084011
  | -111 => 124330809102446660538845562036705210026
  | -110 =>  62165404551223330269422781018352605013
  | -109 => 155413511378058325673556952545881512532
  | -108 =>  77706755689029162836778476272940756266
  | -107 =>  38853377844514581418389238136470378133
  | -106 =>  97133444611286453545973095341175945333
  | -105 =>  48566722305643226772986547670587972667
  | -104 => 121416805764108066932466369176469931666
  | -103 =>  60708402882054033466233184588234965833
  | -102 => 151771007205135083665582961470587414582
  | -101 =>  75885503602567541832791480735293707291
  | -100 =>  37942751801283770916395740367646853646
  | -99 =>  94856879503209427290989350919117134114
  | -98 =>  47428439751604713645494675459558567057
  | -97 => 118571099379011784113736688648896417642
  | -96 =>  59285549689505892056868344324448208821
  | -95 => 148213874223764730142170860811120522053
  | -94 =>  74106937111882365071085430405560261027
  | -93 =>  37053468555941182535542715202780130514
  | -92 =>  92633671389852956338856788006950326283
  | -91 =>  46316835694926478169428394003475163142
  | -90 => 115792089237316195423570985008687907854
  | -89 =>  57896044618658097711785492504343953927
  | -88 => 144740111546645244279463731260859884817
  | -87 =>  72370055773322622139731865630429942409
  | -86 =>  36185027886661311069865932815214971205
  | -85 =>  90462569716653277674664832038037428011
  | -84 =>  45231284858326638837332416019018714006
  | -83 => 113078212145816597093331040047546785013
  | -82 =>  56539106072908298546665520023773392507
  | -81 => 141347765182270746366663800059433481267
  | -80 =>  70673882591135373183331900029716740634
  | -79 =>  35336941295567686591665950014858370317
  | -78 =>  88342353238919216479164875037145925792
  | -77 =>  44171176619459608239582437518572962896
  | -76 => 110427941548649020598956093796432407240
  | -75 =>  55213970774324510299478046898216203620
  | -74 => 138034926935811275748695117245540509050
  | -73 =>  69017463467905637874347558622770254525
  | -72 =>  34508731733952818937173779311385127263
  | -71 =>  86271829334882047342934448278462818156
  | -70 =>  43135914667441023671467224139231409078
  | -69 => 107839786668602559178668060348078522695
  | -68 =>  53919893334301279589334030174039261348
  | -67 => 134799733335753198973335075435098153369
  | -66 =>  67399866667876599486667537717549076685
  | -65 => 168499666669691498716668844293872691711
  | -64 =>  84249833334845749358334422146936345856
  | -63 =>  42124916667422874679167211073468172928
  | -62 => 105312291668557186697918027683670432319
  | -61 =>  52656145834278593348959013841835216160
  | -60 => 131640364585696483372397534604588040399
  | -59 =>  65820182292848241686198767302294020200
  | -58 => 164550455732120604215496918255735050499
  | -57 =>  82275227866060302107748459127867525250
  | -56 =>  41137613933030151053874229563933762625
  | -55 => 102844034832575377634685573909834406562
  | -54 =>  51422017416287688817342786954917203281
  | -53 => 128555043540719222043356967387293008202
  | -52 =>  64277521770359611021678483693646504101
  | -51 => 160693804425899027554196209234116260253
  | -50 =>  80346902212949513777098104617058130127
  | -49 =>  40173451106474756888549052308529065064
  | -48 => 100433627766186892221372630771322662658
  | -47 =>  50216813883093446110686315385661331329
  | -46 => 125542034707733615276715788464153328323
  | -45 =>  62771017353866807638357894232076664162
  | -44 => 156927543384667019095894735580191660403
  | -43 =>  78463771692333509547947367790095830202
  | -42 =>  39231885846166754773973683895047915101
  | -41 =>  98079714615416886934934209737619787752
  | -40 =>  49039857307708443467467104868809893876
  | -39 => 122599643269271108668667762172024734690
  | -38 =>  61299821634635554334333881086012367345
  | -37 => 153249554086588885835834702715030918362
  | -36 =>  76624777043294442917917351357515459181
  | -35 =>  38312388521647221458958675678757729591
  | -34 =>  95780971304118053647396689196894323977
  | -33 =>  47890485652059026823698344598447161989
  | -32 => 119726214130147567059245861496117904971
  | -31 =>  59863107065073783529622930748058952486
  | -30 => 149657767662684458824057326870147381213
  | -29 =>  74828883831342229412028663435073690607
  | -28 =>  37414441915671114706014331717536845304
  | -27 =>  93536104789177786765035829293842113258
  | -26 =>  46768052394588893382517914646921056629
  | -25 => 116920130986472233456294786617302641573
  | -24 =>  58460065493236116728147393308651320787
  | -23 => 146150163733090291820368483271628301966
  | -22 =>  73075081866545145910184241635814150983
  | -21 =>  36537540933272572955092120817907075492
  | -20 =>  91343852333181432387730302044767688729
  | -19 =>  45671926166590716193865151022383844365
  | -18 => 114179815416476790484662877555959610911
  | -17 =>  57089907708238395242331438777979805456
  | -16 => 142724769270595988105828596944949513639
  | -15 =>  71362384635297994052914298472474756820
  | -14 =>  35681192317648997026457149236237378410
  | -13 =>  89202980794122492566142873090593446024
  | -12 =>  44601490397061246283071436545296723012
  | -11 => 111503725992653115707678591363241807530
  | -10 =>  55751862996326557853839295681620903765
  |  -9 => 139379657490816394634598239204052259413
  |  -8 =>  69689828745408197317299119602026129707
  |  -7 =>  34844914372704098658649559801013064854
  |  -6 =>  87112285931760246646623899502532662133
  |  -5 =>  43556142965880123323311949751266331067
  |  -4 => 108890357414700308308279874378165827666
  |  -3 =>  54445178707350154154139937189082913833
  |  -2 => 136112946768375385385349842972707284583
  |  -1 =>  68056473384187692692674921486353642292
  |   0 => 170141183460469231731687303715884105728
  |   1 =>  85070591730234615865843651857942052864
  |   2 =>  42535295865117307932921825928971026432
  |   3 => 106338239662793269832304564822427566080
  |   4 =>  53169119831396634916152282411213783040
  |   5 => 132922799578491587290380706028034457600
  |   6 =>  66461399789245793645190353014017228800
  |   7 => 166153499473114484112975882535043072000
  |   8 =>  83076749736557242056487941267521536000
  |   9 =>  41538374868278621028243970633760768000
  |  10 => 103845937170696552570609926584401920000
  |  11 =>  51922968585348276285304963292200960000
  |  12 => 129807421463370690713262408230502400000
  |  13 =>  64903710731685345356631204115251200000
  |  14 => 162259276829213363391578010288128000000
  |  15 =>  81129638414606681695789005144064000000
  |  16 =>  40564819207303340847894502572032000000
  |  17 => 101412048018258352119736256430080000000
  |  18 =>  50706024009129176059868128215040000000
  |  19 => 126765060022822940149670320537600000000
  |  20 =>  63382530011411470074835160268800000000
  |  21 => 158456325028528675187087900672000000000
  |  22 =>  79228162514264337593543950336000000000
  |  23 =>  39614081257132168796771975168000000000
  |  24 =>  99035203142830421991929937920000000000
  |  25 =>  49517601571415210995964968960000000000
  |  26 => 123794003928538027489912422400000000000
  |  27 =>  61897001964269013744956211200000000000
  |  28 => 154742504910672534362390528000000000000
  |  29 =>  77371252455336267181195264000000000000
  |  30 =>  38685626227668133590597632000000000000
  |  31 =>  96714065569170333976494080000000000000
  |  32 =>  48357032784585166988247040000000000000
  |  33 => 120892581961462917470617600000000000000
  |  34 =>  60446290980731458735308800000000000000
  |  35 => 151115727451828646838272000000000000000
  |  36 =>  75557863725914323419136000000000000000
  |  37 =>  37778931862957161709568000000000000000
  |  38 =>  94447329657392904273920000000000000000
  |  39 =>  47223664828696452136960000000000000000
  |  40 => 118059162071741130342400000000000000000
  |  41 =>  59029581035870565171200000000000000000
  |  42 => 147573952589676412928000000000000000000
  |  43 =>  73786976294838206464000000000000000000
  |  44 =>  36893488147419103232000000000000000000
  |  45 =>  92233720368547758080000000000000000000
  |  46 =>  46116860184273879040000000000000000000
  |  47 => 115292150460684697600000000000000000000
  |  48 =>  57646075230342348800000000000000000000
  |  49 => 144115188075855872000000000000000000000
  |  50 =>  72057594037927936000000000000000000000
  |  51 =>  36028797018963968000000000000000000000
  |  52 =>  90071992547409920000000000000000000000
  |  53 =>  45035996273704960000000000000000000000
  |  54 => 112589990684262400000000000000000000000
  |  55 =>  56294995342131200000000000000000000000
  |  56 => 140737488355328000000000000000000000000
  |  57 =>  70368744177664000000000000000000000000
  |  58 =>  35184372088832000000000000000000000000
  |  59 =>  87960930222080000000000000000000000000
  |  60 =>  43980465111040000000000000000000000000
  |  61 => 109951162777600000000000000000000000000
  |  62 =>  54975581388800000000000000000000000000
  |  63 => 137438953472000000000000000000000000000
  |  64 =>  68719476736000000000000000000000000000
  |  65 =>  34359738368000000000000000000000000000
  |  66 =>  85899345920000000000000000000000000000
  |  67 =>  42949672960000000000000000000000000000
  |  68 => 107374182400000000000000000000000000000
  |  69 =>  53687091200000000000000000000000000000
  |  70 => 134217728000000000000000000000000000000
  |  71 =>  67108864000000000000000000000000000000
  |  72 => 167772160000000000000000000000000000000
  |  73 =>  83886080000000000000000000000000000000
  |  74 =>  41943040000000000000000000000000000000
  |  75 => 104857600000000000000000000000000000000
  |  76 =>  52428800000000000000000000000000000000
  |  77 => 131072000000000000000000000000000000000
  |  78 =>  65536000000000000000000000000000000000
  |  79 => 163840000000000000000000000000000000000
  |  80 =>  81920000000000000000000000000000000000
  |  81 =>  40960000000000000000000000000000000000
  |  82 => 102400000000000000000000000000000000000
  |  83 =>  51200000000000000000000000000000000000
  |  84 => 128000000000000000000000000000000000000
  |  85 =>  64000000000000000000000000000000000000
  |  86 => 160000000000000000000000000000000000000
  |  87 =>  80000000000000000000000000000000000000
  |  88 =>  40000000000000000000000000000000000000
  |  89 => 100000000000000000000000000000000000000
  |  90 =>  50000000000000000000000000000000000000
  |  91 => 125000000000000000000000000000000000000
  |  92 =>  62500000000000000000000000000000000000
  |  93 => 156250000000000000000000000000000000000
  |  94 =>  78125000000000000000000000000000000000
  |  95 =>  39062500000000000000000000000000000000
  |  96 =>  97656250000000000000000000000000000000
  |  97 =>  48828125000000000000000000000000000000
  |  98 => 122070312500000000000000000000000000000
  |  99 =>  61035156250000000000000000000000000000
  | 100 => 152587890625000000000000000000000000000
  | 101 =>  76293945312500000000000000000000000000
  | 102 =>  38146972656250000000000000000000000000
  | 103 =>  95367431640625000000000000000000000000
  | 104 =>  47683715820312500000000000000000000000
  | 105 => 119209289550781250000000000000000000000
  | 106 =>  59604644775390625000000000000000000000
  | 107 => 149011611938476562500000000000000000000
  | 108 =>  74505805969238281250000000000000000000
  | 109 =>  37252902984619140625000000000000000000
  | 110 =>  93132257461547851562500000000000000000
  | 111 =>  46566128730773925781250000000000000000
  | 112 => 116415321826934814453125000000000000000
  | 113 =>  58207660913467407226562500000000000000
  | 114 => 145519152283668518066406250000000000000
  | 115 =>  72759576141834259033203125000000000000
  | 116 =>  36379788070917129516601562500000000000
  | 117 =>  90949470177292823791503906250000000000
  | 118 =>  45474735088646411895751953125000000000
  | 119 => 113686837721616029739379882812500000000
  | 120 =>  56843418860808014869689941406250000000
  | 121 => 142108547152020037174224853515625000000
  | 122 =>  71054273576010018587112426757812500000
  | 123 =>  35527136788005009293556213378906250000
  | 124 =>  88817841970012523233890533447265625000
  | 125 =>  44408920985006261616945266723632812500
  | 126 => 111022302462515654042363166809082031250
  | 127 =>  55511151231257827021181583404541015625
  | 128 => 138777878078144567552953958511352539063
  | 129 =>  69388939039072283776476979255676269532
  | 130 =>  34694469519536141888238489627838134766
  | 131 =>  86736173798840354720596224069595336915
  | 132 =>  43368086899420177360298112034797668458
  | 133 => 108420217248550443400745280086994171143
  | 134 =>  54210108624275221700372640043497085572
  | 135 => 135525271560688054250931600108742713929
  | 136 =>  67762635780344027125465800054371356965
  | 137 => 169406589450860067813664500135928392411
  | 138 =>  84703294725430033906832250067964196206
  | 139 =>  42351647362715016953416125033982098103
  | 140 => 105879118406787542383540312584955245257
  | 141 =>  52939559203393771191770156292477622629
  | 142 => 132348898008484427979425390731194056571
  | 143 =>  66174449004242213989712695365597028286
  | 144 => 165436122510605534974281738413992570714
  | 145 =>  82718061255302767487140869206996285357
  | 146 =>  41359030627651383743570434603498142679
  | 147 => 103397576569128459358926086508745356696
  | 148 =>  51698788284564229679463043254372678348
  | 149 => 129246970711410574198657608135931695870
  | 150 =>  64623485355705287099328804067965847935
  | 151 => 161558713389263217748322010169914619838
  | 152 =>  80779356694631608874161005084957309919
  | 153 =>  40389678347315804437080502542478654960
  | 154 => 100974195868289511092701256356196637399
  | 155 =>  50487097934144755546350628178098318700
  | 156 => 126217744835361888865876570445245796748
  | 157 =>  63108872417680944432938285222622898374
  | 158 => 157772181044202361082345713056557245935
  | 159 =>  78886090522101180541172856528278622968
  | 160 =>  39443045261050590270586428264139311484
  | 161 =>  98607613152626475676466070660348278710
  | 162 =>  49303806576313237838233035330174139355
  | 163 => 123259516440783094595582588325435348387
  | 164 =>  61629758220391547297791294162717674194
  | 165 => 154074395550978868244478235406794185484
  | 166 =>  77037197775489434122239117703397092742
  | 167 =>  38518598887744717061119558851698546371
  | 168 =>  96296497219361792652798897129246365927
  | 169 =>  48148248609680896326399448564623182964
  | 170 => 120370621524202240815998621411557957409
  | 171 =>  60185310762101120407999310705778978705
  | 172 => 150463276905252801019998276764447446761
  | 173 =>  75231638452626400509999138382223723381
  | 174 =>  37615819226313200254999569191111861691
  | 175 =>  94039548065783000637498922977779654226
  | 176 =>  47019774032891500318749461488889827113
  | 177 => 117549435082228750796873653722224567782
  | 178 =>  58774717541114375398436826861112283891
  | 179 => 146936793852785938496092067152780709728
  | 180 =>  73468396926392969248046033576390354864
  | 181 =>  36734198463196484624023016788195177432
  | 182 =>  91835496157991211560057541970487943580
  | 183 =>  45917748078995605780028770985243971790
  | 184 => 114794370197489014450071927463109929475
  | 185 =>  57397185098744507225035963731554964738
  | 186 => 143492962746861268062589909328887411844
  | 187 =>  71746481373430634031294954664443705922
  | 188 =>  35873240686715317015647477332221852961
  | 189 =>  89683101716788292539118693330554632402
  | 190 =>  44841550858394146269559346665277316201
  | 191 => 112103877145985365673898366663193290503
  | 192 =>  56051938572992682836949183331596645252
  | 193 => 140129846432481707092372958328991613129
  | 194 =>  70064923216240853546186479164495806565
  | 195 =>  35032461608120426773093239582247903283
  | 196 =>  87581154020301066932733098955619758206
  | 197 =>  43790577010150533466366549477809879103
  | 198 => 109476442525376333665916373694524697757
  | 199 =>  54738221262688166832958186847262348879
  | 200 => 136845553156720417082395467118155872196
  | 201 =>  68422776578360208541197733559077936098
  | 202 =>  34211388289180104270598866779538968049
  | 203 =>  85528470722950260676497166948847420123
  | 204 =>  42764235361475130338248583474423710062
  | 205 => 106910588403687825845621458686059275153
  | 206 =>  53455294201843912922810729343029637577
  | 207 => 133638235504609782307026823357574093941
  | 208 =>  66819117752304891153513411678787046971
  | 209 => 167047794380762227883783529196967617426
  | 210 =>  83523897190381113941891764598483808713
  | 211 =>  41761948595190556970945882299241904357
  | 212 => 104404871487976392427364705748104760892
  | 213 =>  52202435743988196213682352874052380446
  | 214 => 130506089359970490534205882185130951115
  | 215 =>  65253044679985245267102941092565475558
  | 216 => 163132611699963113167757352731413688893
  | 217 =>  81566305849981556583878676365706844447
  | 218 =>  40783152924990778291939338182853422224
  | 219 => 101957882312476945729848345457133555558
  | 220 =>  50978941156238472864924172728566777779
  | 221 => 127447352890596182162310431821416944448
  | 222 =>  63723676445298091081155215910708472224
  | 223 => 159309191113245227702888039776771180560
  | 224 =>  79654595556622613851444019888385590280
  | 225 =>  39827297778311306925722009944192795140
  | 226 =>  99568244445778267314305024860481987850
  | 227 =>  49784122222889133657152512430240993925
  | 228 => 124460305557222834142881281075602484812
  | 229 =>  62230152778611417071440640537801242406
  | 230 => 155575381946528542678601601344503106015
  | 231 =>  77787690973264271339300800672251553008
  | 232 =>  38893845486632135669650400336125776504
  | 233 =>  97234613716580339174126000840314441260
  | 234 =>  48617306858290169587063000420157220630
  | 235 => 121543267145725423967657501050393051575
  | 236 =>  60771633572862711983828750525196525788
  | 237 => 151929083932156779959571876312991314468
  | 238 =>  75964541966078389979785938156495657234
  | 239 =>  37982270983039194989892969078247828617
  | 240 =>  94955677457597987474732422695619571543
  | 241 =>  47477838728798993737366211347809785772
  | 242 => 118694596821997484343415528369524464428
  | 243 =>  59347298410998742171707764184762232214
  | 244 => 148368246027496855429269410461905580535
  | 245 =>  74184123013748427714634705230952790268
  | 246 =>  37092061506874213857317352615476395134
  | 247 =>  92730153767185534643293381538690987835
  | 248 =>  46365076883592767321646690769345493918
  | 249 => 115912692208981918304116726923363734793
  | 250 =>  57956346104490959152058363461681867397
  | 251 => 144890865261227397880145908654204668491
  | 252 =>  72445432630613698940072954327102334246
  | 253 =>  36222716315306849470036477163551167123
  | 254 =>  90556790788267123675091192908877917807
  | 255 =>  45278395394133561837545596454438958904
  | 256 => 113195988485333904593863991136097397259
  | 257 =>  56597994242666952296931995568048698630
  | 258 => 141494985606667380742329988920121746574
  | 259 =>  70747492803333690371164994460060873287
  | 260 =>  35373746401666845185582497230030436644
  | 261 =>  88434366004167112963956243075076091609
  | 262 =>  44217183002083556481978121537538045805
  | 263 => 110542957505208891204945303843845114511
  | 264 =>  55271478752604445602472651921922557256
  | 265 => 138178696881511114006181629804806393138
  | 266 =>  69089348440755557003090814902403196569
  | 267 =>  34544674220377778501545407451201598285
  | 268 =>  86361685550944446253863518628003995712
  | 269 =>  43180842775472223126931759314001997856
  | 270 => 107952106938680557817329398285004994639
  | 271 =>  53976053469340278908664699142502497320
  | 272 => 134940133673350697271661747856256243299
  | 273 =>  67470066836675348635830873928128121650
  | 274 => 168675167091688371589577184820320304124
  | 275 =>  84337583545844185794788592410160152062
  | 276 =>  42168791772922092897394296205080076031
  | 277 => 105421979432305232243485740512700190078
  | 278 =>  52710989716152616121742870256350095039
  | 279 => 131777474290381540304357175640875237597
  | 280 =>  65888737145190770152178587820437618799
  | 281 => 164721842862976925380446469551094046996
  | 282 =>  82360921431488462690223234775547023498
  | 283 =>  41180460715744231345111617387773511749
  | 284 => 102951151789360578362779043469433779373
  | 285 =>  51475575894680289181389521734716889687
  | 286 => 128688939736700722953473804336792224216
  | 287 =>  64344469868350361476736902168396112108
  | 288 => 160861174670875903691842255420990280270
  | 289 =>  80430587335437951845921127710495140135
  | 290 =>  40215293667718975922960563855247570068
  | 291 => 100538234169297439807401409638118925169
  | 292 =>  50269117084648719903700704819059462585
  | 293 => 125672792711621799759251762047648656461
  | 294 =>  62836396355810899879625881023824328231
  | 295 => 157090990889527249699064702559560820576
  | 296 =>  78545495444763624849532351279780410288
  | 297 =>  39272747722381812424766175639890205144
  | 298 =>  98181869305954531061915439099725512860
  | 299 =>  49090934652977265530957719549862756430
  | 300 => 122727336632443163827394298874656891075
  | 301 =>  61363668316221581913697149437328445538
  | 302 => 153409170790553954784242873593321113843
  | 303 =>  76704585395276977392121436796660556922
  | 304 =>  38352292697638488696060718398330278461
  | 305 =>  95880731744096221740151795995825696152
  | 306 =>  47940365872048110870075897997912848076
  | 307 => 119850914680120277175189744994782120190
  | 308 =>  59925457340060138587594872497391060095
  | 309 => 149813643350150346468987181243477650238
  | 310 =>  74906821675075173234493590621738825119
  | 311 =>  37453410837537586617246795310869412560
  | 312 =>  93633527093843966543116988277173531399
  | 313 =>  46816763546921983271558494138586765700
  | 314 => 117041908867304958178896235346466914248
  | 315 =>  58520954433652479089448117673233457124
  | 316 => 146302386084131197723620294183083642810
  | 317 =>  73151193042065598861810147091541821405
  | 318 =>  36575596521032799430905073545770910703
  | 319 =>  91438991302581998577262683864427276757
  | 320 =>  45719495651290999288631341932213638379
  | 321 => 114298739128227498221578354830534095946
  | 322 =>  57149369564113749110789177415267047973
  | 323 => 142873423910284372776972943538167619932
  | 324 =>  71436711955142186388486471769083809966
  | 325 =>  35718355977571093194243235884541904983
  | 326 =>  89295889943927732985608089711354762458
  | 327 =>  44647944971963866492804044855677381229
  | 328 => 111619862429909666232010112139193453072
  | 329 =>  55809931214954833116005056069596726536
  | 330 => 139524828037387082790012640173991816340
  | 331 =>  69762414018693541395006320086995908170
  | 332 =>  34881207009346770697503160043497954085
  | 333 =>  87203017523366926743757900108744885213
  | 334 =>  43601508761683463371878950054372442607
  | 335 => 109003771904208658429697375135931106516
  | 336 =>  54501885952104329214848687567965553258
  | 337 => 136254714880260823037121718919913883144
  | 338 =>  68127357440130411518560859459956941572
  | 339 =>  34063678720065205759280429729978470786
  | 340 =>  85159196800163014398201074324946176965
  | 341 =>  42579598400081507199100537162473088483
  | 342 => 106448996000203767997751342906182721207
  | 343 =>  53224498000101883998875671453091360604
  | 344 => 133061245000254709997189178632728401508
  | 345 =>  66530622500127354998594589316364200754
  | 346 => 166326556250318387496486473290910501885
  | 347 =>  83163278125159193748243236645455250943
  | 348 =>  41581639062579596874121618322727625472
  | 349 => 103954097656448992185304045806819063678
  | 350 =>  51977048828224496092652022903409531839
  | 351 => 129942622070561240231630057258523829598
  | 352 =>  64971311035280620115815028629261914799
  | 353 => 162428277588201550289537571573154786997
  | 354 =>  81214138794100775144768785786577393499
  | 355 =>  40607069397050387572384392893288696750
  | 356 => 101517673492625968930960982233221741873
  | 357 =>  50758836746312984465480491116610870937
  | 358 => 126897091865782461163701227791527177342
  | 359 =>  63448545932891230581850613895763588671
  | 360 => 158621364832228076454626534739408971677
  | 361 =>  79310682416114038227313267369704485839
  | 362 =>  39655341208057019113656633684852242920
  | 363 =>  99138353020142547784141584212130607298
  | 364 =>  49569176510071273892070792106065303649
  | 365 => 123922941275178184730176980265163259123
  | 366 =>  61961470637589092365088490132581629562
  | 367 => 154903676593972730912721225331454073903
  | 368 =>  77451838296986365456360612665727036952
  | 369 =>  38725919148493182728180306332863518476
  | 370 =>  96814797871232956820450765832158796190
  | 371 =>  48407398935616478410225382916079398095
  | 372 => 121018497339041196025563457290198495237
  | 373 =>  60509248669520598012781728645099247619
  | 374 => 151273121673801495031954321612748119046
  | 375 =>  75636560836900747515977160806374059523
  | 376 =>  37818280418450373757988580403187029762
  | 377 =>  94545701046125934394971451007967574404
  | 378 =>  47272850523062967197485725503983787202
  | 379 => 118182126307657417993714313759959468005
  | 380 =>  59091063153828708996857156879979734003
  | 381 => 147727657884571772492142892199949335006
  | 382 =>  73863828942285886246071446099974667503
  | 383 =>  36931914471142943123035723049987333752
  | 384 =>  92329786177857357807589307624968334379
  | 385 =>  46164893088928678903794653812484167190
  | 386 => 115412232722321697259486634531210417974
  | 387 =>  57706116361160848629743317265605208987
  | 388 => 144265290902902121574358293164013022467
  | 389 =>  72132645451451060787179146582006511234
  | 390 =>  36066322725725530393589573291003255617
  | 391 =>  90165806814313825983973933227508139042
  | 392 =>  45082903407156912991986966613754069521
  | 393 => 112707258517892282479967416534385173802
  | 394 =>  56353629258946141239983708267192586901
  | 395 => 140884073147365353099959270667981467253
  | 396 =>  70442036573682676549979635333990733627
  | 397 =>  35221018286841338274989817666995366814
  | 398 =>  88052545717103345687474544167488417033
  | 399 =>  44026272858551672843737272083744208517
  | 400 => 110065682146379182109343180209360521291
  | 401 =>  55032841073189591054671590104680260646
  | 402 => 137582102682973977636678975261700651614
  | 403 =>  68791051341486988818339487630850325807
  | 404 =>  34395525670743494409169743815425162904
  | 405 =>  85988814176858736022924359538562907259
  | 406 =>  42994407088429368011462179769281453630
  | 407 => 107486017721073420028655449423203634074
  | 408 =>  53743008860536710014327724711601817037
  | 409 => 134357522151341775035819311779004542592
  | 410 =>  67178761075670887517909655889502271296
  | 411 => 167946902689177218794774139723755678240
  | 412 =>  83973451344588609397387069861877839120
  | 413 =>  41986725672294304698693534930938919560
  | 414 => 104966814180735761746733837327347298900
  | 415 =>  52483407090367880873366918663673649450
  | 416 => 131208517725919702183417296659184123625
  | 417 =>  65604258862959851091708648329592061813
  | 418 => 164010647157399627729271620823980154531
  | 419 =>  82005323578699813864635810411990077266
  | 420 =>  41002661789349906932317905205995038633
  | 421 => 102506654473374767330794763014987596582
  | 422 =>  51253327236687383665397381507493798291
  | 423 => 128133318091718459163493453768734495727
  | 424 =>  64066659045859229581746726884367247864
  | 425 => 160166647614648073954366817210918119659
  | 426 =>  80083323807324036977183408605459059830
  | 427 =>  40041661903662018488591704302729529915
  | 428 => 100104154759155046221479260756823824787
  | 429 =>  50052077379577523110739630378411912394
  | 430 => 125130193448943807776849075946029780984
  | 431 =>  62565096724471903888424537973014890492
  | 432 => 156412741811179759721061344932537226230
  | 433 =>  78206370905589879860530672466268613115
  | 434 =>  39103185452794939930265336233134306558
  | 435 =>  97757963631987349825663340582835766394
  | 436 =>  48878981815993674912831670291417883197
  | 437 => 122197454539984187282079175728544707992
  | 438 =>  61098727269992093641039587864272353996
  | 439 => 152746818174980234102598969660680884990
  | 440 =>  76373409087490117051299484830340442495
  | 441 =>  38186704543745058525649742415170221248
  | 442 =>  95466761359362646314124356037925553119
  | 443 =>  47733380679681323157062178018962776560
  | 444 => 119333451699203307892655445047406941399
  | 445 =>  59666725849601653946327722523703470700
  | 446 => 149166814624004134865819306309258676748
  | 447 =>  74583407312002067432909653154629338374
  | 448 =>  37291703656001033716454826577314669187
  | 449 =>  93229259140002584291137066443286672968
  | 450 =>  46614629570001292145568533221643336484
  | 451 => 116536573925003230363921333054108341210
  | 452 =>  58268286962501615181960666527054170605
  | 453 => 145670717406254037954901666317635426512
  | 454 =>  72835358703127018977450833158817713256
  | 455 =>  36417679351563509488725416579408856628
  | 456 =>  91044198378908773721813541448522141570
  | 457 =>  45522099189454386860906770724261070785
  | 458 => 113805247973635967152266926810652676962
  | 459 =>  56902623986817983576133463405326338481
  | 460 => 142256559967044958940333658513315846203
  | 461 =>  71128279983522479470166829256657923102
  | 462 =>  35564139991761239735083414628328961551
  | 463 =>  88910349979403099337708536570822403877
  | 464 =>  44455174989701549668854268285411201939
  | 465 => 111137937474253874172135670713528004846
  | 466 =>  55568968737126937086067835356764002423
  | 467 => 138922421842817342715169588391910006058
  | 468 =>  69461210921408671357584794195955003029
  | 469 =>  34730605460704335678792397097977501515
  | 470 =>  86826513651760839196980992744943753786
  | 471 =>  43413256825880419598490496372471876893
  | 472 => 108533142064701048996226240931179692233
  | 473 =>  54266571032350524498113120465589846117
  | 474 => 135666427580876311245282801163974615291
  | 475 =>  67833213790438155622641400581987307646
  | 476 => 169583034476095389056603501454968269113
  | 477 =>  84791517238047694528301750727484134557
  | 478 =>  42395758619023847264150875363742067279
  | 479 => 105989396547559618160377188409355168196
  | 480 =>  52994698273779809080188594204677584098
  | 481 => 132486745684449522700471485511693960245
  | 482 =>  66243372842224761350235742755846980123
  | 483 => 165608432105561903375589356889617450306
  | 484 =>  82804216052780951687794678444808725153
  | 485 =>  41402108026390475843897339222404362577
  | 486 => 103505270065976189609743348056010906441
  | 487 =>  51752635032988094804871674028005453221
  | 488 => 129381587582470237012179185070013633051
  | 489 =>  64690793791235118506089592535006816526
  | 490 => 161726984478087796265223981337517041314
  | 491 =>  80863492239043898132611990668758520657
  | 492 =>  40431746119521949066305995334379260329
  | 493 => 101079365298804872665764988335948150822
  | 494 =>  50539682649402436332882494167974075411
  | 495 => 126349206623506090832206235419935188527
  | 496 =>  63174603311753045416103117709967594264
  | 497 => 157936508279382613540257794274918985658
  | 498 =>  78968254139691306770128897137459492829
  | 499 =>  39484127069845653385064448568729746415
  | 500 =>  98710317674614133462661121421824366037
  | 501 =>  49355158837307066731330560710912183019
  | 502 => 123387897093267666828326401777280457546
  | 503 =>  61693948546633833414163200888640228773
  | 504 => 154234871366584583535408002221600571932
  | 505 =>  77117435683292291767704001110800285966
  | 506 =>  38558717841646145883852000555400142983
  | 507 =>  96396794604115364709630001388500357458
  | 508 =>  48198397302057682354815000694250178729
  | 509 => 120495993255144205887037501735625446822
  | 510 =>  60247996627572102943518750867812723411
  | 511 => 150619991568930257358796877169531808527
  | 512 =>  75309995784465128679398438584765904264
  | 513 =>  37654997892232564339699219292382952132
  | 514 =>  94137494730581410849248048230957380330
  | 515 =>  47068747365290705424624024115478690165
  | 516 => 117671868413226763561560060288696725412
  | 517 =>  58835934206613381780780030144348362706
  | 518 => 147089835516533454451950075360870906765
  | 519 =>  73544917758266727225975037680435453383
  | 520 =>  36772458879133363612987518840217726692
  | 521 =>  91931147197833409032468797100544316728
  | 522 =>  45965573598916704516234398550272158364
  | 523 => 114913933997291761290585996375680395910
  | 524 =>  57456966998645880645292998187840197955
  | 525 => 143642417496614701613232495469600494888
  | 526 =>  71821208748307350806616247734800247444
  | 527 =>  35910604374153675403308123867400123722
  | 528 =>  89776510935384188508270309668500309305
  | 529 =>  44888255467692094254135154834250154653
  | 530 => 112220638669230235635337887085625386631
  | 531 =>  56110319334615117817668943542812693316
  | 532 => 140275798336537794544172358857031733289
  | 533 =>  70137899168268897272086179428515866645
  | 534 =>  35068949584134448636043089714257933323
  | 535 =>  87672373960336121590107724285644833306
  | 536 =>  43836186980168060795053862142822416653
  | 537 => 109590467450420151987634655357056041632
  | 538 =>  54795233725210075993817327678528020816
  | 539 => 136988084313025189984543319196320052040
  | 540 =>  68494042156512594992271659598160026020
  | 541 =>  34247021078256297496135829799080013010
  | 542 =>  85617552695640743740339574497700032525
  | 543 =>  42808776347820371870169787248850016263
  | 544 => 107021940869550929675424468122125040656
  | 545 =>  53510970434775464837712234061062520328
  | 546 => 133777426086938662094280585152656300820
  | 547 =>  66888713043469331047140292576328150410
  | 548 => 167221782608673327617850731440820376025
  | 549 =>  83610891304336663808925365720410188013
  | 550 =>  41805445652168331904462682860205094007
  | 551 => 104513614130420829761156707150512735016
  | 552 =>  52256807065210414880578353575256367508
  | 553 => 130642017663026037201445883938140918770
  | 554 =>  65321008831513018600722941969070459385
  | 555 => 163302522078782546501807354922676148462
  | 556 =>  81651261039391273250903677461338074231
  | 557 =>  40825630519695636625451838730669037116
  | 558 => 102064076299239091563629596826672592789
  | 559 =>  51032038149619545781814798413336296395
  | 560 => 127580095374048864454536996033340740986
  | 561 =>  63790047687024432227268498016670370493
  | 562 => 159475119217561080568171245041675926233
  | 563 =>  79737559608780540284085622520837963117
  | 564 =>  39868779804390270142042811260418981559
  | 565 =>  99671949510975675355107028151047453896
  | 566 =>  49835974755487837677553514075523726948
  | 567 => 124589936888719594193883785188809317369
  | 568 =>  62294968444359797096941892594404658685
  | 569 => 155737421110899492742354731486011646711
  | 570 =>  77868710555449746371177365743005823356
  | 571 =>  38934355277724873185588682871502911678
  | 572 =>  97335888194312182963971707178757279195
  | 573 =>  48667944097156091481985853589378639598
  | 574 => 121669860242890228704964633973446598993
  | 575 =>  60834930121445114352482316986723299497
  | 576 => 152087325303612785881205792466808248742
  | 577 =>  76043662651806392940602896233404124371
  | 578 =>  38021831325903196470301448116702062186
  | 579 =>  95054578314757991175753620291755155464
  | 580 =>  47527289157378995587876810145877577732
  | 581 => 118818222893447488969692025364693944330
  | 582 =>  59409111446723744484846012682346972165
  | 583 => 148522778616809361212115031705867430412
  | 584 =>  74261389308404680606057515852933715206
  | 585 =>  37130694654202340303028757926466857603
  | 586 =>  92826736635505850757571894816167144008
  | 587 =>  46413368317752925378785947408083572004
  | 588 => 116033420794382313446964868520208930009
  | 589 =>  58016710397191156723482434260104465005
  | 590 => 145041775992977891808706085650261162512
  | 591 =>  72520887996488945904353042825130581256
  | 592 =>  36260443998244472952176521412565290628
  | 593 =>  90651109995611182380441303531413226570
  | 594 =>  45325554997805591190220651765706613285
  | 595 => 113313887494513977975551629414266533212
  | 596 =>  56656943747256988987775814707133266606
  | 597 => 141642359368142472469439536767833166515
  | 598 =>  70821179684071236234719768383916583258
  | 599 =>  35410589842035618117359884191958291629
  | 600 =>  88526474605089045293399710479895729072
  | 601 =>  44263237302544522646699855239947864536
  | 602 => 110658093256361306616749638099869661340
  | 603 =>  55329046628180653308374819049934830670
  | 604 => 138322616570451633270937047624837076675
  | 605 =>  69161308285225816635468523812418538338
  | 606 =>  34580654142612908317734261906209269169
  | 607 =>  86451635356532270794335654765523172922
  | 608 =>  43225817678266135397167827382761586461
  | 609 => 108064544195665338492919568456903966152
  | 610 =>  54032272097832669246459784228451983076
  | 611 => 135080680244581673116149460571129957690
  | 612 =>  67540340122290836558074730285564978845
  | 613 => 168850850305727091395186825713912447113
  | 614 =>  84425425152863545697593412856956223557
  | 615 =>  42212712576431772848796706428478111779
  | 616 => 105531781441079432121991766071195279446
  | 617 =>  52765890720539716060995883035597639723
  | 618 => 131914726801349290152489707588994099307
  | 619 =>  65957363400674645076244853794497049654
  | 620 => 164893408501686612690612134486242624134
  | 621 =>  82446704250843306345306067243121312067
  | 622 =>  41223352125421653172653033621560656034
  | 623 => 103058380313554132931632584053901640084
  | 624 =>  51529190156777066465816292026950820042
  | 625 => 128822975391942666164540730067377050105
  | 626 =>  64411487695971333082270365033688525053
  | 627 => 161028719239928332705675912584221312631
  | 628 =>  80514359619964166352837956292110656316
  | 629 =>  40257179809982083176418978146055328158
  | 630 => 100642949524955207941047445365138320394
  | 631 =>  50321474762477603970523722682569160197
  | 632 => 125803686906194009926309306706422900493
  | 633 =>  62901843453097004963154653353211450247
  | 634 => 157254608632742512407886633383028625616
  | 635 =>  78627304316371256203943316691514312808
  | 636 =>  39313652158185628101971658345757156404
  | 637 =>  98284130395464070254929145864392891010
  | 638 =>  49142065197732035127464572932196445505
  | 639 => 122855162994330087818661432330491113762
  | 640 =>  61427581497165043909330716165245556881
  | 641 => 153568953742912609773326790413113892203
  | 642 =>  76784476871456304886663395206556946102
  | 643 =>  38392238435728152443331697603278473051
  | 644 =>  95980596089320381108329244008196182627
  | 645 =>  47990298044660190554164622004098091314
  | 646 => 119975745111650476385411555010245228284
  | 647 =>  59987872555825238192705777505122614142
  | 648 => 149969681389563095481764443762806535354
  | 649 =>  74984840694781547740882221881403267677
  | 650 =>  37492420347390773870441110940701633839
  | 651 =>  93731050868476934676102777351754084597
  | 652 =>  46865525434238467338051388675877042299
  | 653 => 117163813585596168345128471689692605746
  | 654 =>  58581906792798084172564235844846302873
  | 655 => 146454766981995210431410589612115757182
  | 656 =>  73227383490997605215705294806057878591
  | 657 =>  36613691745498802607852647403028939296
  | 658 =>  91534229363747006519631618507572348239
  | 659 =>  45767114681873503259815809253786174120
  | 660 => 114417786704683758149539523134465435299
  | 661 =>  57208893352341879074769761567232717650
  | 662 => 143022233380854697686924403918081794123
  | 663 =>  71511116690427348843462201959040897062
  | 664 =>  35755558345213674421731100979520448531
  | 665 =>  89388895863034186054327752448801121327
  | 666 =>  44694447931517093027163876224400560664
  | 667 => 111736119828792732567909690561001401659
  | 668 =>  55868059914396366283954845280500700830
  | 669 => 139670149785990915709887113201251752073
  | 670 =>  69835074892995457854943556600625876037
  | 671 =>  34917537446497728927471778300312938019
  | 672 =>  87293843616244322318679445750782345046
  | 673 =>  43646921808122161159339722875391172523
  | 674 => 109117304520305402898349307188477931307
  | 675 =>  54558652260152701449174653594238965654
  | 676 => 136396630650381753622936633985597414134
  | 677 =>  68198315325190876811468316992798707067
  | 678 =>  34099157662595438405734158496399353534
  | 679 =>  85247894156488596014335396240998383834
  | 680 =>  42623947078244298007167698120499191917
  | 681 => 106559867695610745017919245301247979792
  | 682 =>  53279933847805372508959622650623989896
  | 683 => 133199834619513431272399056626559974740
  | 684 =>  66599917309756715636199528313279987370
  | 685 => 166499793274391789090498820783199968425
  | 686 =>  83249896637195894545249410391599984213
  | 687 =>  41624948318597947272624705195799992107
  | 688 => 104062370796494868181561762989499980266
  | 689 =>  52031185398247434090780881494749990133
  | 690 => 130077963495618585226952203736874975333
  | 691 =>  65038981747809292613476101868437487667
  | 692 => 162597454369523231533690254671093719166
  | 693 =>  81298727184761615766845127335546859583
  | 694 =>  40649363592380807883422563667773429792
  | 695 => 101623408980952019708556409169433574479
  | 696 =>  50811704490476009854278204584716787240
  | 697 => 127029261226190024635695511461791968098
  | 698 =>  63514630613095012317847755730895984049
  | 699 => 158786576532737530794619389327239960123
  | 700 =>  79393288266368765397309694663619980062
  | 701 =>  39696644133184382698654847331809990031
  | 702 =>  99241610332960956746637118329524975077
  | 703 =>  49620805166480478373318559164762487539
  | 704 => 124052012916201195933296397911906218846
  | 705 =>  62026006458100597966648198955953109423
  | 706 => 155065016145251494916620497389882773557
  | 707 =>  77532508072625747458310248694941386779
  | 708 =>  38766254036312873729155124347470693390
  | 709 =>  96915635090782184322887810868676733473
  | 710 =>  48457817545391092161443905434338366737
  | 711 => 121144543863477730403609763585845916842
  | 712 =>  60572271931738865201804881792922958421
  | 713 => 151430679829347163004512204482307396052
  | 714 =>  75715339914673581502256102241153698026
  | 715 =>  37857669957336790751128051120576849013
  | 716 =>  94644174893341976877820127801442122533
  | _ =>  0
  end.

Lemma Zceil_div x y :
 y <> 0 -> Zceil (IZR x / IZR y) =
 if x mod y =? 0 then x / y else x / y + 1.
Proof.
move=> yD.
case: Z.eqb_spec => H; last first.
  rewrite Zceil_floor_neq ?Zfloor_div //.
  contradict H.
  have : (IZR (x / y) * IZR y = IZR x)%R.
    by rewrite H; field; apply: IZR_neq 0 _.
  rewrite -mult_IZR =>/eq_IZR<-.
  by  rewrite Z_mod_mult.
rewrite {1}(Z_div_exact_full_2 _ _ yD H).
rewrite mult_IZR.
replace (IZR y * IZR (x / y) / IZR y)%R with (IZR (x / y)).
  by rewrite Zceil_IZR.
by field; apply: IZR_neq 0 _.
Qed.

Lemma f_tbl_correct1 : 0 <= h <= 716 -> f_tbl h = Zceil (2^127 * f h).
Proof.
move=>  [H1 H2].
rewrite /f prop2_1; try lia.
have ->: ZnearestA (2 ^ 19 * log5 2) =  225799.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite -mult_IZR (bpow_opp _ 19)  (bpow_opp _ h).
rewrite -[bpow _ 19]/(IZR 524288).
rewrite Zfloor_div; try lia.
rewrite -IZR_Zpower; last by apply: Z_div_pos; lia.
rewrite -IZR_Zpower; last by lia.
replace (2 ^ 127)%R with 
  (IZR (170141183460469231731687303715884105728)); last first.
  by rewrite [IZR _]/=; lra.
rewrite -Rmult_assoc -mult_IZR.
rewrite Zceil_div; last by apply: Z.pow_nonzero=> /=; lia.
apply: sym_equal.
move: H2.
Time repeat (
lia ||
(rewrite [X in (_ <= X) -> _]Zsucc_pred;
rewrite Z.le_succ_r [Z.succ _]/= [Z.pred _]/=;
    case=>  [|  ->]; 
[idtac |
match goal with |- _ = ?X => 
  vm_cast_no_check (refl_equal X)
end])).
Time Qed.

Lemma f_tbl_correct2 : -787 <= h <= -1 -> f_tbl h = Zceil (2^127 * f h).
Proof.
move=> [H1 H2].
rewrite /f prop2_1; try lia.
have ->: ZnearestA (2 ^ 19 * log5 2) =  225799.
  apply: Znearest_imp.
  by rewrite /log5 /logn ![radix_val _]/=; interval.
rewrite -mult_IZR (bpow_opp _ 19).
rewrite -[bpow _ 19]/(IZR 524288).
rewrite Zfloor_div; try lia.
rewrite -[_ / _]Z.opp_involutive bpow_opp.
rewrite -IZR_Zpower; last first.
  rewrite -[0]/(-(0/524288)) Z.opp_le_mono !Z.opp_involutive.
  by apply: Z_div_le; lia.
rewrite -IZR_Zpower; last by lia.
replace (2 ^ 127)%R with 
  (IZR (170141183460469231731687303715884105728)); last first.
  by rewrite [IZR _]/=; lra.
rewrite Rmult_comm Rmult_assoc -mult_IZR Rmult_comm.
rewrite Zceil_div; last first.
  apply: Z.pow_nonzero => //.
  rewrite -[0]/(-(0/524288)) Z.opp_le_mono !Z.opp_involutive.
  by apply: Z_div_le; lia.
move: H2.
Time repeat (
lia ||
(rewrite [X in (_ <= X) -> _]Zsucc_pred;
rewrite Z.le_succ_r [Z.succ _]/= [Z.pred _]/=;
    case=>  [|  ->]; 
[idtac |
match goal with |- _ = ?X => 
  vm_cast_no_check (refl_equal X)
end])).
Time Qed.

Lemma f_tbl_correct : f_tbl h = Zceil (2^127 * f h).
Proof.
have [H1| H1] : -787 <= h <= -1 \/ 0 <= h <= 716. 
- by have := h_bound'; lia.
- by apply: f_tbl_correct2.
by apply: f_tbl_correct1.
Qed.


Definition direct_method_alg :=
  let v := n * f_tbl h - m * 2 ^ 127 in
  if v <? - 2 ^ 57 then Gt
  else if v >? 2 ^ 57 then Lt
  else Eq.

Lemma Rabs_Zceil x : (Rabs (x - Zceil x) <= 1)%R.
Proof.
have := Zceil_lb x; have := Zceil_ub x; split_Rabs; lra.
Qed.

Theorem direct_method_alg_correct :
 ((direct_method_alg = Lt <-> F2R x2 < F2R x10) /\
  (direct_method_alg = Eq <-> F2R x2 = F2R x10) /\
  (direct_method_alg = Gt <-> F2R x2 > F2R x10))%R.
Proof.
rewrite /direct_method_alg.
have [|||] := direct_method_correct ((n * f_tbl h) / 2 ^ 127) (bpow radix2 (-124)).
- apply: Rgt_lt.
  by rewrite  /= /Z.pow_pos /=; lra.
- by rewrite /eta /Rpower /= /Z.pow_pos /=; interval.
- have ->: (f h * IZR n - IZR n * f_tbl h / 2 ^ 127 =
          (2 ^ 127 * f h - f_tbl h) / 2 ^ 127 * IZR n)%R.
    by field.
  rewrite Rabs_mult.
  rewrite [Rabs (IZR _)]Rabs_right.
    apply: Rmult_le_compat_r.
    apply: IZR_le; have := n_bound; lia.
  rewrite Rcomplements.Rabs_div; [| lra].
  rewrite [Rabs (_ ^ _)]Rabs_right; [| lra].
  rewrite f_tbl_correct.
  rewrite /Rdiv.
  apply: (Rle_trans _ (1 * / 2 ^ 127)).
    apply: Rmult_le_compat_r; [interval| ].
    exact: Rabs_Zceil.
  have ->: -124 = Z.opp 124 by [].
  rewrite bpow_opp.
  rewrite /= /Z.pow_pos /=; lra.
-  apply: Rle_ge; apply: IZR_le; have := n_bound; lia.
move=> H1 [H2 H3].
case: ifP=> Hc.
+ have Hok: (F2R x2 > F2R x10)%R.
    apply: Rgt_lt.
    apply: H2.
    apply Rcomplements.Rlt_div_l; [lra| ].
    have H': (n * f_tbl h - IZR m * 2 ^ 127 < - bpow radix2 (-124) * 2 ^ 54 * 2 ^ 127)%R.
      have ->: (- bpow radix2 (-124) * 2 ^ 54 * 2 ^ 127 = - 2 ^ 57)%R 
           by rewrite /= /Z.pow_pos /=; lra.
      rewrite -mult_IZR.
      have ->: (- 2 ^ 57 = IZR (- 2 ^ 57))%R by rewrite /=; lra.
      have ->: (2 ^ 127 = IZR (2 ^ 127))%R by rewrite pow_IZR /=; lra.
      rewrite -mult_IZR -minus_IZR.
      apply: IZR_lt.
      by apply/Z.ltb_lt.
    lra.
  repeat split=> //.
  + move=> Habs; exfalso.
    by apply: (Rlt_asym (F2R x2) (F2R x10)).
  + move=> Habs; exfalso.
    by apply: (Rgt_not_eq (F2R x2) (F2R x10)).
case: ifP=> Hc'.
+ have Hok: (F2R x2 < F2R x10)%R.
    apply: H1.
    apply: Rlt_gt.
    apply Rcomplements.Rlt_div_r; [lra| ].
    have H': (n * f_tbl h - IZR m * 2 ^ 127 > bpow radix2 (-124) * 2 ^ 54 * 2 ^ 127)%R.
      have ->: (bpow radix2 (-124) * 2 ^ 54 * 2 ^ 127 = 2 ^ 57)%R by rewrite /= /Z.pow_pos /=; lra.
      rewrite -mult_IZR.
      have ->: (2 ^ 57 = IZR (2 ^ 57))%R by rewrite pow_IZR /=; lra.
      have ->: (2 ^ 127 = IZR (2 ^ 127))%R by rewrite pow_IZR /=; lra.
      rewrite -mult_IZR -minus_IZR.
      apply: IZR_lt.
      by apply/Z.gtb_lt.
    lra.
  repeat split=> //.
  + move=> Habs; exfalso.
    by apply: (Rlt_not_eq (F2R x2) (F2R x10)).
  + move=> Habs; exfalso.
    by apply: (Rgt_asym (F2R x2) (F2R x10)).
+ have Hok: F2R x10 = F2R x2.
    apply: H3.
    apply: (Rmult_le_reg_r (2 ^ 127)); [lra| ].
    have ->: (bpow radix2 (-124) * 2 ^ 54 * 2 ^ 127 = 2 ^ 57)%R 
       by rewrite /= /Z.pow_pos /=; lra.
    have Hr: (2 ^ 127 = Rabs (2 ^ 127))%R.
      rewrite Rabs_right //; lra.
    rewrite {2}Hr.
    rewrite -Rabs_mult.
    have ->: ((IZR m - n * f_tbl h / 2 ^ 127) * 2 ^ 127 = IZR m * 2 ^ 127 - n * f_tbl h)%R by field.
    have ->: (2 ^ 127 = IZR (2 ^ 127))%R by rewrite pow_IZR /=; lra.
    rewrite -!mult_IZR -minus_IZR.
    rewrite -abs_IZR.
    have ->: (2 ^ 57 = IZR (2 ^ 57))%R by rewrite pow_IZR /=; lra.
    apply: IZR_le.
    move: Hc=> /Z.ltb_ge Hc.
    rewrite Z.gtb_ltb in Hc'.
    move: Hc'=> /Z.ltb_ge Hc'.
    lia.
  repeat split=> //; by rewrite Hok=> /Rlt_irrefl Habs.
Qed.

(* B. Bipartite table *)

Definition q : Z := `[IZR g / 16 + 1].

Definition r : Z := 16 * q - g.

Lemma g_eq: g = 16 * q - r.
Proof. by rewrite /r; ring. Qed.

(* WARNING: -20 -> -21 *)
Lemma q_bound : -21 <= q <= 20.
Proof.
split.
  apply: Zfloor_lub.
  have H: (IZR (-339) <= IZR g)%R.
    by apply: IZR_le; have := g_bound'; lia.
  rewrite /= in H; lra.
apply: Zfloor_leq.
rewrite plus_IZR /=.
have H: (IZR g <= IZR 308)%R  by apply: IZR_le; have := g_bound'; lia.
rewrite /= in H; lra.
Qed.

Lemma r_bound : 1 <= r <= 16.
Proof.
split.
  apply/(Zlt_le_succ 0)/Z.lt_0_sub/lt_IZR.
  rewrite mult_IZR /=.
  apply: Rmult_lt_r; [lra| ].
  rewrite /q Zfloor_plus1 plus_IZR /=.
  exact: Zfloor_ub.
have H: 16%Z = 16 + g - g by ring.
rewrite [X in (_ <= X)]H.
apply: Zplus_le_compat_r.
apply: le_IZR.
rewrite mult_IZR plus_IZR /=.
apply: Rmult_le_l; [lra| ].
have ->: ((16 + IZR g) / 16 = IZR g / 16 + 1)%R by field.
exact: Zfloor_lb.
Qed.

Lemma f_eq_qr : (f h = bpow radix5 (16 * q) * bpow radix5 (- r) * bpow radix2 (- h))%R.
Proof.
rewrite /f -bpow_plus.
congr (_ _ _ * _)%R.
by rewrite -g_eq_phih /r; ring.
Qed.

Definition theta1 := (bpow radix5 (16 * q) * bpow radix2 (- psi (16 * q) + 127))%R.

Definition theta2 := (bpow radix5 r * bpow radix2 (- psi r + 63))%R.

Ltac btac n :=
  let X := fresh "X" in
  rewrite 1?[Z.pred _]/= [X in (_ <= X) -> _]Zsucc_pred Z.le_succ_r [Z.succ _]/=;
  move => [| ->];
  [| rewrite (ZfloorE n) ?[IZR _]/= /log2 /logn ![radix_val _]/=; interval].

Ltac bntac v :=
  let v1 := eval compute in (v - 37) in
  let v2 := eval compute in (v - 38) in
  first [lia |(btac v1; bntac v1) |(btac v2; bntac v2)].

Lemma h_1_log_2_128 : 
  (16 * (IZR q) * log2 5 - IZR `[16 * IZR q * log2 5] <  1 + log2 (1 - 1/(2 ^ 128)))%R.
Proof.
have [H] := q_bound.
bntac 780%Z.
Qed.

Lemma theta1_bound : (2 ^ 127 <= theta1  < 2^128 - 1)%R.
Proof.
split.
  rewrite /theta1 bpow_plus -Rmult_assoc -Rpower_pow; [| lra].
  rewrite [bpow _ 127]bpow_exp -/(Rpower _ _) INR_IZR_INZ.
  rewrite -[X in (X <= _)%R]Rmult_1_l.
  apply: Rmult_le_compat_r; first by apply/Rlt_le/exp_pos.
  rewrite !bpow_exp /psi /log2 /logn -exp_plus -[X in (X <= _)%R]exp_0.
  apply: exp_le.
  rewrite opp_IZR Ropp_mult_distr_l_reverse.
  apply: Rle_0_minus.
  rewrite Rmult_comm.
  apply: Rmult_le_l; first by apply: ln_gt_0=> /=; lra.
  rewrite /Rdiv Rmult_assoc.
  by apply: Zfloor_lb.
rewrite /theta1 !bpow_exp -exp_plus.
rewrite /psi.
have ->: (2 ^ 128 - 1)%R = exp (ln (2 ^ 128 - 1)) by rewrite exp_ln; lra.
apply: exp_increasing.
rewrite plus_IZR ![ln (IZR _)]/=.
have ->: ln 5 = (log2 5 * ln 2)%R.
  rewrite /log2 /logn /=; field.
  by apply/Rgt_not_eq/ln_gt_0; lra.
rewrite -Rmult_assoc.
set x := (IZR (16 * q) * log2 5)%R.
rewrite -Rmult_plus_distr_r Rmult_comm.
apply: Rmult_lt_l; first by apply: ln_gt_0; lra.
rewrite opp_IZR [IZR 127]/=.
suff H : (x < IZR `[x] + ln (2 ^ 128 - 1) / ln 2 - 127)%R by lra.
have ->: (127 = ln (2 ^ 127) / ln 2)%R.
  rewrite -Rpower_pow /Rpower; [| lra].
  rewrite -[ln 2]/(ln (IZR radix2)) INR_IZR_INZ -bpow_exp.
  by rewrite -[(ln _ / ln _)%R]/(log2 _) /log2 logn_inv.
rewrite /Rminus Rplus_assoc.
have ->: (ln (2 ^ 128 + -1) / ln 2 + - (ln (2 ^ 127) / ln 2) = 
          (ln (2 ^ 128 - 1) - ln (2 ^ 127)) / ln 2)%R.
  by rewrite Rdiv_plus_distr Ropp_div.
have ->: (ln (2 ^ 128 - 1) - ln (2 ^ 127) = ln (2 - 1 / (2 ^ 127)))%R.
  rewrite -Rcomplements.ln_div; [| lra| lra].
  congr (_ _).
  rewrite Rdiv_minus_distr.
  have ->: (2 ^ 128 = 2 * 2 ^ 127)%R by lra.
  rewrite /Rdiv Rmult_assoc Rinv_r ?Rmult_1_r //.
  lra.
have ->: (2 - 1 / 2 ^ 127 = 2 * (1 - 1 / 2 ^ 128))%R.
  rewrite Rmult_minus_distr_l Rmult_1_r /Rdiv -Rmult_assoc Rmult_1_r.
  have ->: (2 ^ 128)%R = (2 * (2 ^ 127))%R by lra.
  rewrite Rinv_mult_distr; [| lra| lra].
  by rewrite -Rmult_assoc Rinv_r; [| lra].
rewrite ln_mult; [| lra| ]; last first.
  by apply/Rlt_Rminus/Rdiv_lt_l; lra.
rewrite Rdiv_plus_distr /Rdiv Rinv_r; last first.
  by apply/Rgt_not_eq/ln_gt_0; lra.
rewrite  -[(ln _ * / ln _)%R]/(logn radix2 _) -/log2.
have H := h_1_log_2_128.
rewrite /x !mult_IZR [IZR 16]/=.
lra.
Qed.

Lemma theta2_int : exists t, IZR t = theta2.
Proof.
exists ((5 ^ r) * (2 ^ - psi r + 63)).
rewrite /theta2 mult_IZR.
have ->: IZR (5 ^ r) = bpow radix5 r.
  rewrite -IZR_Zpower //.
  have := r_bound; lia.
congr (_ * _)%R.
rewrite -IZR_Zpower //.
rewrite /psi.
suff Hlia : `[IZR r * log2 5] <= 63 by lia.
apply: Zfloor_leq.
rewrite Rmult_comm.
apply: Rmult_lt_l; first by apply: logn_pos; lra.
apply: (Rle_lt_trans _ (IZR 16)).
  by apply: IZR_le; have := r_bound; lia.
rewrite /log2 /logn /=; interval.
Qed.

Lemma h_r_log2_5 : (IZR (`[IZR r * log2 5]) < IZR r * log2 5)%R.
Proof.
set x := (_ * _)%R.
suff: IZR `[x] <> x.
  have [_ H1] := archimed x; rewrite /Zfloor minus_IZR /=; lra.
have [Rmin _] := r_bound.
set y := `[x] => Hy.
have Ypos : 0 <= y.
   apply/le_IZR=> /=.
   rewrite Hy /x.
   apply: Rmult_le_pos.
     by apply: (@IZR_le 0); lia.
   apply: Rmult_le_pos.
     have : (ln 1 < ln 5)%R by apply: ln_increasing; lra.
     by rewrite ln_1; lra.
   apply/Rlt_le/Rinv_0_lt_compat.
   have : (ln 1 < ln 2)%R by apply: ln_increasing; lra.
   by rewrite ln_1.
pose nr := Z.to_nat r.
pose ny := Z.to_nat y.
have F : 2 ^ (Z.of_nat ny) = 5 ^ Z.of_nat nr.
   apply: eq_IZR.
   apply: ln_inv .
   - by rewrite -pow_IZR /=; apply: pow_lt; lra.
   - by rewrite -pow_IZR /=; apply: pow_lt; lra.
   rewrite -!pow_IZR !Rcomplements.ln_pow /=; try lra.
   rewrite !INR_IZR_INZ !Z2Nat.id; try lia.
   rewrite Hy /x /log2 /logn /=.
   replace (2 + 1 + 1 + 1)%R with 5%R by ring.
   by field; interval.
have := gcd_pow_2_5 _ _ (Zle_0_nat ny) (Zle_0_nat nr).
rewrite F Z.gcd_diag Z.abs_eq => [F1| ]; last first.
   by apply: Z.pow_nonneg; lia.
absurd (5 <= 1); try lia.
apply: Znumtheory.Zdivide_le; try lia.
rewrite -F1.
apply: Zpow_facts.Zpower_divide.
by rewrite Z2Nat.id; lia.
Qed.

Lemma theta2_bound : (2 ^ 63 < theta2 < 2 ^ 64)%R.
Proof.  
rewrite /theta2 !bpow_exp -exp_plus; split; 
    (rewrite -Rpower_pow; [| lra]);
    rewrite INR_IZR_INZ /=;
    apply: exp_increasing;
    rewrite plus_IZR opp_IZR /=.
  suff Hlra: (IZR (psi r) * ln 2 < IZR r * ln 5)%R by lra.
  rewrite Rmult_comm.
  apply: Rmult_lt_l; first by apply: ln_gt_0; lra.
  rewrite /Rdiv Rmult_assoc.
  exact: h_r_log2_5.
suff Hlra: (IZR r * ln 5 < (1 + IZR (psi r)) * ln 2)%R by lra.
rewrite [((1 + IZR _) * _)%R]Rmult_comm.
apply: Rmult_lt_r; first by apply: ln_gt_0; lra.
rewrite /Rdiv Rmult_assoc.
rewrite [(1 + IZR _)%R]Rplus_comm.
by apply: Zfloor_ub.
Qed.

Definition sigma : Z := psi r - psi (16 * q) + h.

Lemma f_eq_theta : f h = ((theta1 / theta2) * bpow radix2 (- 64 - sigma))%R.
Proof.
rewrite /theta1 /theta2 /sigma f_eq_qr !Rmult_assoc.
apply: Rmult_eq_compat_l.
rewrite /Rdiv Rinv_mult_distr.
- rewrite -!bpow_opp -!Rmult_assoc  [(_ * bpow radix5 _)%R]Rmult_comm !Rmult_assoc.
  apply: Rmult_eq_compat_l.
  rewrite -!bpow_plus.
  by congr (_ _ _); ring.
- apply: Rgt_not_eq.
  by apply: bpow_gt_0.
apply: Rgt_not_eq.
apply: bpow_gt_0.
Qed.

Lemma sigma_bound : 0 <= sigma <= 3.
Proof.
split.
  apply/(Zlt_le_succ (-1))/lt_IZR=> /=.
  apply: (Rle_lt_trans _ (IZR h - IZR g * log2 5 - 1)).
    apply: (Rle_trans _ (IZR h - (IZR h * log5 2) * log2 5 - 1)).
      rewrite /log5 /log2 /logn /=.
      field_simplify; try apply: Rle_refl; split; 
         apply: Rgt_not_eq; apply: ln_gt_0; lra.
    suff H : (IZR g * log2 5 <= IZR h * log5 2 * log2 5)%R by lra.
    apply: Rmult_le_compat_r; first by apply/Rlt_le/logn_pos; lra.
    rewrite g_eq_phih /phi.
    exact: Zfloor_lb.
  rewrite /sigma /psi plus_IZR.
  have ->: (IZR h - IZR g * log2 5 - 1 = - IZR g * log2 5 - 1 + IZR h)%R by ring.
  apply: Rplus_lt_compat_r.
  rewrite g_eq minus_IZR Ropp_minus_distr Rmult_minus_distr_r minus_IZR.
  have ->: (IZR `[IZR r * log2 5] - IZR `[IZR (16 * q) * log2 5] =
         IZR `[IZR r * log2 5] + 1 - IZR `[IZR (16 * q) * log2 5] - 1)%R by ring.
  apply/Rplus_lt_compat_r/Rplus_lt_le_compat.
    by apply: Zfloor_ub.
  by apply/Ropp_le_contravar/Zfloor_lb.
apply/(Z.lt_le_pred _ 4)/lt_IZR=> /=.
apply: (Rlt_le_trans _ (IZR h - IZR g * log2 5 + 1)).
rewrite /sigma /psi plus_IZR minus_IZR.
have ->: (IZR h - IZR g * log2 5 + 1 = - IZR g * log2 5 + 1 + IZR h)%R by ring.
apply: Rplus_lt_compat_r.
rewrite g_eq minus_IZR Ropp_minus_distr Rmult_minus_distr_r.
have ->: (IZR r * log2 5 - IZR (16 * q) * log2 5 + 1 =
          IZR r * log2 5 + (- IZR (16 * q) * log2 5 + 1))%R by ring.
  apply: Rplus_le_lt_compat; first by apply: Zfloor_lb.
  have ->: (- IZR `[IZR (16 * q) * log2 5] = 
            - IZR `[IZR (16 * q) * log2 5] - 1 + 1)%R by ring.
  apply: Rplus_lt_compat_r.
  rewrite -Ropp_minus_distr Ropp_mult_distr_l_reverse.
  apply: Ropp_lt_contravar.
  ring_simplify.
  by apply: Zfloor_ub.
apply: (Rle_trans _ (IZR h - (IZR h * log5 2 - 1) * log2 5 + 1)).
  suff : ((IZR h * log5 2 - 1) * log2 5 <= IZR g * log2 5)%R by lra.
  apply: Rmult_le_compat_r.
    apply: Rlt_le; apply: logn_pos; lra.
  suff : (IZR h * log5 2 < IZR g + 1)%R by lra.
  rewrite g_eq_phih /phi.
  by apply: Zfloor_ub.
rewrite /log5 /log2 /logn /=; ring_simplify.
rewrite Rmult_assoc.
have ->: (ln 2 / ln 5 * (ln 5 / ln 2) = 1)%R.
  field; split; apply: Rgt_not_eq; apply: ln_gt_0; lra.
ring_simplify; interval.
Qed.

Definition delta : R :=
    theta1 * (IZR n) * bpow radix2 (-64 + 8)
  - theta2 * (IZR m) * bpow radix2 (8 + sigma).

(* Lemma 2 *)
Lemma delta_big : (F2R x2 <> F2R x10)%R -> (2^124 * eta <= Rabs delta)%R.
Proof.
move=> H.
have ->: (delta = bpow radix2 (8 + sigma) * theta2 *
                 IZR n * (f h - IZR m / IZR n))%R.
  rewrite /delta Rmult_minus_distr_l.
  have ->: (bpow radix2 (8 + sigma) * theta2 * IZR n * (IZR m / IZR n) = 
            theta2 * IZR m * bpow radix2 (8 + sigma))%R.
    field_simplify=> //.
    apply: Rgt_not_eq.
    apply/(IZR_lt 0)/(Z.lt_le_trans _ (2 ^ 53)) => //.
    by have := n_bound; lia.
  congr (_ - _)%R.
  rewrite f_eq_theta.
  have ->: (bpow radix2 (8 + sigma) * theta2 * IZR n * 
               (theta1 / theta2 * bpow radix2 (-64 - sigma)) = 
            theta1 * IZR n * bpow radix2 (8 + sigma) * bpow radix2 (-64 - sigma))%R.
    field.
    apply: Rgt_not_eq.
    by have := theta2_bound; lra.
  rewrite !Rmult_assoc.
  do 3 apply: Rmult_eq_compat_l.
  rewrite -bpow_plus.
  by congr (_ _ _); ring.
rewrite 3!Rabs_mult Rabs_right; last by apply/Rle_ge/bpow_ge_0.
rewrite Rabs_right; last by have := theta2_bound; lra.
rewrite Rabs_right; last first.
  apply: Rle_ge.
  apply: (IZR_le 0).
  by have := n_bound; lia.
apply: (Rle_trans _ (2 ^ 8 * 2 ^ 63 * 2 ^ 53 * Rabs (f h - IZR m / IZR n))).
  have ->: (2 ^ 124 * eta = 2 ^ 8 * 2 ^ 63 * 2 ^ 53 * eta)%R by rewrite -!pow_add.
  apply: Rmult_le_compat_l; first by rewrite -!pow_add; apply: pow_le; lra.
  rewrite /f bpow_opp -/d_h.
  apply/Rlt_le/Rgt_lt.
  exact: d_h_eta.
apply: Rmult_le_compat_r; first by apply: Rabs_pos.
apply: Rmult_le_compat.
- rewrite -pow_add; apply: pow_le; lra.
- apply: pow_le; lra.
- apply: Rmult_le_compat; try (apply: pow_le; lra).
    rewrite -Rpower_pow; [| lra].
    rewrite bpow_exp /Rpower.
    apply/exp_le/Rmult_le_compat_r; first by apply: ln_ge_0; lra.
    rewrite INR_IZR_INZ.
    apply: IZR_le.
    by have:= sigma_bound; lia.
  have := theta2_bound; lra.
have ->: (2 ^ 53)%R = IZR (2 ^ 53) by rewrite pow_IZR /=; lra.
apply: IZR_le.
have := n_bound; lia.
Qed.

Definition delta' : R :=
    IZR `[IZR (Zceil theta1) * IZR n * bpow radix2 (8 - 64)] - 
    theta2 * (IZR m) * bpow radix2 (8 + sigma).

Lemma delta'_int : exists n, IZR n = delta'.
Proof.
move: theta2_int=> [t Ht].
exists (`[IZR (Zceil theta1) * IZR n * bpow radix2 (8 - 64)] - 
        t * m * (2 ^ (8 + sigma))).
rewrite /delta' minus_IZR !mult_IZR.
suff ->: IZR (2 ^ (8 + sigma)) = bpow radix2 (8 + sigma) by rewrite Ht.
rewrite -IZR_Zpower //.
have := sigma_bound; lia.
Qed.

Lemma f_n_eq_delta : 
  (f h * IZR n = (theta1 * IZR n * bpow radix2 (-64 + 8)) / 
                 (theta2 * bpow radix2 (8 + sigma)))%R.
Proof.
rewrite f_eq_theta.
have ->: (bpow radix2 (-64 - sigma) = 
          bpow radix2 (-64 + 8) / bpow radix2 (8 + sigma))%R.
  rewrite /Rdiv -bpow_opp -bpow_plus.
  by congr (_ _); ring.
field; split; apply: Rgt_not_eq.
apply: bpow_gt_0.
have := theta2_bound; lra.
Qed.

Lemma delta_ineq :
    ((delta < 0 -> F2R x10 < F2R x2)
  /\ (delta > 0 -> F2R x10 > F2R x2)
  /\ (delta = 0 -> F2R x10 = F2R x2))%R.
Proof.
have Hpos: (0 < theta2 * bpow radix2 (8 + sigma))%R.
  apply: Rmult_lt_0_compat.
  have := theta2_bound; lra.
  by apply: bpow_gt_0.
have [Hf1 [Hf2 Hf3]] := f_prop.
(repeat split) => H; rewrite /delta in H.
- apply: Hf2.
  rewrite f_n_eq_delta.
  apply: Rdiv_lt_l=> //; lra.
- apply: Hf1.
  rewrite f_n_eq_delta.
  apply: Rdiv_lt_r=> //; lra.
symmetry; apply: Hf3.
rewrite f_n_eq_delta.
apply: Rdiv_eq_l=> //; lra.
Qed.

Lemma delta_eq_rev : F2R x10 = F2R x2 -> delta = 0.
Proof.
move=> H.
have [d_lt_0| H'] := Rlt_le_dec delta 0.
  have Habs : (F2R x10 < F2R x2)%R by apply delta_ineq.
  lra.
have [d_gt_0| //] :=  Rle_lt_or_eq_dec 0 delta H'.
have Habs : (F2R x10 > F2R x2)%R by apply delta_ineq.
lra.
Qed.

(* Theorem 3 *)
Lemma delta'_ineq' :
    ((delta' < 0 -> F2R x10 < F2R x2)
  /\ (delta' > 0 -> F2R x10 > F2R x2)
  /\ (delta' = 0 -> F2R x10 = F2R x2))%R.
Proof.
pose dtbl := (IZR (Zceil theta1) - theta1)%R.
pose drnd := (IZR `[IZR (Zceil theta1) * IZR n * bpow radix2 (8 - 64)] -
              IZR (Zceil theta1) * IZR n * bpow radix2 (8 - 64))%R.
pose d := (drnd + IZR n * bpow radix2 (8 - 64) * dtbl)%R.
have Hd: delta' = (delta + d)%R.
  rewrite /delta' /delta /d /drnd /dtbl.
  have ->: -64 + 8 = (8 - 64) by [].
  by ring.
have dtbl_min: (0 <= dtbl)%R.
  rewrite /dtbl.
  have Hlra := (Zceil_ub theta1); lra.
have dtbl_max: (dtbl < 1)%R.
  rewrite /dtbl.
  have Hlra := (Zceil_lb theta1); lra.
have drnd_min: (-1 < drnd)%R.
  rewrite /drnd.
  have Hlra := (Zfloor_ub (IZR (Zceil theta1) * IZR n *
                           bpow radix2 (8 - 64))); lra.
have drnd_max: (drnd <= 0)%R.
  rewrite /drnd.
  have Hlra := (Zfloor_lb (IZR (Zceil theta1) * IZR n *
                           bpow radix2 (8 - 64))); lra.
have d_min: (-1 < d)%R.
  rewrite /d.
  suff : (0 <= IZR n * bpow radix2 (8 - 64) * dtbl)%R by lra.
  apply: Rmult_le_pos=> //.
  apply: Rmult_le_pos.
  apply: (IZR_le 0); first by have := n_bound; lia.
  by  apply: bpow_ge_0.
have d_max: (d < 1/4)%R.
  rewrite /d.
  suff : (IZR n * bpow radix2 (8 - 64) * dtbl < 1 / 4)%R by lra.
  have ->: (1 / 4 = 1 / 4 * 1)%R by ring.
  apply: (Rlt_le_trans _ (IZR n * bpow radix2 (8 - 64) * 1)).
    apply: Rmult_lt_compat_l=> //.
    apply: Rmult_lt_0_compat; last by apply: bpow_gt_0.
    by apply: (IZR_lt 0); have := n_bound; lia.
  apply: Rmult_le_compat_r; [lra| ].
  rewrite Rmult_comm.
  apply: Rmult_le_l; first by apply: bpow_gt_0.
  rewrite /Rdiv Rmult_1_l.
  have ->: (/ 4 = bpow radix2 (- 2))%R by [].
  rewrite -bpow_opp -bpow_plus.
  have ->: bpow radix2 (-2 + - (8 - 64)) = IZR (2 ^ 54) by [].
  apply: IZR_le.
  have := n_bound; lia.
have d_zero i : d = IZR i -> d = 0.
  move=> H.
  rewrite H.
  suff ->: i = 0 by [].
  have H1: i < 1 by apply: lt_IZR=> /=; lra.
  have H2: -1 < i by apply: lt_IZR=> /=; lra.
  lia.
have delta_delta'_0: delta = 0 -> delta' = 0.
  move=> delta_eq_0.
  move: delta'_int=> [i Hi].
  have H := (d_zero i); lra.
have Rabs_delta': delta <> 0 -> (2 ^ 10 < Rabs delta')%R.
  move=> delta_neq_0.
  apply: (Rlt_le_trans _ (2 ^ 124 * eta - Rabs d)).
  - rewrite /eta.
    have Hlra1: (2 ^ 10 + 1 < 2 ^ 124 * Rpower 2 (-113 - 7 / 10))%R.
      rewrite -!Rpower_pow; [| lra| lra].
      by rewrite -Rpower_plus /= /Rpower; interval.
    suff : (Rabs d < 1)%R by lra.
    by  split_Rabs; lra.
  apply: (Rle_trans _ (Rabs delta - Rabs d)).
    apply/Rplus_le_compat_r/delta_big=> x2_eq_x10.
    by apply/delta_neq_0/delta_eq_rev.
  rewrite -[Rabs d]Rabs_Ropp.
  have ->: (delta' = delta - -d)%R by rewrite Hd; ring.
  by apply: Rabs_triang_inv.
(repeat split)=> H.
- have [Hf _] := delta_ineq.
  apply: Hf.
  have ->: (delta = delta' - d)%R by rewrite Hd; ring.
  apply: (Rlt_trans _ (delta' - (- 1))).
    by apply/Rplus_lt_compat_l/Ropp_lt_contravar.
  have ->: (delta' - -1 = delta' + 1)%R by ring.
  apply: (Rlt_le_trans _ (delta' + Rabs delta')); first by lra.
  by rewrite Rabs_left; lra.
(* Second case *)
- have [_ [Hf _]] := delta_ineq.
  apply: Hf.
  have ->: (delta = delta' - d)%R by rewrite Hd; ring.
  apply: (Rlt_trans _ (delta' - 1)).
    apply: (Rle_lt_trans _ (delta' - Rabs delta')); last by lra.
    rewrite Rabs_right; lra.
  apply: Rplus_lt_compat_l.
  apply: Ropp_lt_contravar.
  lra.
(* Last case *)
have [_ [_ Hf]] := delta_ineq.
apply: Hf.
have [//| delta_neq_0] := Req_dec delta 0.
have := Rabs_delta' delta_neq_0.
rewrite H Rabs_R0.
lra.
Qed.

Lemma delta'_ineq :
    ((delta' < 0 <-> F2R x10 < F2R x2)
  /\ (delta' > 0 <-> F2R x10 > F2R x2)
  /\ (delta' = 0 <-> F2R x10 = F2R x2))%R.
Proof.
(repeat split) => [| H| | H| | H]; try by apply delta'_ineq'.
- have [//| Hle] := Rlt_le_dec delta' 0.
  by have [d_gt_0| d_eq_0] := Rle_lt_or_eq_dec _ _ Hle;
     have [_ []] := delta'_ineq'; lra.
- have [//| Hge] := Rlt_le_dec 0 delta'.
  by have [d_gt_0| d_eq_0] := Rle_lt_or_eq_dec _ _ Hge;
     have [H1 []] := delta'_ineq'; lra.
have [Hlt| Hge] := Rlt_le_dec 0 delta'.
  by have [_ []] := delta'_ineq'; lra.
have [d_gt_0| //] := Rle_lt_or_eq_dec _ _ Hge.
have [H1 []] := delta'_ineq'; lra.
Qed.

Definition theta1_tbl q : Z :=
  match q with
  | -21 => 302910693998692996157485768413290076966
  | -20 => 336298426882534191759128470626028036789
  | -19 => 186683128335104582129005107785662008085
  | -18 => 207259907386686073192955235040171322419
  | -17 => 230104721262376436189351064420995165904
  | -16 => 255467559620444135892015707268715336457
  | -15 => 283625966735416996535885333662014114405
  | -14 => 314888078651228693933689466069052580905
  | -13 => 174797997549285651882438866782683340398
  | -12 => 194064761537588616893622436057812819408
  | -11 => 215455166527421378856590945602770070141
  | -10 => 239203286653190548679094257880939433815
  |  -9 => 265568996408383549344794103276234313665
  |  -8 => 294840814439182918143871451639708507103
  |  -7 => 327339060789614187001318969682759915222
  |  -6 => 181709681073901722637330951972001133589
  |  -5 => 201738271725539733566868685312735302683
  |  -4 => 223974474217780421055744228056844427813
  |  -3 => 248661618204893321077691124073410420051
  |  -2 => 276069853871622551497390234491081018099
  |  -1 => 306499108173177771671669405430061836724
  |   0 => 170141183460469231731687303715884105728
  |   1 => 188894659314785808547840000000000000000
  |   2 => 209715200000000000000000000000000000000
  |   3 => 232830643653869628906250000000000000000
  |   4 => 258493941422821148397315216271863391740
  |   5 => 286985925493722536125179818657774823687
  |   6 => 318618382226490455405776079553542361119
  |   7 => 176868732008334225927912486150152183217
  |   8 => 196363738611909062123830878199451025720
  |   9 => 218007543808417316859394750271862213031
  |  10 => 242036994678082392051126914580396990474
  |  11 => 268715044302683550071638623558009085183
  |  12 => 298333629248008269731638612618517353496
  |  13 => 331216864211123806751178713779234900611
  |  14 => 183862294395666818064937594201088633456
  |  15 => 204128152598478183127259193653345185578
  |  16 => 226627774989027955951103258828533066424
  |  17 => 251607373812388019852618613412845800985
  |  18 => 279340299571981831419774226402503504146
  |  19 => 310130032290502989833240994779765547114
  |  20 => 172156751238329846960951049916624692801
  |   _ => 0
  end.

Lemma Lpsi_eq: ZnearestA (2 ^ 12 * log2 5) = 9511.
rewrite /ZnearestA.
have ->: `[2 ^ 12 * log2 5] = 9510%Z.
  apply: Zfloor_eq; rewrite /log2 /logn ![radix_val _] /=.
  split; interval.
have H: ((/ 2) < 2 ^ 12 * log2 5 - IZR 9510)%R.
  rewrite /log2 /logn ![IZR _]/=; interval.
  rewrite (Rcompare_Gt _ _ H).
  have ->: ((2 ^ 12) = 4096)%R by ring.
  apply: Zceil_imp=> /=.
  rewrite /log2 /logn /=; split; interval.
Qed.

Ltac vtac1 :=
  let X := fresh "X" in
  rewrite 1?[Z.pred _]/= [X in (_ <= X) -> _]Zsucc_pred Z.le_succ_r; case=> [| ->];
  [| rewrite !bpow_powerRZ /=; symmetry; apply: Zceil_imp=> /=; lra].

Lemma theta1_tbl_ok : theta1_tbl q = Zceil theta1.
Proof.
rewrite /theta1.
have ->: psi (16 * q) = (9511 * q / 2 ^ 8).
  rewrite -Zfloor_div //.
  have ->: IZR (2 ^ 8) = bpow radix2 8 by [].
  rewrite /Rdiv -bpow_opp [Z.opp _]/=.
  rewrite [9511 * q]Zmult_comm.
  have [_ [_ ->]] := prop2 g _ h_bound.
  by rewrite mult_IZR Lpsi_eq.
  have := q_bound; lia.
have [q_min] := q_bound.
rewrite /theta1_tbl.
do 42 vtac1.
lia.
Qed.

Definition theta2_tbl r : Z :=
  match r with
  | 1 => 11529215046068469760
  | 2 => 14411518807585587200
  | 3 => 18014398509481984000
  | 4 => 11258999068426240000
  | 5 => 14073748835532800000
  | 6 => 17592186044416000000
  | 7 => 10995116277760000000
  | 8 => 13743895347200000000
  | 9 => 17179869184000000000
  |10 => 10737418240000000000
  |11 => 13421772800000000000
  |12 => 16777216000000000000
  |13 => 10485760000000000000
  |14 => 13107200000000000000
  |15 => 16384000000000000000
  |16 => 10240000000000000000
  |_ => 0
  end.

Ltac vtac2 :=
  let X := fresh "X" in
  rewrite 1?[Z.pred _]/= [X in (_ <= X) -> _]Zsucc_pred Z.le_succ_r; case=> [| ->];
  [| rewrite !bpow_powerRZ /=; ring].

Lemma theta2_tbl_ok : IZR (theta2_tbl r) = theta2.
Proof.
rewrite /theta2.
have ->: psi r = 9511 * r / 2 ^ 12.
  rewrite -Zfloor_div //.
  have ->: IZR (2 ^ 12) = bpow radix2 12 by [].
  rewrite /Rdiv -bpow_opp [Z.opp _]/=.
  rewrite [9511 * r]Zmult_comm.
  have [_ [H _]] :=  prop2 r _ h_bound.
  rewrite H ?mult_IZR ?Lpsi_eq //.
  have := r_bound; lia.
rewrite /theta2_tbl.
have [r_min] := r_bound.
do 16 vtac2; lia.
Qed.

Definition ineq_alg : comparison :=
  let q := g / 16 + 1 in
  let r := 16 * q - g in
  let psir := (9511 * r) / 2 ^ 12 in
  let psiq := (9511 * q) / 2 ^ 8 in
  let s := psir - psiq + h in
  let a := ((theta1_tbl q) * (n * 2 ^ 8)) / (2 ^ 64) in
  let b := (theta2_tbl r) * (m * 2 ^ (8 + s)) in
  let D := a - b in 0 ?= D.

Theorem ineq_alg_correct :
 ((ineq_alg = Lt <-> F2R x2 < F2R x10) /\
  (ineq_alg = Eq <-> F2R x2 = F2R x10) /\
  (ineq_alg = Gt <-> F2R x2 > F2R x10))%R.
Proof.
rewrite /ineq_alg.
have ->: g / 16 + 1 = `[IZR g / 16 + 1].
  rewrite Zfloor_plus1.
  rewrite -Zfloor_div //.
rewrite -/q -/r.
rewrite theta1_tbl_ok.
have ->: 9511 * q / 2 ^ 8 = psi (16 * q).
  rewrite -Zfloor_div //.
  have [_ [_ ->]] := prop2 q _ h_bound.
   rewrite Lpsi_eq mult_IZR.
  have ->: -8 = Z.opp 8 by [].
  by rewrite bpow_opp /= Rmult_comm.
  have := q_bound; lia.
have ->: 9511 * r / 2 ^ 12 = psi r.
  rewrite -Zfloor_div //.
  have [_ [H _]] := prop2 r _ h_bound.
  rewrite H.
    rewrite Lpsi_eq mult_IZR.
    have ->: -12 = Z.opp 12 by [].
    by rewrite bpow_opp /= Rmult_comm.
  have := r_bound; lia.
rewrite -/sigma.
rewrite -Rcompare_IZR [IZR 0]/= minus_IZR.
have ->: (IZR (theta2_tbl r * (m * 2 ^ (8 + sigma))) = 
          theta2 * IZR m * bpow radix2 (8 + sigma))%R.
  rewrite mult_IZR theta2_tbl_ok mult_IZR Rmult_assoc.
  congr (_ * (_ * _))%R.
  have ->: 2%Z = radix2 by [].
  rewrite IZR_Zpower //.
  have := sigma_bound; lia.
have ->: IZR (Zceil theta1 * (n * 2 ^ 8) / 2 ^ 64) =
         IZR `[IZR (Zceil theta1) * IZR n * bpow radix2 (8 - 64)].
  rewrite -mult_IZR.
  have ->: 8 - 64 = 8 + (Z.opp 64) by [].
  rewrite bpow_plus bpow_opp.
  rewrite -Rmult_assoc.
  rewrite -Zfloor_div //.
  congr (IZR `[_]).
  rewrite mult_IZR mult_IZR.
  by rewrite -Rmult_assoc -mult_IZR.
rewrite -/delta'.
move: delta'_ineq=> [[H1 H1'] [[H2 H2'] [H3 H3']]].
repeat split.
+ by move=> /Rcompare_Lt_inv.
+ move=> /H2' H.
  by apply: Rcompare_Lt.
+ move=> /Rcompare_Eq_inv H.
  by rewrite H3.
+ move=> H.
  apply: Rcompare_Eq.
  by rewrite H3'.
+ move=> /Rcompare_Gt_inv.
+ move=> /H1 H.
  exact: Rgt_lt.
+ move=> /Rgt_lt /H1' H.
  by apply: Rcompare_Gt.
Qed.

(* V. Equality cases *)

Lemma eq_case : (F2R x2 = F2R x10) ->
(     (0 <= g <= 22 /\ 0 <= h <= 53 /\ (5 ^ g |m) /\ 
      (2 ^ h |n) /\ m * 2 ^ h = n * 5 ^ g)
  \/ 
      -22 <= g <= 0  /\ -51 <= h < 0 /\ (2 ^ (-h) |m) /\ (5 ^ (- g) | M10) /\
      (m * 5 ^ (-g) = n * 2 ^ (-h)))
.
Proof.
move=> x2_eq_x10.
have [g_sign| g_sign] := Z_lt_le_dec g 0; [right| left].
(*** First case (g < 0) ***)
  have h_neg: h < 0.
    rewrite g_eq_phih /phi in g_sign.
     (**)
    apply: lt_IZR.
    have ->: IZR h = ((IZR h * log5 2) / log5 2)%R.
      field.
      apply: Rgt_not_eq; apply: logn_pos; lra.
    apply: Rdiv_lt_l.
    by apply: logn_pos; lra.
    rewrite /= Rmult_0_l.
    apply: (Rlt_le_trans _ (IZR `[IZR h * log5 2] + 1)).
      rewrite plus_IZR /=; first by apply/Zfloor_ub.
      rewrite -(plus_IZR _ 1); apply: (IZR_le _ 0); lia.
  have eq_m_n: (m * 5 ^ -g) = n * 2 ^ (-h).
    apply: eq_IZR.
    rewrite 2!mult_IZR.
    rewrite (IZR_Zpower radix2); last by lia.
    rewrite (IZR_Zpower radix5); last by lia.
    rewrite x2_eq x10_eq in x2_eq_x10.
    rewrite !bpow_opp.
    apply: Rdiv_eq_Rdiv; try by apply: bpow_gt_0.
    apply: (Rmult_eq_reg_r _ _ _ x2_eq_x10).
    by apply/Rgt_not_eq/bpow_gt_0.
  have div_m: (2 ^ (- h) | m).
    apply: (Z.gauss _ (5 ^ (- g))).
    rewrite Zmult_comm eq_m_n.
    apply: Z.divide_factor_r.
    apply: gcd_pow_2_5; lia.
  have div_n: (5 ^ (- g) | n).
    apply: (Z.gauss _ (2 ^ (- h))).
    rewrite Zmult_comm -eq_m_n.
    apply: Z.divide_factor_r.
    rewrite Z.gcd_comm.
    apply: gcd_pow_2_5; lia.
  have div_M10: (5 ^ (- g) | M10).
    apply: (Z.gauss _ (2 ^ v)).
      by rewrite Zmult_comm; exact: div_n.
    rewrite Z.gcd_comm.
    apply: gcd_pow_2_5; have := v_bound; lia.
  have Hg': -22 <= g.
    apply: Zopp_le_cancel=> /=.
    apply: Zlt_succ_le=> /=.
    apply: lt_IZR=> /=.
    apply: (Rle_lt_trans _ (log5 (IZR M10))).
      have ->: IZR (- g) = log5 (IZR (5 ^ (- g))).
        have ->: 5%Z = radix5 by [].
        rewrite IZR_Zpower=> //; last by lia.
        by rewrite /log5 logn_inv.
      apply: logn_le; split.
        rewrite (IZR_Zpower radix5) => //; [| lia].
        by apply: bpow_gt_0.
      apply/IZR_le/Z.divide_pos_le=> //.
     have := M10_bound; lia.
    apply: (Rlt_le_trans _ (log5 (IZR (10 ^ 16)))).
      apply: logn_lt.
        by apply: (IZR_lt 0); have := M10_bound; lia.
      by apply: IZR_lt; have := M10_bound; lia.
    have ->: IZR (10 ^ 16) = Rpower 10 16.
      have ->: IZR (10 ^ 16) = (10 ^ 16)%R.
        by rewrite pow_IZR [Z.of_nat 16]/=.
      rewrite -Rpower_pow ?INR_IZR_INZ -?IZR_IZR //.
      lra.
    rewrite /log5 /logn /Rpower ln_exp /=; interval.
  suff : -51 <= h by repeat split=> //; lia.
  rewrite g_eq_phih /phi in Hg'.
  apply/(Zlt_le_succ (-52))/lt_IZR=> /=.
  apply: (Rmult_lt_reg_r (log5 2)); first by apply: logn_pos; lra.
  apply: (Rlt_le_trans _ (-22)); first by rewrite /log5 /logn /=; interval.
  apply: (Rle_trans _ (IZR `[IZR h * log5 2])).
    by apply: (IZR_le (-22)).
  by apply: Zfloor_lb.
(*** Second case (g >= 0) ***)
have h_pos: 0 <= h.
  rewrite g_eq_phih /phi in g_sign.
  apply: le_IZR.
  have ->: IZR h = ((IZR h * log5 2) / log5 2)%R.
    field.
    apply: Rgt_not_eq; apply: logn_pos; lra.
  apply: Rdiv_le_r; first by apply: logn_pos; lra.
  apply: (Rle_trans _ (IZR (`[IZR h * log5 2]))); last by apply: Zfloor_lb.
  rewrite Rmult_0_l.
  by apply: (IZR_le 0); lia.
have eq_m_n: m * 2 ^ h = n * 5 ^ g.
  apply: eq_IZR.
  rewrite 2!mult_IZR.
  rewrite (IZR_Zpower radix2) // (IZR_Zpower radix5) //.
  rewrite x2_eq x10_eq in x2_eq_x10.
  apply: (Rmult_eq_reg_r _ _ _ x2_eq_x10).
  apply: Rgt_not_eq.
  by apply: bpow_gt_0.
have div_m: (5 ^ g | m).
  apply: (Z.gauss _ (2 ^ h)).
  rewrite Zmult_comm eq_m_n.
  apply: Z.divide_factor_r.
  rewrite Z.gcd_comm.
  by apply: gcd_pow_2_5.
have div_n: (2 ^ h | n).
  apply: (Z.gauss _ (5 ^ g)).
  rewrite Zmult_comm -eq_m_n.
  apply: Z.divide_factor_r.
  by apply: gcd_pow_2_5.
have Hg': g <= 22.
  apply: Zlt_succ_le=> /=.
  apply: lt_IZR=> /=.
  apply: (Rle_lt_trans _ (log5 (Rpower 2 53))).
    apply: (Rle_trans _ (log5 (IZR m))).
      have ->: IZR g = log5 (IZR (5 ^ g)).
        have ->: 5%Z = radix5 by [].
        rewrite IZR_Zpower=> //.
        by rewrite /log5 logn_inv.
      apply: logn_le; split.
        rewrite (IZR_Zpower radix5) //.
        by apply: bpow_gt_0.
      apply: IZR_le.
      apply: Z.divide_pos_le=> //.
      have := m_bound; lia.
    apply: logn_le; split.
      by apply: (IZR_lt 0); have := m_bound; lia.
    have ->: Rpower 2 53 = IZR (2 ^ 53).
      have ->: IZR (2 ^ 53) = (2 ^ 53)%R by rewrite pow_IZR /=; lra.
      rewrite -Rpower_pow ?INR_IZR_INZ -?IZR_IZR //.
      lra.
    by apply: IZR_le; have := m_bound; lia.
  by rewrite /log5 /logn /Rpower ln_exp /=; interval.
suff Hh' : h <= 53 by repeat split.
rewrite g_eq_phih /phi in Hg'.
have [//| Habs] := Z_le_gt_dec h 53.
suff : 23 <= `[IZR h * log5 2] by lia.
apply: Zfloor_lub.
have Hh: (54 <= IZR h)%R by apply: (IZR_le 54); lia.
rewrite /= [(IZR h * _)%R]Rmult_comm.
apply: Rmult_le_r; first by apply: logn_pos; lra.
apply: (Rle_trans _ 54)=> //.
rewrite /log5 /logn /=; interval.
Qed.

Lemma eq_case_equ : (F2R x2 = F2R x10) <->
(
  (0 <= h <= 53 /\ 0 <= g <= 22 /\ (2 ^ h |n) /\ (5 ^ g * (n / 2 ^ h) = m))
\/ 
  -51 <= h < 0 /\ -22 <= g <= 0 /\ (2 ^ (-h) | m) /\ 
    (5 ^ (-g) * (m / (2 ^ (- h)))) = n)
.
Proof.
split => [x2_eq_x10| [[[Hgpos ?] [[Hhpos ?] [Hn Heq]]]| [[? Hgneg] 
                    [[? Hhneg] [Hn Heq]]]]].
- have  [[[Hgpos ?] [[Hhpos ?] [Hm [Hn Heq]]]]| [[? Hgneg]
        [[? Hhneg] [Hm [Hn Heq]]]]] := eq_case x2_eq_x10.
    left; repeat split=> //.
    rewrite -Z.divide_div_mul_exact=> //.
      rewrite Zmult_comm -Heq Z.div_mul //.
      by apply: Z.pow_nonzero.
    by apply: Z.pow_nonzero=> //.
  right; repeat split=> //.
  rewrite -Z.divide_div_mul_exact=> //.
    rewrite Zmult_comm Heq Z.div_mul //.
    by apply: Z.pow_nonzero=> //; lia.
  apply: Z.pow_nonzero=> //; lia.
(* <- *)
- rewrite x2_eq x10_eq.
  apply: Rmult_eq_compat_r.
  rewrite -!IZR_Zpower // -!mult_IZR.
  congr (_ _)=> /=.
  rewrite -Heq -Zmult_assoc [_ / _ * _]Zmult_comm.
  rewrite -Z.divide_div_mul_exact=> //.
    rewrite [_ * n]Zmult_comm Z.div_mul.
      by rewrite Zmult_comm.
    by apply: Z.pow_nonzero=> //.
  by apply: Z.pow_nonzero=> //.
rewrite x2_eq x10_eq.
apply/Rmult_eq_compat_r/Rmult_eq_Rmult; try by  apply: bpow_gt_0.
rewrite /Rdiv -!bpow_opp -!IZR_Zpower //; try lia.
rewrite -!mult_IZR.
congr (_ _)=> /=.
rewrite -Heq -Zmult_assoc [_ / _ * _]Zmult_comm.
rewrite -Z.divide_div_mul_exact=> //.
  rewrite [_ * m]Zmult_comm Z.div_mul.
    by rewrite Zmult_comm.
  by apply: Z.pow_nonzero=> //; lia.
by apply: Z.pow_nonzero=> //; lia.
Qed.

Definition eq_alg : bool :=
  if (0 <=? h) && (h <=? 53) && (0 <=? g) && (g <=? 22) && (n mod (2 ^ h) =? 0) then
    let m' := 5 ^ g * (n / (2 ^ h)) in
    (m' =? m)
  else if (h >=? -51) && (-22 <=? g) && (g <=? 0) && (m mod (2 ^ (-h)) =? 0) then
    let n' := 5 ^ (- g) * (m / (2 ^ (- h))) in
    n' =? n
  else
    false.

Theorem eq_alg_correct :
  eq_alg = true <-> (F2R x2 = F2R x10).
Proof.
split=> H.
+ apply eq_case_equ.
  rewrite /eq_alg in H.
  case H1: ((0 <=? h) && (h <=? 53) && (0 <=? g) && (g <=? 22) && (n mod (2 ^ h) =? 0)) H.
  + move: H1=> /andP [/andP [/andP [/andP [/Z.leb_le H11 /Z.leb_le H12] /Z.leb_le H13] /Z.leb_le H14] /Z.eqb_eq H15] /Z.eqb_eq H16.
    left; repeat split=> //.
    apply Z.mod_divide=> //.
    by apply/Zgt_not_eq/(Zpower_gt_0 radix2).
  + case H2: ((h >=? -51) && (-22 <=? g) && (g <=? 0) && (m mod 2 ^ (- h) =? 0))=> //.
    move: H2=> /andP [/andP [/andP [/Z.geb_le H12 /Z.leb_le H13] /Z.leb_le H14] /Z.eqb_eq H15] /Z.eqb_eq H16.
    have Hh: h < 0.
    + case: (Z_lt_le_dec h 0)=> // Hh; exfalso.
      have Hg': 0 <= g.
        rewrite g_eq_phih /phi.
        apply: (Z.le_trans _ `[IZR 0 * log5 2]).
        + rewrite /= Rmult_0_l.
          by rewrite Zfloor_IZR.
        + apply: Zfloor_le=> /=.
          apply: Rmult_le_compat_r.
          apply: Rlt_le.
          apply: logn_pos; lra.
          apply: IZR_le; lia.
      have Hg: g = 0%Z by lia.
      rewrite Hg /= in H16.
      case: (Z_le_lt_eq_dec 0 h Hh).
      + move=> Hh'.
        rewrite Z.pow_neg_r in H16; last by lia.
        rewrite Zdiv_0_r in H16.
        have := n_bound; lia.
      + move=> Hh'.
        rewrite -Hh' /= Zdiv_1_r in H16.
        have Hm: m = n.
          by case: m H16.
        have := m_bound; have := n_bound; lia.
    right; repeat split=> //.
    + apply Z.mod_divide=> //.
      apply/Zgt_not_eq/(Zpower_gt_0 radix2); lia.
move: eq_case_equ=> [/(_ H) H' _].
rewrite /eq_alg.
case: H'.
+ move=> [[Hh /Z.leb_le ->] [[/Z.leb_le -> /Z.leb_le ->] [H3 ->]]] /=.
  have /Z.leb_le -> := Hh.
  have ->: n mod 2 ^ h =? 0%Z.
    apply/Z.eqb_eq.
    apply Z.mod_divide=> //.
    by apply/Zgt_not_eq/(Zpower_gt_0 radix2).
  by rewrite Z.eqb_refl.
+ move=> [[/Z.geb_le -> Hh] [[/Z.leb_le -> /Z.leb_le ->] [H3 ->]]].
  rewrite /=.
  rewrite Zle_bool_false //=.
  have ->: m mod 2 ^ (- h) =? 0.
    apply/Z.eqb_eq.
    apply Z.mod_divide=> //.
    by apply/Zgt_not_eq/(Zpower_gt_0 radix2); lia.
  by rewrite Z.eqb_refl.
Qed.

End CompBinDec.

Compute easycomp   {|Fnum := 7205759403792794; Fexp := -56 |}
                   {|Fnum :=                   1; Fexp :=   -1 |}.

Compute direct_method_alg   {|Fnum := 7205759403792794; Fexp := -56 |}
                   {|Fnum :=                   1; Fexp :=   -1 |}.

Compute ineq_alg   {|Fnum := 7205759403792794; Fexp := -56 |}
                   {|Fnum :=                   1; Fexp :=   -1 |}.

Compute   eq_alg   {|Fnum := 7205759403792794; Fexp := -56 |}
                   {|Fnum :=                   1; Fexp :=   -1 |}.
