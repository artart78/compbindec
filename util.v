From Flocq Require Import Core.
From mathcomp Require Import all_ssreflect.

Require Import Psatz Reals.
Require Import Znumtheory.

Require Import Interval.Interval_tactic.

(*****************************************************************************)
(*                       Theorem for Z                                       *)
(*****************************************************************************)

Open Scope Z_scope.

Lemma rel_prime_pow_r (a b n : Z) :
   0 <= n -> rel_prime a b -> rel_prime a (b ^ n).
Proof.
move=> nP Rab.
elim /natlike_ind : nP => [|m Pm H].
   by apply/rel_prime_sym/rel_prime_1.
rewrite Z.pow_succ_r; try lia.
by apply: rel_prime_mult.
Qed.

Lemma gcd_pow_2_5 x y : 0 <= x -> 0 <= y -> Z.gcd (2 ^ x) (5 ^ y) = 1%Z.
Proof.
move=> xP yP.
apply Zgcd_1_rel_prime.
apply: rel_prime_pow_r; first by lia.
apply/rel_prime_sym/rel_prime_pow_r; first by lia.
apply: bezout_rel_prime.
exists (-1) 3; lia.
Qed.

Open Scope R_scope.

(*****************************************************************************)
(*                       IZR INR                                             *)
(*****************************************************************************)

Coercion IZR : Z >-> R.

Definition nsINR := nosimpl INR.

Coercion nsINR : nat >-> R.

Canonical Rplus_monoid := Monoid.Law 
           (fun x y z => (sym_equal (Rplus_assoc x y z)))  Rplus_0_l  Rplus_0_r.
Canonical Rplus_comoid := Monoid.ComLaw Rplus_comm.

Fact INR0 : 0%N = 0 :> R.
Proof. by rewrite /nsINR. Qed.

Fact S_INR m : (m.+1)%N = m + 1 :> R.
Proof. by exact: S_INR. Qed.

Fact plus_INR m n : (m + n)%N = m + n :> R.
Proof. by exact: plus_INR. Qed.

Fact minus_INR m n : (n <= m)%N -> (m - n)%N = m - n :> R.
Proof. by move=> nLm; apply/minus_INR/leP. Qed.

Lemma iterR_cons n v : iter n (Rplus v) 0 = n * v.
Proof.
elim: n=> [/=|n IH]; first by rewrite INR0; lra.
rewrite iterS IH -addn1 !plus_INR S_INR INR0; lra.
Qed.

Lemma big_const_R m n r : \big[Rplus/0]_(m <= i < n) r =  (n - m)%N * r.
Proof. by rewrite big_const_nat iterR_cons. Qed.

(*****************************************************************************)
(*                       Radix 2 5 and 10                                    *)
(*****************************************************************************)

Definition  radix2 : radix. exists  2%Z. auto. Defined.
Definition  radix5 : radix. exists  5%Z. auto. Defined.
Definition radix10 : radix. exists 10%Z. auto. Defined.

(*****************************************************************************)
(*                       Logarithm of a radix                                *)
(*****************************************************************************)

Definition logn (n: radix) (x: R) := (ln x / ln (IZR n))%R.

Definition log5 := logn radix5.
Definition log2 := logn radix2.

Lemma ln_gt_0 x : 1 < x -> 0 < ln x.
Proof. by move=> xP; rewrite -ln_1; apply: ln_increasing; lra. Qed.

Lemma ln_ge_0 x : 1 < x -> 0 <= ln x.
Proof. by move=> /ln_gt_0; lra. Qed.

Lemma radix_Rgt_1 (r: radix): 1 < IZR r.
Proof. by apply: IZR_lt (radix_gt_1 _). Qed.

Lemma logn_lt r x y : 0 < x -> x < y -> logn r x < logn r y.
Proof.
move=> xP xLy.
apply: Rmult_lt_compat_r.
  by apply/Rinv_0_lt_compat/ln_gt_0/radix_Rgt_1.
by apply: ln_increasing.
Qed.

Lemma logn_le r x y : 0 < x <= y -> logn r x <= logn r y.
Proof.
move=> xLy.
have [<-//|xLLy] : x = y \/ x < y by lra.
  by lra.
by apply/Rlt_le/logn_lt; lra.
Qed.

Lemma logn_inv r x : logn r (bpow r x) = IZR x.
Proof.
have H :=  ln_gt_0 _ (radix_Rgt_1 r).
by rewrite bpow_exp /logn ln_exp /Rdiv Rinv_r_simpl_l; lra.
Qed.

Lemma Rpower_logn (n: radix) r: 0 < r -> Rpower n (logn n r) = r.
Proof.
move=> Hr.
rewrite /logn /Rpower.
rewrite -{2}[r]exp_ln //; congr (_ _); field.
apply: Rgt_not_eq; apply: Rlt_gt; apply: ln_gt_0.
exact: radix_Rgt_1.
Qed.

Lemma logn_1 r : logn r 1 = 0%R.
Proof. by rewrite /logn ln_1; lra. Qed.

Lemma logn_pos r x : 1 < x -> 0 < logn r x.
Proof. by move=> xP; rewrite -(logn_1 r); apply: logn_lt; lra. Qed.

(*****************************************************************************)
(*                       Order and Division                                  *)
(*****************************************************************************)

Lemma Rdiv_le_l x y z : 0 < y -> x <= z * y -> x / y <= z.
Proof.
move=> yP xLzy.
rewrite (_ : z = z * y / y); last by field; lra.
apply/Rmult_le_compat_r=> //.
by have := Rinv_0_lt_compat _ yP; lra.
Qed.

Lemma Rdiv_lt_l x y z : 0 < y -> x < z * y -> x / y < z.
Proof.
move=> yP xLzy.
rewrite (_ : z = z * y / y); last by field; lra.
apply/Rmult_lt_compat_r=> //.
by have := Rinv_0_lt_compat _ yP; lra.
Qed.

Lemma Rdiv_eq_l x y z : 0 < y -> x = z * y -> x / y = z.
Proof. by move=> yP ->; field; lra. Qed.

Lemma Rdiv_le_r x y z : 0 < y -> z * y <= x -> z <= x / y.
Proof.
move=> yP zyLx.
rewrite (_ : z = z * y / y); last by field; lra.
apply/Rmult_le_compat_r=> //.
by have := Rinv_0_lt_compat _ yP; lra.
Qed.

Lemma Rdiv_lt_r x y z : 0 < y -> z * y < x -> z < x / y.
Proof.
move=> yP zyLx.
rewrite (_ : z = z * y / y); last by field; lra.
apply/Rmult_lt_compat_r=> //.
by have := Rinv_0_lt_compat _ yP; lra.
Qed.

Lemma Rdiv_eq_Rdiv x y z w : 0 < y -> 0 < w -> z * y = x * w -> z / w = x / y.
Proof. by move=> yP wP H; field[H]; lra. Qed.

Lemma Rdiv_le_Rdiv x y z w : 0 < y -> 0 < w -> z * y <= x * w -> z / w <= x / y.
Proof.
move=> yP wP H.
apply: Rdiv_le_r yP _.
rewrite (_ :  z / w * y = z * y / w); last by field; lra.
by apply: Rdiv_le_l.
Qed.

Lemma Rdiv_lt_Rdiv x y z w : 0 < y -> 0 < w -> z * y < x * w -> z / w < x / y.
Proof.
move=> yP wP H.
apply: Rdiv_lt_r yP _.
rewrite (_ :  z / w * y = z * y / w); last by field; lra.
by apply: Rdiv_lt_l.
Qed.

Lemma Rmult_le_r x y z :  0 < y -> x / y <= z -> x <= y * z.
Proof.
move=> yP xyLz.
rewrite (_ : x = y * (x / y)); last by field; lra.
by apply: Rmult_le_compat_l; lra.
Qed.

Lemma Rmult_lt_r x y z : 0 < y -> x / y < z -> x < y * z.
Proof.
move=> yP xyLz.
rewrite (_ : x = y * (x / y)); last by field; lra.
by apply: Rmult_lt_compat_l; lra.
Qed.

Lemma Rmult_le_l x y z : 0 < y -> z <= x / y -> y * z <= x.
Proof.
move=> yP zLxy.
rewrite (_ : x = y * (x / y)); last by field; lra.
by apply: Rmult_le_compat_l; lra.
Qed.

Lemma Rmult_lt_l x y z : 0 < y -> z < x / y -> y * z < x.
Proof.
move=> yP zLxy.
rewrite (_ : x = y * (x / y)); last by field; lra.
by apply: Rmult_lt_compat_l; lra.
Qed.

Lemma Rmult_eq_Rmult x y z w : 0 < y -> 0 < w -> z / w = x / y -> z * y = x * w.
Proof.
move=> yP wP zwExy.
rewrite (_ : z * y = z / w * y * w); last by field; lra.
by rewrite zwExy; field; lra.
Qed.

(*****************************************************************************)
(*                       Exponential and Power                               *)
(*****************************************************************************)

Lemma exp_le_inv x y: exp x <= exp y -> x <= y.
Proof.
case/Rle_lt_or_eq_dec.
+ by move/exp_lt_inv/Rlt_le.
+ move/exp_inv ->; exact: Rle_refl.
Qed.

Lemma Rpower_le_inv_l x y z:
  1 < x ->
  Rpower x y <= Rpower x z ->
  y <= z.
Proof.
rewrite /Rpower=> Hx H.
apply: (Rmult_le_reg_r (ln x)).
exact: ln_gt_0.
exact: exp_le_inv.
Qed.

Lemma Rpower_lt_inv_l x y z:
  1 < x ->
  Rpower x y < Rpower x z ->
  y < z.
Proof.
rewrite /Rpower=> Hx H.
apply: (Rmult_lt_reg_r (ln x)).
exact: ln_gt_0.
exact: exp_lt_inv.
Qed.

Lemma Rpower_rad (r: radix) (x: Z): (0 <= x)%Z -> Rpower r x = (r ^ x)%Z.
Proof.
move=> Hx.
rewrite -bertrand.powerRZ_Rpower; last by apply: radix_pos.
by rewrite -bpow_powerRZ -IZR_Zpower // Z2R_IZR.
Qed.

(*****************************************************************************)
(*                        Signed Power for R                                 *)
(*****************************************************************************)

Notation "a ^+ b"  := (Zpower_nat a b) : Z_scope.

Lemma Zpower_nat_signE n : ((-1)^+ n = 1 \/ (-1)^+ n = -1)%Z.
Proof. by (elim: n => [|n IH]); [left | rewrite Zpower_nat_succ_r; lia]. Qed.

Lemma Zpower_nat_IZR (a : Z) n : (a ^+ n)%Z = a ^ n :> R.
Proof. by elim: n => //= n IH; rewrite -Zpower_nat_succ_r mult_IZR IH. Qed.

Lemma Rpower_signE n : (-1)^ n = 1 \/ (-1)^ n = -1.
Proof. by (elim: n => [|n IH]); [left | rewrite /=; lra]. Qed.

Lemma Zpower_nat_sign_odd n : ((-1)^+ n = if odd n then -1 else 1)%Z.
Proof.
elim: n => [//| n IH].
rewrite Zpower_nat_succ_r IH [odd _.+1]/=; case: odd => /=; lia.
Qed.

Lemma Rpower_sign_odd n : (-1)^  n = if odd n then -1 else 1.
Proof. by elim: n => //= n ->; case: odd=> /=; lra. Qed.

Lemma bpow_10 x : bpow radix2 x * bpow radix5 x = bpow radix10 x.
Proof.
rewrite !bpow_powerRZ /=.
rewrite -powerRZ_mult_distr //.
lra.
Qed.

(*****************************************************************************)
(*                        Floor and Ceil functions                           *)
(*****************************************************************************)

Notation "`[ x ]" := (Zfloor x) (format "`[ x ]").

Lemma Zfloor_bound x : `[x] <= x < `[x] + 1.
Proof.
rewrite /Zfloor minus_IZR /=.
have [] := archimed x; lra.
Qed.

Lemma Zfloor_lub (z : Z) x : z <= x -> (z <= `[x])%Z.
Proof.
move=> H.
suff: (z < `[x] + 1)%Z by lia.
apply: lt_IZR; rewrite plus_IZR /=.
have [] := Zfloor_bound x; lra.
Qed.

Lemma Zfloor_eq (z : Z) x :  z <= x < z + 1 -> `[x] = z.
Proof.
move=> xB.
have ZxB := Zfloor_bound x.
suff : (`[x] < z + 1 /\ z <= `[x])%Z by lia.
split; last by apply: Zfloor_lub; lra.
apply : lt_IZR; rewrite plus_IZR /=; lra.
Qed.

Lemma ZfloorZ (z : Z) : `[z] = z.
Proof. apply: Zfloor_eq; lra. Qed.

Lemma Zfloor_le x y : x <= y -> (`[x] <= `[y])%Z.
Proof.
move=> H; apply: Zfloor_lub.
have := Zfloor_bound x; lra.
Qed.

Lemma Zfloor_addz (z: Z) x : `[x + z] = (`[x] + z)%Z.
Proof.
have ZB := Zfloor_bound x.
by apply: Zfloor_eq; rewrite plus_IZR; lra.
Qed.

Lemma ZfloorD_cond r1 r2 : 
  if Rle_dec (`[r1] + `[r2] + 1) (r1 + r2)
  then `[r1 + r2] = (`[r1] + `[r2] + 1)%Z
  else  `[r1 + r2] = (`[r1] + `[r2])%Z.  
Proof.
have [Zr1 Zr2] := (Zfloor_bound r1, Zfloor_bound r2).
case: Rle_dec => H /=.
  apply: Zfloor_eq; rewrite plus_IZR plus_IZR /=; lra.
apply: Zfloor_eq; rewrite plus_IZR; lra.
Qed.

Lemma Zfloor_leq x n : x < IZR (n + 1) -> (`[x] <= n)%Z.
Proof.
move=> H.
have [nLx | xLn] := Rle_lt_dec (IZR n) x.
  by apply/Zeq_le/Zfloor_imp.
rewrite -[n]Zfloor_IZR.
by apply Zfloor_le; lra.
Qed.

Lemma Zceil_lb x : IZR (Zceil x) < x + 1.
Proof.
rewrite /Zceil opp_IZR.
by have H := (Zfloor_ub (- x)); lra.
Qed.

Lemma Zfloor_plus1 x : `[x + 1] = (`[x] + 1)%Z.
Proof. by rewrite -Zfloor_addz. Qed.

Lemma Zceil_eqlog5_1 : Zceil (-1126 / (1 + log5 2)) = (-787)%Z.
Proof. by rewrite /log5 /logn /=; apply: Zceil_imp=> /=; split; interval. Qed.

Lemma Zceil_eqlog5_2 : Zceil (292 / log5 2) = 679%Z.
Proof. by rewrite /log5 /logn /=; apply: Zceil_imp=> /=; split; interval. Qed.

Lemma Zceil_eqlog5_3 : Zceil (-339 / log5 2) = (-787)%Z.
Proof. by rewrite /log5 /logn /=; apply: Zceil_imp=> /=; split; interval. Qed.

Lemma Zfloor_eqlog5_1 : `[309 / log5 2] = 717%Z.
Proof. by rewrite /log5 /logn /=; apply: Zfloor_imp=> /=; split; interval. Qed.

Lemma Zfloor_eqlog5_2 : `[1025 / (1 + log5 2)] = 716%Z.
Proof. by rewrite /log5 /logn /=; apply: Zfloor_imp=> /=; split; interval. Qed.

(*****************************************************************************)
(*                        Fractional Part                                    *)
(*****************************************************************************)

Definition frac_part x := x - `[x].

Notation "`{ r }" := (frac_part r) (format "`{ r }").

Lemma frac_bound x : 0 <= `{x} < 1.
Proof. rewrite /frac_part; have := Zfloor_bound x; lra. Qed.

Lemma frac_inv r : (0 <= r < 1) -> `{r} = r.
Proof.
move=> [/Zfloor_le rP rL1]; rewrite (ZfloorZ 0) in rP.
rewrite /frac_part.
suff -> : `[r] = 0%Z by rewrite /=; lra.
suff /(lt_IZR _ 1) F : `[r] < 1 by lia.
have H1 := Zfloor_bound r; lra.
Qed.

Lemma frac_addz (z : Z) x :  `{x + z} = `{x}.
Proof. rewrite /frac_part; rewrite Zfloor_addz plus_IZR; lra. Qed.

Lemma fracB_eq0 r1 r2 : `{r1} = `{r2} -> `{r1 - r2} = 0.
Proof.
rewrite /frac_part => H1.
have->: r1 - r2 = (`[r1] - `[r2])%Z by rewrite minus_IZR; lra.
rewrite ZfloorZ; lra.
Qed.

Lemma fracZ (z: Z) : `{z} = 0.
Proof. rewrite /frac_part ZfloorZ; lra. Qed.

Lemma frac_eq r1 r2 : `{r1 - r2} = 0 -> `{r1} = `{r2}.
Proof. 
rewrite /frac_part=> H1.
have {2}-> : r1 = r2 + (r1 - r2) by lra.
have DE : r1 - r2 = `[r1 - r2] by lra.
rewrite DE Zfloor_addz plus_IZR -DE; lra.
Qed.

Lemma fracD r1 r2 : `{r1 + r2} = `{`{r1} + `{r2}}.
Proof.
rewrite -(frac_addz ((- `[r1] - `[r2])%Z)) minus_IZR opp_IZR.
congr (`{_}); rewrite /frac_part; lra.
Qed.

Lemma fracD_cond r1 r2 :
  if Rle_dec 1 (`{r1} + `{r2})  then `{r1 + r2} = `{r1} + `{r2} - 1
  else  `{r1 + r2} = `{r1} + `{r2}.
Proof.
rewrite /frac_part.
have:= ZfloorD_cond r1 r2.
(do 2 case: Rle_dec) => /= H1 H2 ->; rewrite ?plus_IZR /=; lra.
Qed.

Lemma fracB r1 r2 : `{r1} <= `{r2} -> `{r2 - r1} = `{r2} - `{r1}.
Proof.
move=> r1Lr2.
have := fracD_cond (r2 - r1) r1.
have->: r2 - r1 + r1 = r2 by ring.
case: Rle_dec => U /=; try lra.
case: (frac_bound (r2 - r1)); lra.
Qed.

Lemma fracK r : `{`{r}} = `{r}.
Proof.
have {1}->: `{r} = `{r} + `{0} by rewrite (fracZ 0); lra.
rewrite -fracD; congr `{_}; lra.
Qed.

Lemma fracN r : `{r} <> 0 -> `{-r} = 1 - `{r}.
Proof.
rewrite /frac_part => rD0.
suff->: `[-r] = (- `[r] - 1)%Z.
  rewrite minus_IZR opp_IZR /=; lra.
apply: Zfloor_eq; rewrite minus_IZR !opp_IZR /=.
have := Zfloor_bound r; lra.
Qed.

Lemma fracN_NZ r : `{r} <> 0 -> `{-r} <> 0.
Proof. move=> rNZ; rewrite fracN //; have := frac_bound r; lra. Qed.

Lemma fracNZ_N r : `{-r} <> 0 -> `{r} <> 0.
Proof. rewrite -{2}[r]Ropp_involutive; exact: fracN_NZ. Qed.
  
Lemma frac_inv_gt_1 r : `{r} <> 0 -> 1 < 1 / `{r}.
Proof.
have F1 := frac_bound r => Dr.
rewrite -{1}Rinv_1 /Rdiv Rmult_1_l.
apply: Rinv_lt_contravar; lra.
Qed.

Lemma frac_inv_floor_ge_1 r : `{r} <> 0 -> (1 <= `[1 / `{r}])%Z.
Proof.
move=> Dr.
rewrite -{1}(ZfloorZ 1); apply: Zfloor_le => /=.
by have := frac_inv_gt_1 _ Dr; lra.
Qed.

(*****************************************************************************)
(*                        Modulo 1 for R                                     *)
(*****************************************************************************)

Definition Rmod1 r := Rmin `{r} (1 - `{r}).

Notation "`| r |" := (Rmod1 r) : R_scope.

Lemma Rmod1_pos r : 0 <= `|r| < 1.
Proof.
by rewrite /Rmod1 /Rmin; case: Rle_dec; have := frac_bound r; lra.
Qed.

Lemma Rmod1_def r : 
  `|r| = Rabs (r - (1 + `[r])%Z) \/ `|r| = Rabs (r - `[r]).
Proof.
have := Zfloor_bound r.
rewrite /Rmod1 /Rmin /frac_part plus_IZR /=.
case: Rle_dec; split_Rabs; try lra.
Qed.

Lemma Rmod1_min r (p : Z) : `|r| <= Rabs (r - p).
Proof.
have Br := Zfloor_bound r.
have [/IZR_le|/IZR_le] : (p <= `[r] \/ 1 + `[r] <= p)%Z by lia.
  rewrite /Rmod1 /Rmin /frac_part plus_IZR /=.
  case: Rle_dec; split_Rabs; try lra.
rewrite /Rmod1 /Rmin /frac_part !plus_IZR /=.
case: Rle_dec; split_Rabs; try lra.
Qed.


(*****************************************************************************)
(*                       Computable logarithm                                *)
(*****************************************************************************)

Fixpoint IPR' (p: positive) : R :=
  match p with
  | xH => 1
  | xI p => 1 + 2 * IPR' p
  | xO p => 2 * IPR' p
  end.

Lemma IPR_2_IPR p :  IPR_2 p = 2 * IPR p.
Proof.
by rewrite /IPR; elim: p => //=; lra.
Qed.

Lemma IPR'_correct (p: positive) : IPR p = IPR' p.
Proof.
by elim: p => // p IH; rewrite /IPR /= IPR_2_IPR IH.
Qed.

Lemma IPR_pos r: 0 < IPR r.
Proof.
rewrite IPR'_correct; elim: r=> /= [r Hr|r Hr|]; lra.
Qed.

Fixpoint log2p p : Z := match p with
| xH => 0
| xI p | xO p => (log2p p) + 1
end.

Lemma log2p_pos p: (0 <= log2p p)%Z.
Proof.
by elim: p=> //= p H; lia.
Qed.

Lemma log2p_ineq p: (2 ^ (log2p p) <= Zpos p < 2 ^ ((log2p p) + 1))%Z.
Proof.
elim: p=> //= p IH; have Hp := log2p_pos p; rewrite Z.pow_add_r // Z.pow_add_r; lia.
Qed.

Lemma log2p_correct (p: positive) : (log2p p = `[log2 (IPR p)])%Z.
Proof.
symmetry.
apply: Zfloor_eq.
have [Hp1 Hp2] := log2p_ineq p; have Hp_pos := log2p_pos p.
have [H1 H2]: Rpower radix2 (log2p p) <= Rpower radix2 (log2 (IPR p)) < Rpower radix2 ((log2p p + 1)%Z).
  rewrite Rpower_logn; last by exact: IPR_pos.
  rewrite !Rpower_rad //=; [|lia].
  rewrite -[IPR p]/(IZR (Zpos p)).
  by split; [exact: IZR_le|exact: IZR_lt].
split.
apply: (Rpower_le_inv_l radix2)=> //=; lra.
apply: (Rpower_lt_inv_l radix2)=> //=; [lra|].
by rewrite plus_IZR in H2.
Qed.

Definition log2z (z: Z) : Z :=
  match z with
  | Zpos p => log2p p
  | _ => 0
  end.

Lemma log2z_correct (z: Z) : (0 < z)%Z -> (log2z z = `[log2 (IZR z)])%Z.
Proof.
elim: z=> // p Hp /=; by rewrite (log2p_correct p).
Qed.
